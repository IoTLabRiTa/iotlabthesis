package com.example.iotivitymobile

import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import com.example.iotivitymobile.device.Thermostat
import com.example.iotivitymobile.device.communication_protocols.CommunicationProtocol
import com.example.iotivitymobile.device.communication_protocols.IoTivityProtocol
import com.example.iotivitymobile.pattern_observer.MyObserver
import com.example.iotivitymobile.pattern_observer.message.OnProviderStarted


class ThermostatActivity : DeviceActivity(), MyObserver<OnProviderStarted> {

    val INIT_DEVICE_STRING ="name:Thermostat,nameProductor:Rita,model:Premium,serialNumber:40,isOn:true,temperatureReading:20,targetTemperature: 24"


    //GRAFICA
    lateinit var temperatureReadingTV : TextView
    lateinit var targetTemperatureTV : TextView

    lateinit var temperatureReadingAddBT : Button
    lateinit var temperatureReadingMinusBT : Button
    lateinit var targetTemperatureAddBT : Button
    lateinit var targetTemperatureMinusBT : Button

    lateinit var onOffSwitch : Switch
    lateinit var powerConsumptionTV: TextView

    lateinit var topicTV : TextView


    //LISTENER
    private lateinit var onCheckedListener: CompoundButton.OnCheckedChangeListener

    //MODEL DATA
    private lateinit var thermostat: Thermostat

    //CONNECTION to wifi
    private var connectionBroadcastReceiver:ConnectionBroadcastReceiver = ConnectionBroadcastReceiver(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        temperatureReadingTV = findViewById(R.id.TemperatureReadingValue)
        targetTemperatureTV = findViewById(R.id.targetTemperatureValue)


        temperatureReadingAddBT = findViewById(R.id.TemperatureReadingAddBT)
        temperatureReadingMinusBT = findViewById(R.id.TemperatureReadingMinusBT)

        targetTemperatureAddBT = findViewById(R.id.targetTemperatureAddBT)
        targetTemperatureMinusBT = findViewById(R.id.targetTemperatureMinusBT)

        onOffSwitch= findViewById(R.id.onOffSwitch)
        powerConsumptionTV = findViewById(R.id.powerConsumption)
        topicTV = findViewById(R.id.TopicTV)
        topicTV.text=""


        //Controllo connessione wifi
        val filter = IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        registerReceiver(connectionBroadcastReceiver, filter)

        startDevice()

        powerConsumptionTV.text = thermostat.getPowerConsumption().toString()+" W"

        onCheckedListener = CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            thermostat.onOff(b)
            powerConsumptionTV.text = thermostat.getPowerConsumption().toString()+" W"
            protocol.publishState()
        }

        onOffSwitch.setOnCheckedChangeListener(onCheckedListener)


        temperatureReadingAddBT.setOnClickListener { view ->
            thermostat.setTemperatureReading(thermostat.getTemperatureReading()+1)
            temperatureReadingTV.text=thermostat.getTemperatureReading().toString()+" °C"
            protocol.publishState()
        }

        temperatureReadingMinusBT.setOnClickListener { view ->
            thermostat.setTemperatureReading(thermostat.getTemperatureReading()-1)
            temperatureReadingTV.text=thermostat.getTemperatureReading().toString()+" °C"
            protocol.publishState()
        }

        targetTemperatureAddBT.setOnClickListener { view ->
            thermostat.setTargetTemperature(thermostat.getTargetTemperature()+1)
            targetTemperatureTV.text=thermostat.getTargetTemperature().toString()+" °C"
            protocol.publishState()
        }

        targetTemperatureMinusBT.setOnClickListener { view ->
            thermostat.setTargetTemperature(thermostat.getTargetTemperature()-1)
            targetTemperatureTV.text=thermostat.getTargetTemperature().toString()+" °C"
            protocol.publishState()
        }
    }

    override fun update(message: OnProviderStarted) {
        //message.perform(this)
        this.runOnUiThread{
            topicTV.text = message.topic + " added"
        }
        Log.d("TOPICTV","Updating with ${message.topic}")
    }


    private fun startDevice(){
        thermostat = Thermostat(this)
        //TODO Generalizzare con stringa per protocollo
        protocol= IoTivityProtocol(this,thermostat,INIT_DEVICE_STRING)
        protocol.registerObserver(this)
    }

    override fun onDestroy() {
        protocol.lostConnection()
        protocol.dead()
        super.onDestroy()
    }
}
