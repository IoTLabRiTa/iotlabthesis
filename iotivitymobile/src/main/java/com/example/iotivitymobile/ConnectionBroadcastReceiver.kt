package com.example.iotivitymobile

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.iotivitymobile.device.communication_protocols.IoTivityProtocol
import java.lang.Exception


class ConnectionBroadcastReceiver(var myActivity : DeviceActivity): BroadcastReceiver() {

    var firstInit = true

    override fun onReceive(context : Context?, intent : Intent?) {

        var wifiManager= context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager

        try{
            var isConnected= wifiManager.connectionInfo
            if(isConnected.networkId<0 && isConnected.linkSpeed<=0) {
                firstInit=false
                Log.d("IOTIVITY_WIFI_CHECK", "Disconnect to WIFI")
                var activity= context as DeviceActivity
                activity.protocol.lostConnection()
            }
            else {
                if (!firstInit){
                    myActivity.unregisterReceiver(this)
                    var activity= context as DeviceActivity
                    activity.finish()
                    var newIntent : Intent = Intent()
                    newIntent.setClass(context, myActivity.javaClass)
                    context.startActivity(newIntent)
                }
                Log.d("IOTIVITY_WIFI_CHECK","Connect to WIFI")
            }



        }catch (e: Exception){
            Log.d("IOTIVITY_ERROR", "exception "+  e.printStackTrace())
            Log.d("IOTIVITY_ERROR","Disconnect to WIFI")
        }

    }

}