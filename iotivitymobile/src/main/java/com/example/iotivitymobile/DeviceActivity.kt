package com.example.iotivitymobile

import android.support.v7.app.AppCompatActivity
import com.example.iotivitymobile.device.communication_protocols.CommunicationProtocol

abstract class DeviceActivity : AppCompatActivity() {

    //COMMUNICATION
    lateinit var protocol: CommunicationProtocol

}