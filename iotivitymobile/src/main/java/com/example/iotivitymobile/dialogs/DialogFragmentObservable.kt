package com.example.iotivitymobile.dialogs

import android.support.v4.app.DialogFragment
import com.example.iotivitymobile.pattern_observer.MyObserver
import java.util.ArrayList

abstract class DialogFragmentObservable<T> : DialogFragment(){
    private var observers: MutableList<MyObserver<T>> = ArrayList()

    fun registerObserver(myObserver: MyObserver<T>) {
        this.observers!!.add(myObserver)
    }

    fun removeObserver(myObserver: MyObserver<T>) {
        this.observers!!.remove(myObserver)
    }

    fun notifyObserver(message: T) {
        for (myObserver in observers!!)
            myObserver.update(message)
    }

    fun setObservers(observers: MutableList<MyObserver<T>>) {
        this.observers = observers
    }
}