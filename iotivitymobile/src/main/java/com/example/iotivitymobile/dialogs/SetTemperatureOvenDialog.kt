package com.example.iotivitymobile.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import com.example.iotivitymobile.R
import com.example.iotivitymobile.pattern_observer.message.MyMessage
import com.example.iotivitymobile.pattern_observer.message.OnTemperatureSet
import me.tankery.lib.circularseekbar.CircularSeekBar

class SetTemperatureOvenDialog : DialogFragmentObservable<MyMessage>(){
    @SuppressLint("NewApi")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        val builder = AlertDialog.Builder(activity)

        val inflater = activity!!.layoutInflater
        var view = inflater.inflate(R.layout.set_temperature_dialog, null)

        var centralImage : ImageView = view.findViewById(R.id.centerImage)
        var temperature : TextView = view.findViewById(R.id.temperature)
        temperature.text = "0"

        var seekbar : CircularSeekBar  = view.findViewById(R.id.seekBar)
        seekbar.max = 300F
        seekbar.circleStrokeWidth = 30F
        seekbar.progress = 1F

        seekbar.setOnSeekBarChangeListener(object : CircularSeekBar.OnCircularSeekBarChangeListener{
            override fun onProgressChanged(circularSeekBar: CircularSeekBar?, progress: Float, fromUser: Boolean) {
                temperature.text = progress.toInt().toString()

                centralImage.rotation = progress*360/300
            }

            override fun onStartTrackingTouch(seekBar: CircularSeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: CircularSeekBar?) {
            }
        })



        builder.setView(view)

        var okListener= DialogInterface.OnClickListener { dialog, id ->
            notifyObserver(OnTemperatureSet(seekbar.progress.toInt()))
        }

        val alertDialog = builder.setTitle("Set desired temperature")
                .setPositiveButton("Set",okListener)
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                // Create the AlertDialog object and return it
                .create()
        return alertDialog
    }
}