package com.example.iotivitymobile.device

import android.util.Log
import com.example.iotivitymobile.device.communication_protocols.CommunicationProtocol


class Device(st:DeviceState,deviceStructure: String) {


    private lateinit var name : String
    private lateinit var nameProductor : String
    private lateinit var model: String
    private lateinit var serialNumber: String
    private  var state: DeviceState = st

    init {
        convert(createAttributesMap(deviceStructure))
    }

    /*
    private var hostAddress : String = host

    fun getHostAddress()= hostAddress

    fun setHostAddress(value : String){
        hostAddress = value
    }*/

    fun getName() = name
    fun getNameProductor() = nameProductor
    fun getModel() = model
    fun getSerialNumber() = serialNumber
    fun getState()=state

    fun setState(value: DeviceState){
        state = value
    }

    fun getUniqueIdentifier()= name+nameProductor+model+serialNumber


    private fun createAttributesMap(value:String) : HashMap<String,String>{
        var map: HashMap<String,String> = HashMap()
        var attrsList=value.split(",")
        attrsList.forEach {
            val tuple = it.split(":")
            map.put(tuple[0],tuple[1])
        }
        return map
    }
    /**
     * Convert the map received from a device to the values of the devices
     */
    fun convert(attrsmap: HashMap<String,String>){
       try {
            attrsmap.forEach {
                when (it.key) {
                    "name" -> name = it.value
                    "nameProductor" -> nameProductor = it.value
                    "model" -> model = it.value
                    "serialNumber" -> serialNumber = it.value
                    else -> state.convertState(it.key,it.value)
                }
            }
        }catch (ex: Exception){
        //TODO Errore nella comunicazione, contattare il venditore del prodotto
            Log.d("MESSAGE_FORMAT", "Errore nel messaggio di invio dei parametri")
        }
    }


    fun getSerialization(): HashMap<String,String> {
        //TODO da fare con reflection
        var map:HashMap<String,String> = HashMap()
        map.put("name", name )
        map.put("nameProductor",nameProductor)
        map.put("model",model)
        map.put("serialNumber",serialNumber)
        state.getSerialization(map)
        return map
    }

    override fun toString(): String {
        return "Device '$name' '$serialNumber', state=$state"
    }


}