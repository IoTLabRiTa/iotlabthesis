package com.example.iotivitymobile.device.communication_protocols


import android.content.Context
import android.net.nsd.NsdManager
import android.util.Log
import com.example.iotivitymobile.device.Device
import com.example.iotivitymobile.device.DeviceState
import com.example.iotivitymobile.pattern_observer.MyObservable
import com.example.iotivitymobile.pattern_observer.message.MyMessage
import com.example.iotivitymobile.pattern_observer.message.OnProviderStarted
import org.iotivity.base.*
import org.iotivity.service.RcsResourceAttributes
import org.iotivity.service.ns.common.MediaContents
import org.iotivity.service.ns.common.NSException
import org.iotivity.service.ns.provider.ProviderService
import org.iotivity.service.server.RcsGetResponse
import org.iotivity.service.server.RcsResourceObject
import org.iotivity.service.server.RcsSetResponse
import java.lang.IllegalArgumentException


class IoTivityProtocol(context: Context,deviceState:DeviceState,deviceStructure:String) : CommunicationProtocol() {


    private val LOG_TAG: String = "IOTIVITY_CONNECTION"
    private val LOG_TAG_ERROR: String = "IOTIVITY_ERROR"

    private val URI = "/a/TempSensor"
    private val TYPE = "oic.r.temperature.sensor"
    private val INTERFACE = "oic.if."
    private var resourceObject : RcsResourceObject? = null
    private lateinit var providerService: ProviderService
    private var isNotifyingPresence = false
    private var providerIsStarted = false
    private lateinit var presenceThread: Thread
    private var device:Device

    init{
        device= Device(deviceState,deviceStructure)
        configurePlatform(context)
        startExpose()
    }


    private fun configurePlatform(applicationContext: Context) {
        OcPlatform.Configure(PlatformConfig(applicationContext,
                ServiceType.IN_PROC, ModeType.CLIENT_SERVER,
                QualityOfService.LOW))
    }

    override fun startExpose() {
        //Delete previous deviceResource
        if (resourceObject != null) {
            resourceObject!!.destroy()
            resourceObject = null
        }

        providerService = ProviderService.getInstance()
        //Define attributes
        val attributesDevice = RcsResourceAttributes()
        device.getSerialization().forEach{
            attributesDevice.put(it.key,it.value)
        }

        //Create deviceResource
        resourceObject = RcsResourceObject.Builder(
                URI, TYPE,
                INTERFACE).setAttributes(attributesDevice).build()

        //Define handlers
        var mGetRequestHandler: RcsResourceObject.GetRequestHandler = RcsResourceObject.GetRequestHandler { request, attrs ->
            //DEFAULT RESPONSE
            Log.d(LOG_TAG, "Received Get Request from ${request.resourceUri}, sending Default Response")
            RcsGetResponse.defaultAction()
        }

        var mSetRequestHandler: RcsResourceObject.SetRequestHandler = RcsResourceObject.SetRequestHandler {
            request, attrs ->

            //Ack received--> Device created succesfully on Android Things
            if(attrs.contains("ACK_CREATION")){
                if(attrs.get("ACK_CREATION").asBoolean()){
                    startProvider()
                    startPresence()
                }
                Log.d(LOG_TAG,"Received Set Request from ${request.resourceUri}, performing Default Action")
            }
            else if (attrs.contains("COMMAND")){
                val command = attrs.get("COMMAND").asString()
                attrs.remove("COMMAND")
                try {
                    device.getState().parseCommand(command,createAttributesMap(attrs))
                    publishState()
                }catch (ex: IllegalArgumentException){
                    Log.d(LOG_TAG_ERROR,ex.message)
                }
            }
            RcsSetResponse.defaultAction()

        }

        resourceObject!!.setSetRequestHandler(mSetRequestHandler)
        resourceObject!!.setGetRequestHandler(mGetRequestHandler)

    }

    private fun createAttributesMap(attrs: RcsResourceAttributes) : HashMap<String,String>{
        var map: HashMap<String,String> = HashMap()
        attrs.keySet().forEach {
            map.put(it,attrs[it].asString())
        }
        return map
    }

    //FUNCTION TO START PROVIDER AND NOTIFY DEVICE STATE ( WITH PUBLISHING OF MESSAGES)

    fun startProvider(){
        //Provider Listeners
        var subscribedListener : ProviderService.OnConsumerSubscribedListener = ProviderService.OnConsumerSubscribedListener{ consumer ->
            //Accept subscription
            consumer.acceptSubscription(true)
            consumer.setTopic(device.getUniqueIdentifier())
        }
        var messageSynchronizedListener : ProviderService.OnMessageSynchronizedListener = ProviderService.OnMessageSynchronizedListener { syncInfo ->
            // TODO Non ancora implementato
        }
        startProviderService(subscribedListener,messageSynchronizedListener)
        notifyObserver(OnProviderStarted(device.getUniqueIdentifier()))
        Log.d(LOG_TAG, "PROVIDER SERVICE STARTED, REGISTER TOPIC")
    }

    private fun startProviderService(subscribedListener:ProviderService.OnConsumerSubscribedListener,messageSynchronizedListener: ProviderService.OnMessageSynchronizedListener){
        try {
            providerService.start(subscribedListener, messageSynchronizedListener, true, "Info", false)
            sleep(1)
            registerTopic()
        }catch(ex:NSException){
            Log.d(LOG_TAG_ERROR, "PROVIDER CANNOT START: ${ex.message}")
            sleep(1)
            startProviderService(subscribedListener,messageSynchronizedListener)
        }
    }


    private fun registerTopic(){
        try{
            providerService.registerTopic(device.getUniqueIdentifier())
            providerIsStarted = true
        }catch (ex:NSException){
            Log.d(LOG_TAG_ERROR, "PROVIDER CANNOT REGISTER TOPIC: ${ex.message}")
            sleep(1)
            registerTopic()
        }
    }

    override fun publishState() {
        try {
            if (providerIsStarted) {
                var representation = OcRepresentation()
                device.getSerialization().forEach {
                    representation.setValue(it.key, it.value)
                }
                var message = providerService.createMessage()
                message.title = "Temperature reading"
                message.topic = device.getUniqueIdentifier()
                message.time = "Now"
                message.ttl = 10
                message.sourceName = "Thermostat"
                val media = MediaContents("daasd")
                message.setMediaContents(media)
                message.contentText = device.getUniqueIdentifier() + " changes state"
                message.extraInfo = representation
                providerService.sendMessage(message)
            }
        }catch (ex : NSException){
            Log.d(LOG_TAG_ERROR, "PROVIDER CANNOT PUBLISH MESSAGE, DEVICE IS OFFLINE")
        }
    }


    //FUNCTION TO START AND STOP PRESENCE NOTIFICATION

    private fun startPresence(){

        presenceThread=Thread(Runnable {
            isNotifyingPresence=true
            Log.d(LOG_TAG,"CLIENT STARTS TO NOTIFY ITS PRESENCE")
            sleep(5)
            while (isNotifyingPresence) {
                notifyPresence()
                sleep(10)

            }
        })
        presenceThread.start()
    }

    private fun notifyPresence() {
        try {
            OcPlatform.startPresence(OcPlatform.DEFAULT_PRESENCE_TTL)
            Log.d(LOG_TAG,"CLIENT NOTIFIES ITS PRESENCE")
        } catch (e: OcException) {
            Log.d(LOG_TAG_ERROR, "START PRESENCE ERROR:  ${e.toString()}")
        }
    }

    private fun stopPresenceServer() {
        try {
            OcPlatform.stopPresence()
            Log.d(LOG_TAG,"CLIENT STOPS ITS PRESENCE")
            isNotifyingPresence=false
        } catch (e: OcException) {
            Log.e(LOG_TAG_ERROR, "STOP PRESENCE ERROR:  ${e.toString()}")
        }
    }

    /**
     * FUNCTION USED WHEN CONNECTION IS LOST OR DEVICE DEAD
     * Destroy resource object on OcPlatform stack, sto provider service and notification of presence
     */
    override fun lostConnection(){
        if (resourceObject != null)
            resourceObject!!.destroy()

        providerService.stop()
        isNotifyingPresence=false
    }

    override fun dead() {
        stopPresenceServer()
    }


    //TODO DA SPOSTARE IN UNA CLASSE STATICA CONTENENTE I TAG E QUESTA FUNZIONE
    /**
     * Static function used to sleep the thread
     */
    companion object {
        fun sleep(seconds: Int) {
            try {
                Thread.sleep((seconds * 1000).toLong())
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }



}