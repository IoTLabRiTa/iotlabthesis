package com.example.iotivitymobile.device

import java.lang.IllegalArgumentException


abstract class DeviceState {

    private var isOn : Boolean = false
    private var powerConsumption : Int = 0
    private var commandMap: HashMap<String,Command> = HashMap()


    fun isOn() = isOn
    fun getPowerConsumption()= powerConsumption
    fun getCommandMap()=commandMap

    fun setIsOn(value: Boolean){
        isOn = value
    }

    fun setPowerConsumption(value:Int){
        powerConsumption=value
    }

    fun convertState(attr: String, value: String){
        when (attr) {
            "isOn" -> onOff(value.toBoolean())
            "powerConsumption" -> powerConsumption = value.toInt()
            else -> {
                if(!convertAddedAttribute(attr,value))
                    commandMap[attr]=Command(attr,value)
            }
        }
    }

    abstract fun getStringCommands() : HashMap<String,String>

    abstract fun convertAddedAttribute(attr: String, value: String) : Boolean

    fun getSerialization(map: HashMap<String, String>) {
        map.put("isOn",isOn.toString())
        map.put("powerConsumption",powerConsumption.toString())
        getAddedSerialization(map)
        map.putAll(getStringCommands())
    }

    abstract fun getAddedSerialization(map: HashMap<String, String>)

    abstract fun onOff(value: Boolean)

    //TODO Implementare nostra eccezione
    @Throws(IllegalArgumentException::class)
    abstract fun parseCommand(command: String?, parametersMap: HashMap<String, String>)


}