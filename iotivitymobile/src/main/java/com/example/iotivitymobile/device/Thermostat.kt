package com.example.iotivitymobile.device

import com.example.iotivitymobile.ThermostatActivity
import java.lang.IllegalArgumentException


class Thermostat(controller: ThermostatActivity): DeviceState() {


    private var temperatureReading: Float = 0F
    private var targetTemperature: Float = 0F
    private var hwController : ThermostatActivity = controller

    fun getTemperatureReading() = temperatureReading
    fun getTargetTemperature() = targetTemperature


    override fun parseCommand(command: String?, parametersMap: HashMap<String, String>) {
        when(command){
            "onOff" -> onOff(parametersMap["onOffState"]!!.toBoolean())
            "setTargetTemperature" -> setTargetTemperature(parametersMap["targetTemperature"]!!.toFloat())
            "resetTemperature" -> setTargetTemperature(17F)
            else -> throw IllegalArgumentException("Command not found")
        }
    }

    fun setTemperatureReading(temp: Float){
        temperatureReading = temp
        hwController.runOnUiThread{
            hwController.temperatureReadingTV.text ="${temperatureReading} °C"
        }
    }

    fun setTargetTemperature(temp: Float){
        targetTemperature = temp
        hwController.runOnUiThread{
            hwController.targetTemperatureTV.text ="${targetTemperature} °C"
        }
    }

    override fun onOff(value: Boolean) {
        setIsOn(value)
        if(isOn())
            setPowerConsumption(50)
        else
            setPowerConsumption(5)

        hwController.runOnUiThread{
            hwController.onOffSwitch.isChecked = isOn()
        }

    }

    /**
     * Assign the value of the message to the class
     */
    override fun convertAddedAttribute(attr: String, value: String) : Boolean{
        when(attr){
            "temperatureReading"->setTemperatureReading(value.toFloat())
            "targetTemperature"->setTargetTemperature(value.toFloat())
            else -> return false
        }
        return true
    }

    override fun getStringCommands() : HashMap <String, String>  {
        val commands : HashMap <String, String> = HashMap()
        commands.put("setTargetTemperature","Imposta la temperatura desiderata-targetTemperature,Integer,10,30,SeekBar")
        commands.put("resetTemperature","Resetta la temperatura di defeault [17 °C]")
        return commands
    }

    override fun getAddedSerialization(map: HashMap<String, String>) {
        map.put("targetTemperature",targetTemperature.toString())
        map.put("temperatureReading",temperatureReading.toString())
    }

    override fun toString(): String {
        return "$(temperatureReading = ${temperatureReading}, targetTemperature = ${targetTemperature}, on = ${isOn()})"
    }

}