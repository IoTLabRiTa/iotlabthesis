package com.example.iotivitymobile.device

import com.example.iotivitymobile.OvenActivity
import java.lang.IllegalArgumentException

class Oven(var hwController : OvenActivity) : DeviceState()  {

    private var temperature : Int = 0
    private var mode : String ="Static"
    private var modeList : ArrayList<String> = ArrayList()
    private var temperatureReached : Boolean = false

    init {
        modeList.add("Static")
        modeList.add("Grill")
        modeList.add("Ventilated")
    }

    fun getTemperature() = temperature

    fun setTemperature(value: Int){
        temperature = value
        hwController.runOnUiThread{
            //hwController.temperatureSeekBar.setProgress(value,true)
            hwController.temperatureDisplay.text = "$temperature °C"
        }
    }

    fun getModeList() = modeList
    fun getMode() = mode

    fun setMode(value: String){
        mode = value
        hwController.runOnUiThread{
            //hwController.modeRG.check(hwController.getRadioID(getMode()))
            hwController.modeSelectedDisplay.text = mode
        }
    }

    fun isTemperaturereached() = temperatureReached

    fun setTemperatureReached(value : Boolean){
        temperatureReached = value
    }



    override fun getStringCommands(): HashMap<String, String> {
        val commands : HashMap <String, String> = HashMap()
        commands.put("setTemperature","Imposta la temperatura selezionata-temperature,Integer,0,300,SeekBar")
        commands.put("changeMode","Imposta la modalità di cottura del forno-mode,String,Grill/Ventilated/Static, ,Spinner")
        return commands
    }

    override fun convertAddedAttribute(attr: String, value: String): Boolean {
        when(attr){
            "temperature"->setTemperature(value.toInt())
            "mode" -> setMode(value)
            "temperatureReached" -> setTemperatureReached(value.toBoolean())
            else -> return false
        }
        return true
    }

    override fun getAddedSerialization(map: HashMap<String, String>) {
        map.put("temperature",temperature.toString())
        map.put("mode",mode)
        map.put("temperatureReached",temperatureReached.toString())
    }

    override fun onOff(value: Boolean) {

        setIsOn(value)
        if(isOn()){
            setPowerConsumption(50)
        }
        else{
            setTemperature(0)
            setPowerConsumption(5)
        }

        hwController.runOnUiThread{
            hwController.onOffSwitch.isChecked = isOn()
        }

    }

    override fun parseCommand(command: String?, parametersMap: HashMap<String, String>) {
        when(command){
            "onOff" -> onOff(parametersMap["onOffState"]!!.toBoolean())
            "setTemperature" -> setTemperature(parametersMap["temperature"]!!.toInt())
            "changeMode" -> setMode(parametersMap["mode"]!!)
            else -> throw IllegalArgumentException("Command not found")
        }
    }

}