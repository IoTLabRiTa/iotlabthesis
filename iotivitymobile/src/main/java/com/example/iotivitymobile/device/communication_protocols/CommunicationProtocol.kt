package com.example.iotivitymobile.device.communication_protocols

import com.example.iotivitymobile.pattern_observer.MyObservable
import com.example.iotivitymobile.pattern_observer.message.MyMessage


abstract class CommunicationProtocol : MyObservable<MyMessage>(){

    abstract fun lostConnection()
    abstract fun dead()
    abstract fun startExpose()
    abstract fun publishState()

}
