package com.example.iotivitymobile.device


import java.io.Serializable

class Command(comName:String,comandStructure:String) : Serializable{


    private var name: String = comName
    private lateinit  var description:String
    private var parameterList :ArrayList<MyParameter> = ArrayList()


    init {
        val list= comandStructure.split("-")
        description=list[0]
        parseCommand(list[1])
    }

    fun getName() = name
    fun getDescription() = description
    fun getParameters() = parameterList

    private fun parseCommand(commandStructure :String){
        commandStructure.split(";").forEach {
            it.split(",").apply {
                var param = MyParameter()
                param.nameParameter=this[0]
                param.type=this[1]
                param.min = this[2]
                param.max = this[3]
                param.graphic = this[4]
                parameterList.add(param)
            }
        }
    }

    class MyParameter {
        lateinit var nameParameter:String
        lateinit var type: String
        lateinit var min: String
        lateinit var max: String
        lateinit var graphic : String
    }





}
