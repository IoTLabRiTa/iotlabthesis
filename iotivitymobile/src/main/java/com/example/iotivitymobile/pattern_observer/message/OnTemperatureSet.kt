package com.example.iotivitymobile.pattern_observer.message

import android.app.Activity
import com.example.iotivitymobile.OvenActivity

class OnTemperatureSet(var temperature : Int) : MyMessage {
    override fun perform(controller: Activity) {

        controller as OvenActivity
        controller.runOnUiThread{
            controller.getOven().setTemperature(temperature)
            controller.temperatureDisplay.text = "$temperature °C"
            controller.protocol.publishState()
        }


    }
}