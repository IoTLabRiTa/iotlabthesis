package com.example.iotivitymobile.pattern_observer.message

import android.app.Activity



interface MyMessage {

    fun perform(controller: Activity)

}