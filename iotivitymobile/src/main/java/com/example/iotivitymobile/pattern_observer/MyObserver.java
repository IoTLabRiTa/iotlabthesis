package com.example.iotivitymobile.pattern_observer;

/**
 * Created by Daniele on 16/03/2018.
 */

public interface MyObserver<T> {

    public void update(T object);

}
