package com.example.iotivitymobile

import android.content.IntentFilter
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.iotivitymobile.device.Oven
import com.example.iotivitymobile.device.communication_protocols.IoTivityProtocol
import com.example.iotivitymobile.dialogs.SetTemperatureOvenDialog
import com.example.iotivitymobile.pattern_observer.MyObserver
import com.example.iotivitymobile.pattern_observer.message.MyMessage

class OvenActivity: DeviceActivity(), MyObserver<MyMessage> {

    val INIT_DEVICE_STRING ="name:Oven,nameProductor:RiTa,model:Super,serialNumber:112,isOn:true,temperature:200,mode:Static,temperatureReached:false"


    //GRAFICA
    /*
    lateinit var temperatureTV : TextView
    lateinit var topicTV : TextView

    lateinit var temperatureSeekBar : SeekBar
    lateinit var modeRG : RadioGroup
    lateinit var temperatureReachedSwitch : Switch
*/
    //Grafica aggiornata
    lateinit var setTemperatureBT : ImageButton
    lateinit var temperatureDisplay : TextView
    lateinit var temperatureReachedBT : ImageButton
    lateinit var modeSelectedDisplay : TextView
    lateinit var previousModeBT : ImageButton
    lateinit var nextModeBT : ImageButton
    lateinit var onOffSwitch : Switch
    lateinit var powerConsumptionTV: TextView



    //LISTENER
    private lateinit var onCheckedListener: CompoundButton.OnCheckedChangeListener

    //MODEL DATA
    private lateinit var oven: Oven

    //CONNECTION TO WIFI
    private var connectionBroadcastReceiver:ConnectionBroadcastReceiver = ConnectionBroadcastReceiver(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.oven)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()
        supportActionBar?.hide()
        setContentView(R.layout.oven_fullscreen)

        setTemperatureBT = findViewById(R.id.setTemperatureBT)
        temperatureDisplay = findViewById(R.id.temperatureDisplay)
        temperatureReachedBT = findViewById(R.id.temperatureReachedBT)
        modeSelectedDisplay = findViewById(R.id.modeSelectedDisplay)
        previousModeBT = findViewById(R.id.previousModeBT)
        nextModeBT = findViewById(R.id.nextModeBT)
        onOffSwitch = findViewById(R.id.onOffSwitch)
        powerConsumptionTV = findViewById(R.id.powerConsumption)


        //Controllo connessione wifi
        val filter = IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        registerReceiver(connectionBroadcastReceiver, filter)

        startDevice()

        //Draw UI
        powerConsumptionTV.text = oven.getPowerConsumption().toString()+" W"

        onCheckedListener = CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            oven.onOff(b)
            powerConsumptionTV.text = oven.getPowerConsumption().toString()+" W"
            temperatureDisplay.text = "0 °C"
            oven.setTemperatureReached(false)
            drawReachedBT()

            protocol.publishState()
        }

        onOffSwitch.setOnCheckedChangeListener(onCheckedListener)

        temperatureDisplay.text = oven.getTemperature().toString() + " °C"

        //Color temperatureReached BT
        drawReachedBT()

        temperatureReachedBT.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                oven.setTemperatureReached(!oven.isTemperaturereached())
                //Update UI
                drawReachedBT()
                protocol.publishState()
            }

        })

        previousModeBT.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                var index = oven.getModeList().indexOf(oven.getMode())
                var previous = oven.getModeList().size - 1 ;
                if(index != 0){
                    previous = index -1
                }
                oven.setMode(oven.getModeList().get(previous))
                protocol.publishState()
            }

        })

        nextModeBT.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                var index = oven.getModeList().indexOf(oven.getMode())
                var next = 0;
                if(index != oven.getModeList().size -1){
                    next = index + 1
                }
                oven.setMode(oven.getModeList().get(next))
                protocol.publishState()
            }

        })

        setTemperatureBT.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                var setTemperatureDialog : SetTemperatureOvenDialog = SetTemperatureOvenDialog()
                setTemperatureDialog.registerObserver(this@OvenActivity)
                setTemperatureDialog.show(this@OvenActivity.supportFragmentManager,"setTemperatureDialog")            }

        })

/*
        temperatureSeekBar.max = 300

        temperatureSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{

            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                val newValue = p1
                temperatureTV.text = newValue.toString()+ " °C"
                oven.setTemperature(newValue)
                protocol.publishState()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }
            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })

        modeRG.check(getRadioID(oven.getMode()))

        modeRG.setOnCheckedChangeListener { radioGroup, i ->
            val radioButton = findViewById<RadioButton>(i)
            oven.setMode(radioButton.text.toString())
            protocol.publishState()
            Log.d("VALUE_RADIO",radioButton.text.toString())
        }

        */

    }

    fun drawReachedBT(){
        if(oven.isTemperaturereached()){
            temperatureReachedBT.setImageResource(android.R.drawable.ic_notification_overlay)
        }else{
            temperatureReachedBT.setImageResource(android.R.drawable.presence_invisible)
        }
    }

    fun getRadioID(value:String):Int{
        when(value){
            "Ventilated" -> return R.id.VentilatedRB
            "Grill" -> return R.id.GrillRB
            else -> return  R.id.StaticRB
        }
    }


    override fun update(message: MyMessage) {
        this.runOnUiThread{
            message.perform(this)
        }
    }

    private fun startDevice(){
        oven = Oven(this)
        //TODO Generalizzare con stringa per protocollo
        protocol= IoTivityProtocol(this,oven,INIT_DEVICE_STRING)
        protocol.registerObserver(this)
    }


    override fun onDestroy() {
        protocol.lostConnection()
        protocol.dead()
        super.onDestroy()
    }

    fun getOven() = oven

}