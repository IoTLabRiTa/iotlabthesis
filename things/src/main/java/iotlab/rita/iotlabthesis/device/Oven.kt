package iotlab.rita.iotlabthesis.device


import iotlab.rita.iotlabthesis.model.home_resources.ResourceCollectionInterface

class Oven : DeviceState()  {


    private var temperature : Int = 0
    private var mode : String ="Static"
    private var temperatureReached : Boolean = false

    fun getTemperature() = temperature

    fun setTemperature(value: Int){
        temperature = value
    }

    fun getMode() = mode

    fun setMode(value: String){
        mode = value
    }

    fun isTemperaturereached() = temperatureReached

    fun setTemperatureReached(value : Boolean){
        temperatureReached = value
    }


    override fun convertAddedAttribute(attr: String, value: String,isAreset:Boolean): Boolean {
        when(attr){
            "temperature"->setTemperature(value.toInt())
            "mode" -> setMode(value)
            "temperatureReached" -> setTemperatureReached(value.toBoolean())
            else -> return false
        }
        return true
    }

    override fun getAddedSerialization(map: HashMap<String, String>) {
        map.put("temperature",temperature.toString())
        map.put("mode",mode)
        map.put("temperatureReached",temperatureReached.toString())
    }


    override fun registerResource(resourceCollectionInterface: ResourceCollectionInterface) {
        //Nessuna risorsa da registrare il forno
        //TODO Controllare
    }

    override fun toString(): String {
        return "(temperature= $temperature, mode= '$mode', temperatureReached= $temperatureReached, on= ${isOn()})"
    }

}