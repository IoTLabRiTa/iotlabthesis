package iotlab.rita.iotlabthesis.device

import iotlab.rita.iotlabthesis.device.device_resources.EnergyCollectedDeviceResource
import iotlab.rita.iotlabthesis.model.home_resources.ResourceCollectionInterface

class SolarPanel : DeviceState() {


    private lateinit var powerAccumulated : EnergyCollectedDeviceResource

    fun getPOwerAccumulated() = powerAccumulated.getValue()
    /**
     * Assign the value of the message to the class
     */
    override fun convertAddedAttribute(attr: String, value: String, isAreset : Boolean) : Boolean{
        when(attr){
            "powerAccumulated"->powerAccumulated.setValue(value.toFloat(), isAreset)
            else -> return false
        }
        return true
    }

    override fun registerResource(resourceCollectionInterface: ResourceCollectionInterface) {
        powerAccumulated = resourceCollectionInterface.getEnergyCollectedDeviceResource()
    }

    override fun toString(): String {
        return "(powerAccumulated=${powerAccumulated.getValue()}, on=${isOn()})"
    }

    override fun getAddedSerialization(map: HashMap<String, String>) {
        map.put("powerAccumulated",powerAccumulated.getValue().toString())
    }

}