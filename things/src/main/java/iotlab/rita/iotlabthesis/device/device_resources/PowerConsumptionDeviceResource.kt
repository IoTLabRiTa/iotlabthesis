package iotlab.rita.iotlabthesis.device.device_resources

import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage

class PowerConsumptionDeviceResource : DeviceResource<Int>() {

    private var power : Int = 0

    fun getValue() = power

    fun setValue(newValue : Int,isAreset:Boolean){
        //TODO controllare se è una valore differenziale e se c'è quindi bisogno di inviare lo stesso valore e non la differenza
        Log.d("RESOURCE","SONO PASSATO")
        if(isAreset)
            notifyObserver(ResourceMessage(newValue)) //Send the value
        else
            notifyObserver(ResourceMessage(newValue-power)) //Send the difference
        power = newValue
    }
}