package iotlab.rita.iotlabthesis.device

import android.util.Log
import iotlab.rita.iotlabthesis.device.communication_protocols.CommunicationProtocol
import iotlab.rita.iotlabthesis.model.rules.Antecedent
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ProtocolMessage

class Device(st:DeviceState,attrsmap: HashMap<String,String>,host:String, protocol : CommunicationProtocol)
    : MyObservable<MyMessage>(), MyObserver<ProtocolMessage>, Antecedent {


    private lateinit var name : String
    private lateinit var nameProductor : String
    private lateinit var model: String
    private lateinit var serialNumber: String
    private var communicationProtocol : CommunicationProtocol = protocol
    private var hostAddress : String = host
    private  var deviceState: DeviceState = st

    init {
        convert(attrsmap,false)
        communicationProtocol.registerObserver(this)
    }

    fun getHostAddress()= hostAddress
    fun getCommunicationProtocol() = communicationProtocol

    fun setHostAddress(value : String){
        hostAddress = value
    }

    fun setState(value: DeviceState){
        deviceState = value
    }

    fun getName() = name
    fun getNameProductor() = nameProductor
    fun getModel() = model
    fun getSerialNumber() = serialNumber
    fun getDeviceState()=deviceState

    fun setCommunicationProtocol(value:CommunicationProtocol){
        communicationProtocol = value
    }


    override fun update(message: ProtocolMessage) {
        //TODO Aggiornare la smartHome per questione risorse
        message.perform(this)
        notifyObserver(message)
    }

    /**
     * Convert the map received from a device to the values of the devices
     */
    fun convert(attrsmap: HashMap<String,String>, isAreset : Boolean){

        try {
            attrsmap.forEach {
                Log.d("RESOURCE","Parsing: ${it.key}   ${it.value}")
                when (it.key) {
                    "name" -> name = it.value
                    "nameProductor" -> nameProductor = it.value
                    "model" -> model = it.value
                    "serialNumber" -> serialNumber = it.value
                    else -> deviceState.convertState(it.key,it.value, isAreset)
                }
            }
        }catch (ex: Exception){
        //TODO Errore nella comunicazione, contattare il venditore del prodotto
            Log.d("MESSAGE_FORMAT", "Errore nel messaggio di invio dei parametri ${ex.printStackTrace()}")
        }
    }


    override fun toString(): String {
        return "Device '$name' '$serialNumber', deviceState=$deviceState"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Device) return false

        if (name != other.name) return false
        if (nameProductor != other.nameProductor) return false
        if (model != other.model) return false
        if (serialNumber != other.serialNumber) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + nameProductor.hashCode()
        result = 31 * result + model.hashCode()
        result = 31 * result + serialNumber.hashCode()
        result = 31 * result + hostAddress.hashCode()
        return result
    }

    override fun getSerialization(): HashMap<String,String> {
        //TODO da fare con reflection
        var map:HashMap<String,String> = HashMap()
        map.put("name", name )
        map.put("nameProductor",nameProductor)
        map.put("model",model)
        map.put("serialNumber",serialNumber)
        map.putAll(deviceState.getSerialization())
        return map
    }

    fun getUniqueID() = name+nameProductor+model+serialNumber

    override fun getAntecedentID(): String = getUniqueID()


}