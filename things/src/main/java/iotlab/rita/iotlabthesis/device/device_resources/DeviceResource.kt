package iotlab.rita.iotlabthesis.device.device_resources

import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage

abstract class DeviceResource<T> :  MyObservable<ResourceMessage<T>>(){



}