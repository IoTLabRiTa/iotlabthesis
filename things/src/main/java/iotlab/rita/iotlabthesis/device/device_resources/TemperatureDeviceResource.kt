package iotlab.rita.iotlabthesis.device.device_resources

import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage

class TemperatureDeviceResource : DeviceResource<Float>(){

    private var value : Float = 0F

    fun getValue() = value

    fun setValue(newValue : Float){
        this.value = newValue
        notifyObserver(ResourceMessage(this.value))
    }
}