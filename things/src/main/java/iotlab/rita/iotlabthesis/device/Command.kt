package iotlab.rita.iotlabthesis.device


import java.io.Serializable

class Command(comName:String,private var commandStructure:String) : Serializable{


    private var name: String = comName
    private lateinit  var description:String
    private var parameterList :ArrayList<MyParameter> = ArrayList()


    init {
        val list= commandStructure.split("-")
        description=list[0]
        //Check commands with empty parameters
        if(list.size > 1)
            parseCommand(list[1])
    }

    fun getName() = name
    fun getDescription() = description
    fun getParameters() = parameterList
    fun getCommandStructure() = commandStructure

    private fun parseCommand(commandStructure :String){
        commandStructure.split(";").forEach {
            it.split(",").apply {
                var param = MyParameter()
                param.nameParameter=this[0]
                param.type=this[1]
                param.min = this[2]
                param.max = this[3]
                param.graphic = this[4]
                parameterList.add(param)
            }
        }
    }

    class MyParameter {
        lateinit var nameParameter:String
        lateinit var type: String
        lateinit var min: String
        lateinit var max: String
        lateinit var graphic : String
    }





}
