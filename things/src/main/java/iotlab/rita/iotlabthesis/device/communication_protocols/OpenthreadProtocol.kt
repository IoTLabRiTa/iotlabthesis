package iotlab.rita.iotlabthesis.device.communication_protocols

import android.util.Log
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.Inet6Address

class OpenthreadProtocol (var host:String, var senderSocket : DatagramSocket) : CommunicationProtocol() {

    fun getHostAddress() = host

    override fun lostConnection() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendCommand(value: String, parameterList: HashMap<String, String>) {
        var command = "COMMAND:"+value
        parameterList.forEach{
            command+=";"+it.key+":"+it.value
        }
        var buf = command.toByteArray()
        var packet = DatagramPacket(buf,buf.size,Inet6Address.getByName(host),senderSocket.localPort)
        Thread{
            senderSocket.send(packet)
            Log.d("THREAD_CONNECTION","Command executed")
        }.start()

        //TODO Rendere invisibile mettendo questa funzione da ovverridare in un'altra funzione padre che viene chiamata lei al suo posto
        waitingNewState = true
    }

    override fun isNotReady(): Boolean {
        //TODO check se ha senso
        return false
    }
}