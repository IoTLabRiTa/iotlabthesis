package iotlab.rita.iotlabthesis.device.device_resources

import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage

class EnergyCollectedDeviceResource : DeviceResource<Float>(){

    private var energyCollected : Float = 0F

    fun getValue() = energyCollected

    fun setValue(newValue : Float, isAreset:Boolean){

        Log.d("RESOURCE","SONO PASSATO")
        if(isAreset)
            notifyObserver(ResourceMessage(newValue)) // send the value
        else
            notifyObserver(ResourceMessage(newValue-energyCollected)) //Send the difference
        this.energyCollected = newValue

    }




}