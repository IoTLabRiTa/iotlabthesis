package iotlab.rita.iotlabthesis.device

import android.util.Log
import iotlab.rita.iotlabthesis.device.device_resources.PowerConsumptionDeviceResource
import iotlab.rita.iotlabthesis.model.home_resources.ResourceCollectionInterface

abstract class DeviceState() {

    private var isOn : Boolean = false
    private lateinit var powerConsumption : PowerConsumptionDeviceResource
    private var commandMap: HashMap<String,Command> = HashMap()

    //collezione di risorse del dispositivo

    fun isOn() = isOn
    fun getPowerConsumption()= powerConsumption
    fun getCommandMap()=commandMap


    fun setIsOn(value: Boolean){
        isOn = value
    }

    fun convertState(attr: String, value: String, isAreset: Boolean){
        when (attr) {
            "isOn" -> isOn = value.toBoolean()
            "powerConsumption" -> powerConsumption.setValue(value.toInt(),isAreset)
            else -> {
                if(!convertAddedAttribute(attr,value, isAreset))
                    commandMap.put(attr,Command(attr,value))

            }
        }
        Log.d("CommandListSize",commandMap.size.toString())
    }

    fun setInterface(resourceCollectionInterface: ResourceCollectionInterface){
        powerConsumption = resourceCollectionInterface.getPowerConsumptionDeviceResource()
        registerResource(resourceCollectionInterface)
    }

    fun getSerialization() : HashMap<String, String> {
        var map : HashMap<String, String> = HashMap()
        map.put("isOn",isOn.toString())
        map.put("powerConsumption",powerConsumption.getValue().toString())
        getAddedSerialization(map)
        commandMap.forEach{
            map.put(it.key,it.value.getCommandStructure())
        }
        return map
    }

    abstract fun getAddedSerialization(map: HashMap<String, String>)

    abstract fun registerResource(resourceCollectionInterface: ResourceCollectionInterface)
    abstract fun convertAddedAttribute(attr: String, value: String, isAreset: Boolean) : Boolean





}