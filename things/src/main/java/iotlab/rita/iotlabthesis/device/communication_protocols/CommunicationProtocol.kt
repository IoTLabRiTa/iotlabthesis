package iotlab.rita.iotlabthesis.device.communication_protocols

import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.ProtocolMessage

abstract class CommunicationProtocol: MyObservable<ProtocolMessage>(){

    var waitingNewState : Boolean = false

    abstract fun lostConnection()
    abstract fun sendCommand(value: String, parameterList: HashMap<String, String>)
    abstract fun isNotReady() : Boolean

}
