package iotlab.rita.iotlabthesis.device.communication_protocols


import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.message.DeviceStateUpdateMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.OnDeviceDisconnectedMessage
import org.iotivity.base.OcConnectivityType
import org.iotivity.base.OcPlatform
import org.iotivity.base.OcPresenceHandle
import org.iotivity.base.OcPresenceStatus
import org.iotivity.service.RcsResourceAttributes
import org.iotivity.service.client.RcsRemoteResourceObject
import org.iotivity.service.ns.common.Message
import org.iotivity.service.ns.common.NSException
import org.iotivity.service.ns.common.SyncInfo
import org.iotivity.service.ns.consumer.Provider
import java.lang.Exception
import java.util.*
import kotlin.collections.HashMap


class IoTivityProtocol(resource: RcsRemoteResourceObject) : CommunicationProtocol(),OcPlatform.OnPresenceListener{


    private var presenceHandle : OcPresenceHandle
    private var remoteResource : RcsRemoteResourceObject = resource
    private var provider: Provider? = null

    init {
        presenceHandle=subscribePresence(resource.address)
        checkConnectionAfter(20)
    }

    fun subscribePresence(address : String) : OcPresenceHandle{
        Log.d("IOTIVITY_CONNECTION", "START CONSUMER AND SUBSCRIBE TO UNICAST PRESENCE")
        return OcPlatform.subscribePresence(address,
                EnumSet.of(OcConnectivityType.CT_IP_USE_V6), this)
    }


    override fun onPresence(ocPresenceStatus: OcPresenceStatus?, nonce: Int, hostAddress: String?) {
        when (ocPresenceStatus) {
            OcPresenceStatus.STOPPED,OcPresenceStatus.TIMEOUT -> {
                lostConnection()
                notifyObserver(OnDeviceDisconnectedMessage())
            }
        }
        Log.d("IOTIVITY_CONNECTION","RECEIVE PRESENCE ${ocPresenceStatus} FROM ${hostAddress}")

    }


    fun setProvider(pr: Provider) {
        provider=pr
        provider!!.subscribe()
    }

    fun getProvider()=provider

    override fun lostConnection(){
        try {
            if (provider != null && provider!!.providerState!=Provider.ProviderState.DENY && provider!!.providerState!=Provider.ProviderState.STOPPED)
                provider!!.unsubscribe()
            Log.d("IOTIVITY_CONNECTION", "LOST CONNECTION")
            if (presenceHandle != null)
                OcPlatform.unsubscribePresence(presenceHandle)
            remoteResource.destroy()
        }catch (ex:NSException) {
            Log.d("IOTIVITY_ERROR", "Connection end error: ${ex.message.toString()}")
        }
    }


    override fun sendCommand(value: String, parameterMap: HashMap<String, String>) {
        // "COMMAND" -> NOME COMANDO
        // "NOME VARIABILE" -> VALORE
        // ECC
        var attributes = RcsResourceAttributes()
        attributes.put("COMMAND",value)
        parameterMap.keys.forEach {
            attributes.put(it,parameterMap[it])
        }
        remoteResource.setRemoteAttributes(attributes
        ) { attrs,eCode ->
            Log.d("IOTIVITY_CONNECTION","Command executed")
            //Ack received, can now start consumer and subscribe presence
        }
        //Set waiting new state
        waitingNewState = true
    }

    override fun isNotReady() : Boolean{
        return provider==null || provider!!.providerId.isEmpty() ||
                provider!!.providerState==Provider.ProviderState.STOPPED || provider!!.providerState==Provider.ProviderState.DENY
    }

    private fun checkConnectionAfter(seconds:Int){
        Thread{
            sleep(seconds)
            if(isNotReady()) {
                Log.d("IOTIVITY_CONNECTION","provider is not ready after ${seconds} seconds")
                lostConnection()
                notifyObserver(OnDeviceDisconnectedMessage())
            }
        }.start()
    }



    /**
     * Static function used to sleep the thread
     */
    companion object {
        fun sleep(seconds: Int) {
            try {
                Thread.sleep((seconds * 1000).toLong())
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }


}