package iotlab.rita.iotlabthesis.model.rules

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.SmartHome

class Consequence {

    private lateinit var deviceName: String
    private lateinit var device: Device
    private lateinit var command: String
    private lateinit var parameters : HashMap<String,String>
    private lateinit var lambda : ()->Unit
    private var correctedSaved : Boolean = false
    private lateinit var label : String

    constructor()

    constructor(map:HashMap<String,Any>,smartHome: SmartHome){
        deserialize(map,smartHome)
    }

    constructor(map:LinkedTreeMap<String,Any>,smartHome: SmartHome){
        deserialize(map,smartHome)
    }

    fun getLabel() = label

    fun setCorrectedSaved(value: Boolean){
        correctedSaved = value
    }
    fun isCorrectedSaved() : Boolean = correctedSaved

    fun setParameters(devName:String,com: String, par : HashMap<String,String>){
        deviceName = devName
        command = com
        parameters = par
        label = "Apply command ${command} setting "
        parameters.forEach{
            label = label+ it.key + ":" +it.value +","
        }
        label += " to ${deviceName}"
    }

    fun setUnitCommand(dev: Device){
        device = dev
        lambda = {device.getCommunicationProtocol().sendCommand(command,parameters)}
    }

    fun applyConsequence() = lambda()


    fun updateConsequence(smartHome:SmartHome){
        var tempdev = smartHome.getAllDevices().filter { it.getUniqueID().equals(deviceName) }.firstOrNull()
        if(tempdev!=null){
            setUnitCommand(tempdev)
            setCorrectedSaved(true)
            Log.d("RULE_CONFIGURATION","Consequece rule correct saved: ${label}")
        }else{
            setCorrectedSaved(false)
            Log.d("RULE_CONFIGURATION","Consequence rule not saved: ${label}")
        }
    }


    fun getSerialization():HashMap<String,Any>{
        var map : HashMap<String,Any> = HashMap()
        map.put("device",deviceName)
        map.put("command",command)
        map.put("parameters",parameters)
        return map
    }

    fun deserialize(map: HashMap<String, Any>,smartHome: SmartHome){
        val devname = map["device"] as String
        val commandname = map["command"] as String
        val param : HashMap<String,String>
        if(map["parameters"] != null){
            param = map["parameters"] as HashMap<String,String>
        }
        else param = HashMap()
        setParameters(devname,commandname,param)
        updateConsequence(smartHome)
    }


    fun deserialize(map: LinkedTreeMap<String,Any>,smartHome: SmartHome){
        val devname = map["device"] as String
        val commandname = map["command"] as String
        var param : HashMap<String,String> = HashMap()
        if(map["parameters"] != null){
            val treeparam = map["parameters"] as LinkedTreeMap<String,String>
            treeparam.forEach{
                param.put(it.key,it.value)
            }
        }
        else param = HashMap()
        setParameters(devname,commandname,param)
        updateConsequence(smartHome)
    }
}