package iotlab.rita.iotlabthesis.model.home_resources

import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage

class PowerConsumptionResource : HomeResource<Int>() {

    init {
        super.setValue(0)
        super.setName("PowerConsumptionResource")
    }

    override fun reset() {
        super.resetValue(0)
    }

    override fun update(message: ResourceMessage<Int>) {
        message.perform(this)
    }


    override fun setValue(newValue: Int) {
        //Accumulo la power consumption
        super.setValue(newValue+super.getValue()!!)
    }
}