package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage
import java.time.LocalDateTime

class EnergyCollectedResource : HomeResource<Float>() {


    init {
        super.setValue(0F)
        super.setName("EnergyCollectedResource")
    }

    /*
        Accumulate energy
     */
    override fun setValue(newValue: Float) {
        super.setValue(newValue+super.getValue()!!)
    }

    override fun reset() {
        super.resetValue(0F)
    }


    override fun update(message: ResourceMessage<Float>) {
        var log  = "Old value: [${super.getValue()}]"
        message.perform(this)
        log = log + " New value [${super.getValue()}]"
        Log.d("RESOURCE_UPDATE",log)
    }


}