package iotlab.rita.iotlabthesis.model.home_resources

import iotlab.rita.iotlabthesis.device.device_resources.EnergyCollectedDeviceResource
import iotlab.rita.iotlabthesis.device.device_resources.PowerConsumptionDeviceResource
import iotlab.rita.iotlabthesis.device.device_resources.TemperatureDeviceResource
import java.lang.Exception

abstract class ResourceCollectionInterface {

    fun getTemperatureDeviceResource() : TemperatureDeviceResource {
        var temperatureDeviceResource  = TemperatureDeviceResource()
        temperatureDeviceResource.registerObserver(getTemperature())
        getTemperature().setIsActive(true)
        return temperatureDeviceResource
    }

    fun getEnergyCollectedDeviceResource() : EnergyCollectedDeviceResource {
        var energyCollectedDeviceResource  = EnergyCollectedDeviceResource()
        energyCollectedDeviceResource.registerObserver(getEnergyCollected())
        getEnergyCollected().setIsActive(true)
        return energyCollectedDeviceResource
    }

    fun getPowerConsumptionDeviceResource (): PowerConsumptionDeviceResource {
        var powerDeviceResource = PowerConsumptionDeviceResource()
        powerDeviceResource.registerObserver(getPowerConsumption())
        getPowerConsumption().setIsActive(true)
        return powerDeviceResource
    }


    open protected fun getTemperature() : TemperatureResource {throw Exception("Illegal implementation")}
    open protected fun getEnergyCollected() : EnergyCollectedResource {throw Exception("Illegal implementation")}
    open protected fun getPowerConsumption(): PowerConsumptionResource {throw Exception("Illegal implementation")}
}