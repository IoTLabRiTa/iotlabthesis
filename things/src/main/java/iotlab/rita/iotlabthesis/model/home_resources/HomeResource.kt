package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import iotlab.rita.iotlabthesis.model.rules.Antecedent
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage
import java.lang.Exception
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit


abstract class HomeResource<T> : MyObserver<ResourceMessage<T>>, Antecedent{


    private lateinit var name:String
    private var value : T? = null
    private lateinit var lastTimestamp  : LocalDateTime //Oppure mi salvo la liste di tutti i cambiamenti?
    private var isActive : Boolean = false //Indica se c'è almeno un dispositivo che la fornisce

    init {
        setLastTimestamp(getCurrentTimestamp())
    }
    fun getName() = name
    fun getValue() = value
    fun getLastTimestamp() = lastTimestamp
    fun isActive() = isActive


    open fun setValue(newValue: T){
        value = newValue
        try {
            Log.d("RESOURCE_UPDATE_TS","DENTRO AL TRY")
            setLastTimestamp(getCurrentTimestamp())
        }catch (e:Exception){
            Log.d("RESOURCE_UPDATE","Error to define currentTimestamp: ${e.message} ")
        }

    }

    fun resetValue(newValue: T){
        value = newValue
    }

    abstract fun reset()

    fun setLastTimestamp(currentTimestamp : LocalDateTime){
        lastTimestamp = currentTimestamp
        //TODO Notify Observers
    }

    fun setIsActive(value : Boolean){
        isActive = value
    }

    fun setName(name : String){
        this.name = name
    }

    fun getCurrentTimestamp() : LocalDateTime{
        val date = LocalDateTime.now()
        return date
        //val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
        //val text = date.format(formatter)
        //return LocalDateTime.parse(text, formatter)
    }


    //TODO RIMUOVERE
    fun getElapsedTime() : LocalDateTime{
        val fromDateTime = lastTimestamp
        val toDateTime = getCurrentTimestamp()
        var resultDateTime = LocalDateTime.from(fromDateTime)

        val years = resultDateTime.until(toDateTime, ChronoUnit.YEARS)
        resultDateTime = resultDateTime.plusYears(years)

        val months = resultDateTime.until(toDateTime, ChronoUnit.MONTHS)
        resultDateTime = resultDateTime.plusMonths(months)

        val days = resultDateTime.until(toDateTime, ChronoUnit.DAYS)
        resultDateTime = resultDateTime.plusDays(days)


        val hours = resultDateTime.until(toDateTime, ChronoUnit.HOURS)
        resultDateTime = resultDateTime.plusHours(hours)

        val minutes = resultDateTime.until(toDateTime, ChronoUnit.MINUTES)
        resultDateTime = resultDateTime.plusMinutes(minutes)

        val seconds = resultDateTime.until(toDateTime, ChronoUnit.SECONDS)

        Log.d("TIME",years.toString() + " years " +
                months + " months " +
                days + " days " +
                hours + " hours " +
                minutes + " minutes " +
                seconds + " seconds.")

        return resultDateTime
    }

    override fun getSerialization(): HashMap<String,String>{
        var map : HashMap<String, String> = HashMap()
        map.put("resourceName", name)
        map.put("lastTimestamp",lastTimestamp.toString())
        map.put("isActive",isActive.toString())
        map.put("value",value.toString())
        return map
    }

    override fun getAntecedentID(): String = getName()


}
