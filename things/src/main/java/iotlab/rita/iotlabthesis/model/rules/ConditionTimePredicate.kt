package iotlab.rita.iotlabthesis.model.rules

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome
import java.text.SimpleDateFormat
import java.util.*
import java.util.function.Predicate


class ConditionTimePredicate : ConditionRule {

    constructor():super()

    constructor(map:HashMap<String,String>,smartHome: SmartHome):super(map,smartHome)
    constructor(map: LinkedTreeMap<String, String>, smartHome: SmartHome):super(map,smartHome)

    override fun isTimeBased() = true

    override fun setParameters(op: String, attribute: String, value: String, space: String, antname: String) {
        super.setParameters(op, attribute, value, space, antname)
        label = "${op} ${antname}"
    }

    override fun check(): Boolean {
        try {
            val calendar = Calendar.getInstance()
            var sdf = SimpleDateFormat("d MMM yyyy HH:mm", Locale.ITALY)
            return predicate.test(sdf.format(calendar.time))
        }catch (ex: Exception){
            Log.d("RULE_CONFIGURATION","Error in execute predeicate: ${ex.message}")
            return false
        }

    }

    override fun setPredicate(ant: Antecedent) {
        antecedent = ant
        var sdf = SimpleDateFormat("d MMM yyyy HH:mm", Locale.ITALY)
        var calendar = Calendar.getInstance()
        calendar.time = sdf.parse(antecedent.getSerialization()["date"])
        predicate = Predicate {
            var comparedCalendar = Calendar.getInstance()
            comparedCalendar.time = sdf.parse(it)
            comparedCalendar.after(calendar)
        }
        //TODO FAR PARTIRE ALLARME
    }

    override fun updateCondition(smartHome: SmartHome) {
        try{
            var sdf = SimpleDateFormat("d MMM yyyy HH:mm", Locale.ITALY)
            var calendar = Calendar.getInstance()
            calendar.time = sdf.parse(antName)
            var timeAntecedent = TimeAntecedent(calendar,sdf)
            setPredicate(timeAntecedent)
            setcorrectedSaved(true)
        }catch (ex: Exception){
            setcorrectedSaved(false)
            Log.d("RULE_CONFIGURATION","Error to config your time dependent rule")
        }

    }


}