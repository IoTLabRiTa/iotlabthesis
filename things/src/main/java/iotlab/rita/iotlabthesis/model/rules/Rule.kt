package iotlab.rita.iotlabthesis.model.rules

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome

class Rule {

    private var id: Int = 0
    private var repeat : Boolean = true
    private var activated : Boolean = true
    private var conditionList : ArrayList<ConditionRule> = ArrayList()
    private var consequenceList : ArrayList<Consequence> = ArrayList()
    private var ready = false

    fun getID() = id
    fun setID(number : Int){
        id = number
    }

    fun isActivated() = activated
    fun setActivated(value:Boolean){
        activated = value
    }

    fun getConditionList() = conditionList
    fun getConsequenceList() = consequenceList

    fun addConditionPredicate(cond:ConditionRule){
        conditionList.add(cond)
    }

    fun addConsequence(cons:Consequence){
        consequenceList.add(cons)
    }

    fun setReady(value:Boolean) {
        ready = value
    }
    fun checkReady() = consequenceList.fold(true){acc,x -> acc and x.isCorrectedSaved()}

    fun updateRule(smartHome: SmartHome){
        conditionList.forEach { it.updateCondition(smartHome) }
        consequenceList.forEach { it.updateConsequence(smartHome) }
        ready = checkReady()
    }

    fun applyRule(){
        if(conditionList.fold(activated){ acc, x -> acc and x.check() }){
            if(repeat) {
                if(ready){
                    Log.d("RULE_CONFIGURATION","Rule ${id} applied")
                    consequenceList.forEach {
                        it.applyConsequence()
                        Log.d("RULE_CONFIGURATION","Rule ${id}: ${it.getLabel()}")
                    }
                }else Log.d("RULE_CONFIGURATION","Device is not ready, rule ${id} not applied")
            }
            else Log.d("RULE_CONFIGURATION","Rule ${id} already applied")
            repeat = false
        }else{
            repeat = true
            //Log.d("RULE_CONFIGURATION","Rule ${id} not applied, conditions are not satisfied")
            conditionList.forEach {
                Log.d("RULE_CONFIGURATION","Rule ${id}: condition ${it.getConditionLabel()} is ${it.check()}")
            }
        }
    }

    fun getSerialization(): HashMap<String,Any>{
        var map: HashMap<String,Any> = HashMap()
        map.put("isActivated",activated.toString())
        map.put("id",id.toString())
        for (i in 0..conditionList.size-1){
            map.put("condition${i}",conditionList[i].getSerialization())
        }
        for (i in 0..consequenceList.size-1){
            map.put("consequence${i}",consequenceList[i].getSerialization())
        }
        return map
    }

    fun deserialize(map : HashMap<String, Any>,smartHome: SmartHome){
        map.forEach{
            if(it.key.equals("isActivated"))
                activated = (it.value as String).toBoolean()
            else if(it.key.equals("id"))
                id = (it.value as String).toInt()
            else if(it.key.contains("condition")){
                var conditionRule = it.value as HashMap<String,String>
                if(conditionRule["smartSpace"].equals("DateTime"))
                    addConditionPredicate(ConditionTimePredicate(conditionRule,smartHome))
                else addConditionPredicate(ConditionResourcePredicate(conditionRule,smartHome))
            }
            else if(it.key.contains("consequence"))
                addConsequence(Consequence(it.value as HashMap<String,Any>,smartHome))
        }
        ready = checkReady()
    }

    fun deserialize(map : LinkedTreeMap<String,Any>,smartHome: SmartHome){
        map.forEach{
            if(it.key.equals("isActivated"))
                activated = (it.value as String).toBoolean()
            else if(it.key.equals("id"))
                id = (it.value as String).toInt()
            else if(it.key.contains("condition")){
                var conditionRule = it.value as LinkedTreeMap<String,String>
                if(conditionRule["smartSpace"].equals("DateTime"))
                    addConditionPredicate(ConditionTimePredicate(conditionRule,smartHome))
                else addConditionPredicate(ConditionResourcePredicate(conditionRule,smartHome))
            }
            else if(it.key.contains("consequence"))
                addConsequence(Consequence(it.value as LinkedTreeMap<String, Any>,smartHome))
        }
        ready = checkReady()
    }
}