package iotlab.rita.iotlabthesis.model

import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.rules.Rule
import iotlab.rita.iotlabthesis.pattern_observer.message.OnRuleAddedMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.OnRuleDeletedMessage
import java.util.zip.DeflaterInputStream


class SmartHome(nameSpace: String) : SmartSpace(nameSpace) {

    private var rooms: ArrayList<SmartRoom> = ArrayList()
    private var rules : ArrayList<Rule> = ArrayList()
    private var RULES_ID = 0

    init {
        addRoom(SmartRoom("Bathroom"))
        addRoom(SmartRoom("Bedroom"))
        addRoom(SmartRoom("Kitchen"))
    }

    fun getRules() = rules
    fun addRule(newRule: Rule){
        newRule.setID(++RULES_ID)
        rules.add(newRule)
        notifyObserver(OnRuleAddedMessage(newRule))
    }

    fun removeRule(oldRule : Rule){
        rules.remove(oldRule)
        notifyObserver(OnRuleDeletedMessage(oldRule))
    }

    fun applyRules(){
        rules.forEach { it.applyRule() }
    }

    fun addRoom(newRoom:SmartRoom){
        rooms.add(newRoom)
    }

    fun getRooms() = rooms

    /*
        Retrieve all devices in home and rooms
     */
    fun getAllDevices() : ArrayList<Device>{
        var allDevices: ArrayList<Device> = ArrayList()
        rooms.forEach {
            allDevices.addAll(it.getDevices())
        }
        allDevices.addAll(getDevices())
        return allDevices
    }

    /*
        return a device from anywhere
     */
    fun getDeviceAnyWhere(deviceID : String):Device{
        return getAllDevices().filter {
            it.getUniqueID().equals(deviceID)
        }.first()
    }

    fun removeDevice(device: Device) : Boolean{
        return removeFromSmartSpace(device) != null
    }

    fun removeFromSmartSpace(device: Device) : SmartSpace?{
        if(!this.getDevices().remove(device)){
            this.rooms.forEach {
                 if(it.getDevices().remove(device))
                     return it
            }
        }else{
            return this
        }
        return null
    }

    fun getSmartSpaceOfDevice(device: Device):SmartSpace?{
        if(getDevices().contains(device))
            return this
        else
            getRooms().forEach {
                if(it.getDevices().contains(device))
                    return it
            }
        return null //Should not happen
    }

    fun initRules(newrules: HashMap<String, Any>) {
        var maxID =0
        newrules.forEach{
            var newRule = Rule()
            newRule.deserialize(it.value as HashMap<String, Any>,this)
            if(newRule.getID()>maxID)
                maxID = newRule.getID()
            rules.add(newRule)
        }
        RULES_ID = maxID
    }

    fun updateRules(){
        rules.forEach {
            it.updateRule(this)
        }
    }


}