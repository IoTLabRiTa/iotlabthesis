package iotlab.rita.iotlabthesis.model.home_resources

import iotlab.rita.iotlabthesis.model.rules.Antecedent

class ResourceCollection : ResourceCollectionInterface()  {

    private val temperature = TemperatureResource()
    private val energyCollected = EnergyCollectedResource()
    private val powerConsumption = PowerConsumptionResource()
    private var resourceList : ArrayList<Antecedent>

    init {
        resourceList = ArrayList()
        resourceList.add(temperature)
        resourceList.add(energyCollected)
        resourceList.add(powerConsumption)
    }

    override fun getTemperature() = temperature
    override fun getEnergyCollected() = energyCollected
    override fun getPowerConsumption() = powerConsumption

    fun getTempertureResource() = temperature
    fun getEnergyCollectedResource() = energyCollected
    fun getPowerConsumptionResource() = powerConsumption

    fun resetResources(){
        temperature.reset()
        energyCollected.reset()
        powerConsumption.reset()
    }

    override fun toString(): String {
        return "ResourceCollection(temperature=${temperature.getValue()}, energyCollected=${energyCollected.getValue()}" +
                ",powerConsumption=${powerConsumption.getValue()})"
    }

    fun getSerialization(): ArrayList<HashMap<String,String>>{
        var result : ArrayList<HashMap<String,String>> = ArrayList()
        result.add(temperature.getSerialization())
        result.add(energyCollected.getSerialization())
        result.add(powerConsumption.getSerialization())
        return result
    }

    fun getResourceList() = resourceList


}