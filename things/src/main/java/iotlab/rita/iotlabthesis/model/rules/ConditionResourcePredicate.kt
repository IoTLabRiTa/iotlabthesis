package iotlab.rita.iotlabthesis.model.rules

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import java.util.function.Predicate

class ConditionResourcePredicate : ConditionRule {

    constructor():super()

    constructor(map:HashMap<String,String>,smartHome: SmartHome):super(map,smartHome)
    constructor(map: LinkedTreeMap<String, String>, smartHome: SmartHome):super(map,smartHome)

    override fun isTimeBased() = false

    override fun setParameters(op:String,attribute:String,value:String,space: String, antname : String){
        super.setParameters(op, attribute, value, space, antname)
        label = "${smartSpace} ${antName}: ${compareAttribute} ${operator} ${comparedValue}"
    }

    override fun setPredicate(ant:Antecedent){
        antecedent = ant
        when(operator){
            ">" -> predicate = Predicate{it.toDouble() > comparedValue.toDouble()  }
            ">=" -> predicate = Predicate{it.toDouble() >= comparedValue.toDouble()  }
            "<" -> predicate = Predicate{it.toDouble() < comparedValue.toDouble()  }
            "<=" -> predicate = Predicate{it.toDouble() <= comparedValue.toDouble()  }
            "=" -> predicate = Predicate{it.equals(comparedValue)  }
            else -> predicate = Predicate{it.equals(comparedValue) }
        }
        Log.d("RULE_CONFIGURATION","Predicate is been correctly created")
    }

    override fun check():Boolean {
        if(correctedSaved){
            return predicate.test(antecedent.getSerialization()[compareAttribute]!!.replace(".0",""))
        }else
            return false
    }



    override fun updateCondition(smartHome: SmartHome) {
        var spaceList = ArrayList<SmartSpace>()
        spaceList.add(smartHome)
        spaceList.addAll(smartHome.getRooms())
        var spaceFound =spaceList.filter { it.getName().equals(smartSpace) }.firstOrNull()
        if(spaceFound != null) {
            var resourceList = ArrayList<Antecedent>()
            resourceList.addAll(spaceFound.getDevices())
            resourceList.addAll(spaceFound.getResources().getResourceList())
            var ant = resourceList.filter { it.getAntecedentID().equals(antName) }.firstOrNull()
            if (ant != null) {
                setPredicate(ant)
                setcorrectedSaved(true)
                Log.d("RULE_CONFIGURATION","Condition rule correct saved: ${label}")
            } else {
                setcorrectedSaved(false)
                Log.d("RULE_CONFIGURATION","Condition rule issue: not correct saved: ${label}")
            }
        }else Log.d("RULE_CONFIGURATION","Error to retrieve rule from server: space not found : ${label}")
    }


}