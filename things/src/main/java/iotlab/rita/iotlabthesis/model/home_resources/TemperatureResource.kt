package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ResourceMessage
import java.time.LocalDateTime

class TemperatureResource : HomeResource<Float>() {


    init {
        super.setValue(0F)
        super.setName("TemperatureResource")
    }
    /*
        Check if 1 minutes has passed from the last value set. Otherwise perform the average between them
     */
    override fun setValue(newValue: Float) {
        super.setValue(newValue)
        /*
        //TODO rivedere

        val maxElapsedTime = super.getCurrentTimestamp().minusMinutes(1)//LocalDateTime.of(0,0,0,0,1,0)
        if (maxElapsedTime.isAfter(super.getLastTimestamp()) || super.getValue() ==0F){
            Log.d("RESOURCE_UPDATE_TS","DENTRO ALL'IF")
            super.setValue(newValue)
        }
        else{
            Log.d("RESOURCE_UPDATE_TS","DENTRO ALL'ELSE")
            super.setValue((newValue+super.getValue()!!)/2)
        }*/
        //super.setValue(newValue)
    }

    override fun reset() {
        super.resetValue(0F)
    }

    override fun update(message: ResourceMessage<Float>) {
        var log  = "Old value: [${super.getValue()}]"
       message.perform(this)
        log = log + " New value [${super.getValue()}]"
        Log.d("RESOURCE_UPDATE",log)
    }
}