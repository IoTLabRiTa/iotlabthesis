package iotlab.rita.iotlabthesis.model

import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.home_resources.ResourceCollection
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage

abstract class SmartSpace(nameSpace:String) : MyObservable<MyMessage>() {

    private var name : String = nameSpace
    private var devices : ArrayList<Device> = ArrayList()
    private var resources = ResourceCollection()

    fun addDevice(device: Device){
        devices.add(device)
    }

    fun getDevices() = devices
    fun getResources() = resources
    fun getName() = name


}