package iotlab.rita.iotlabthesis.activity

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.controller.HomeControlUnit

class MainActivity : AppCompatActivity() {

    private lateinit var homeCU: HomeControlUnit
    private lateinit var showRulesBT : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        homeCU = HomeControlUnit(this)

        showRulesBT = findViewById(R.id.showRulesBT)
        showRulesBT.setOnClickListener {
            setContentView(R.layout.rules_and_rooms)
            homeCU.setRules()
        }
    }


    override fun onDestroy() {
        super.onDestroy()

        //TODO fare close connection per iotivity
        //Stop discovery
        homeCU.getCommunicationUnit().getIotivityCommunicationUnit().stopDiscovery()
        //Stop consumer
        homeCU.getCommunicationUnit().getIotivityCommunicationUnit().stopConsumer()

        homeCU.getCommunicationUnit().getOpenThreadCommunicationUnit().closeConnection()

    }
}