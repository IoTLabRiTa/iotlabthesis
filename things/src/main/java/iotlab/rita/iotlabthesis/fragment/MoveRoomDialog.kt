package iotlab.rita.iotlabthesis.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.DeviceChangeSpaceMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ProtocolMessage
import kotlinx.android.synthetic.main.activity_main.view.*

class MoveRoomDialog() : DialogFragmentObservable<ProtocolMessage>(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val inflater = activity!!.layoutInflater
        var view = inflater.inflate(R.layout.move_room_dialog, null)

        builder.setView(view)

        var moveRoomSpinner = view.findViewById(R.id.moveRoom) as Spinner

        var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item, arguments!!.getStringArrayList("smartspaces"))
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        moveRoomSpinner.adapter=spinnerAdapter

        var okListener= DialogInterface.OnClickListener { dialog, id ->
            notifyObserver(DeviceChangeSpaceMessage(moveRoomSpinner.selectedItem as String))
        }

        val alertDialog = builder.setTitle("Move device")
                .setPositiveButton("yes",okListener)
                .setNegativeButton("cancel", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                // Create the AlertDialog object and return it
                .create()
        return alertDialog
    }


}