package iotlab.rita.iotlabthesis.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import iotlab.rita.iotlabthesis.device.Command
import android.support.constraint.ConstraintSet
import android.support.constraint.ConstraintLayout
import android.view.TextureView
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ProtocolMessage


class CommandDialog : DialogFragmentObservable<ProtocolMessage>() {

    @SuppressLint("NewApi")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var command : Command = arguments!!.get("command") as Command
        val mConstraintLayout = ConstraintLayout(context)
        mConstraintLayout.id=View.generateViewId()
        var set = ConstraintSet()

        var okListener: DialogInterface.OnClickListener? = null
        //TODO TOTALMENTE DA SISTEMARE
        if(command.getParameters().isEmpty()){
            val description= TextView(context)
            description.id = View.generateViewId()
            description.text = command.getDescription()
            mConstraintLayout.addView(description,0)
            set.clone(mConstraintLayout)
            set.connect(description.id, ConstraintSet.TOP, mConstraintLayout.id, ConstraintSet.TOP, 16)
            set.connect(description.id, ConstraintSet.LEFT, mConstraintLayout.id, ConstraintSet.LEFT, 16)
            set.applyTo(mConstraintLayout)
            okListener= DialogInterface.OnClickListener{dialog, id ->
                var map: HashMap<String,String> = HashMap()
                notifyObserver(CommandMessage(command.getName(),map))
            }
        }
        else{
            command.getParameters().forEach{
                //TODO SISTEMARE PER PIU PARAMETRI
                //Add view -> Clone set -> connect -> applyto
                //Description of parameter
                val description= TextView(context)
                description.id = View.generateViewId()
                description.text = it.nameParameter
                mConstraintLayout.addView(description,0)
                set.clone(mConstraintLayout)
                set.connect(description.id, ConstraintSet.TOP, mConstraintLayout.id, ConstraintSet.TOP, 16)
                set.connect(description.id, ConstraintSet.LEFT, mConstraintLayout.id, ConstraintSet.LEFT, 16)
                set.applyTo(mConstraintLayout)

                if(it.graphic.equals("SeekBar")){
                    //Parameters
                    val seekBar = SeekBar(context)
                    seekBar.id = View.generateViewId()
                    //seekBar.min =  it.min.toInt()
                    seekBar.max = it.max.toInt()
                    seekBar.setProgress(1,true)
                    mConstraintLayout.addView(seekBar,0)
                    seekBar.layoutParams.width = 0
                    seekBar.requestLayout()

                    //seekBar.layoutParams.width = 400
                    //seekBar.requestLayout()
                    set.clone(mConstraintLayout)
                    set.connect(seekBar.id, ConstraintSet.TOP, description.id, ConstraintSet.TOP, 0)
                    //set.connect(seekBar.id, ConstraintSet.LEFT,description.id, ConstraintSet.RIGHT, 24)
                    set.connect(seekBar.id, ConstraintSet.BOTTOM, description.id, ConstraintSet.BOTTOM, 0)
                    //set.connect(seekBar.id, ConstraintSet.RIGHT, mConstraintLayout.id, ConstraintSet.RIGHT, 24)

                    set.connect(seekBar.id, ConstraintSet.START,description.id,ConstraintSet.END,24)
                    set.connect(seekBar.id, ConstraintSet.END,mConstraintLayout.id,ConstraintSet.END,24)

                    set.applyTo(mConstraintLayout)
                    okListener= DialogInterface.OnClickListener{dialog, id ->
                        var map: HashMap<String,String> = HashMap()
                        map.put("temperature",seekBar.progress.toString())
                        notifyObserver(CommandMessage(command.getName(),map))
                    }

                }
                else if(it.graphic.equals("Switch")){
                    //Parameters
                    val switch = Switch(context)
                    switch.id = View.generateViewId()
                    mConstraintLayout.addView(switch,0)
                    //switch.layoutParams.width = 0
                    switch.requestLayout()

                    //seekBar.layoutParams.width = 400
                    //seekBar.requestLayout()
                    set.clone(mConstraintLayout)
                    set.connect(switch.id, ConstraintSet.TOP, description.id, ConstraintSet.TOP, 0)
                    //set.connect(seekBar.id, ConstraintSet.LEFT,description.id, ConstraintSet.RIGHT, 24)
                    set.connect(switch.id, ConstraintSet.BOTTOM, description.id, ConstraintSet.BOTTOM, 0)
                    //set.connect(seekBar.id, ConstraintSet.RIGHT, mConstraintLayout.id, ConstraintSet.RIGHT, 24)

                    set.connect(switch.id, ConstraintSet.START,description.id,ConstraintSet.END,24)
                    set.connect(switch.id, ConstraintSet.END,mConstraintLayout.id,ConstraintSet.END,24)

                    set.applyTo(mConstraintLayout)
                    okListener= DialogInterface.OnClickListener{dialog, id ->
                        var map: HashMap<String,String> = HashMap()
                        map.put("onOffState",switch.isChecked.toString())
                        notifyObserver(CommandMessage(command.getName(),map))
                    }
                }else if(it.graphic.equals("Spinner")){
                    var listValue = ArrayList(it.min.split("/"))
                    var spinner = Spinner(context)
                    var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context, R.layout.spinner_item,listValue)
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinner.adapter=spinnerAdapter
                    spinner.id = View.generateViewId()
                    mConstraintLayout.addView(spinner,0)

                    spinner.requestLayout()

                    set.clone(mConstraintLayout)
                    set.connect(spinner.id, ConstraintSet.TOP, description.id, ConstraintSet.TOP, 0)
                    set.connect(spinner.id, ConstraintSet.BOTTOM, description.id, ConstraintSet.BOTTOM, 0)

                    set.connect(spinner.id, ConstraintSet.START,description.id,ConstraintSet.END,24)
                    set.connect(spinner.id, ConstraintSet.END,mConstraintLayout.id,ConstraintSet.END,24)

                    set.applyTo(mConstraintLayout)
                    okListener= DialogInterface.OnClickListener{dialog, id ->
                        var map: HashMap<String,String> = HashMap()
                        map.put("mode",spinner.selectedItem.toString())
                        notifyObserver(CommandMessage(command.getName(),map))
                    }

                }

            }
        }


        val alertDialog = AlertDialog.Builder(context)
                .setView(mConstraintLayout)
                .setTitle(command.getName())
                .setPositiveButton("yes",okListener)
                .setNegativeButton("cancel", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                // Create the AlertDialog object and return it
                .create()

        return alertDialog
    }

}