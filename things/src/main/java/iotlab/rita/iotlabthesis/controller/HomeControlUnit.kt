package iotlab.rita.iotlabthesis.controller

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.communication.CommunicationUnit
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.SolarPanel
import iotlab.rita.iotlabthesis.device.Thermostat
import iotlab.rita.iotlabthesis.expandable_listview.RoomsExpandableListAdapter
import iotlab.rita.iotlabthesis.expandable_listview.RulesGUI
import iotlab.rita.iotlabthesis.fragment.CommandDialog
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import java.util.*
import kotlin.collections.ArrayList


class HomeControlUnit(context: Context) : MyObserver<MyMessage> {

    private var communicationUnit : CommunicationUnit
    private var smartHome : SmartHome
    /*
       TODO Da eliminare/spostare: Utilizzati solo come LOG temporaneo
     */
    private var logTV : TextView
    private  var activity : AppCompatActivity
    private var expandableListView: ExpandableListView
    private var expandableListAdapter: RoomsExpandableListAdapter
    var fragmentManager : FragmentManager

    //Grafica per le regole
    private var rulesGUI : RulesGUI? = null


    init {
        //Definizione activity usata per la nearby
        activity = context as AppCompatActivity
        smartHome = SmartHome("Home")
        smartHome.registerObserver(this)
        communicationUnit= CommunicationUnit(context,activity,this)

        communicationUnit.getFirebaseCommunicationUnit().getRules(smartHome)

        fragmentManager = activity.supportFragmentManager
        logTV = (context as Activity).findViewById(R.id.LOGTV)
        logTV.setMovementMethod(ScrollingMovementMethod())

        expandableListView = (context as Activity).findViewById(R.id.expandableListView)

        var smartSpaces : ArrayList<SmartSpace> = ArrayList()
        smartSpaces.add(smartHome)
        smartHome.getRooms().forEach {
            smartSpaces.add(it)
        }
        expandableListAdapter = RoomsExpandableListAdapter(context,smartSpaces,fragmentManager)
        expandableListView.setAdapter(expandableListAdapter)

    }

    fun setRules(){
        rulesGUI = RulesGUI(activity,smartHome)
    }


    override fun update(message: MyMessage) {
        message.perform(this)
    }

    fun getSmartHome() = smartHome
    fun getCommunicationUnit() = communicationUnit

    /*
        TODO Funzioni grafiche da rimuovere dalla cu
     */
    fun log(logString : String){

        if(logTV !=null) {
            activity.runOnUiThread {
                if (logTV.text.contains("EmptyLog"))
                    logTV.text = logString
                else
                    logTV.append("\n ${logString}")
            }
        }
    }

    fun updateLV(){
        if(expandableListAdapter != null) {
            activity.runOnUiThread {
                (expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }
        }
        if(rulesGUI != null){
            rulesGUI!!.update()
        }
    }

}
