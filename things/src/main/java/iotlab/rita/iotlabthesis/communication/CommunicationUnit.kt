package iotlab.rita.iotlabthesis.communication

import android.app.Activity
import android.content.Context
import iotlab.rita.iotlabthesis.controller.HomeControlUnit

class CommunicationUnit(context: Context,activity: Activity,homeCU:HomeControlUnit) {

    private var ioTivityCU: IoTivityCommunicationUnit
    private lateinit var openThreadCU: OpenThreadCommunicationUnit
    private var nearbyCU : NearbyCommunicationUnit
    private var firebaseCU : FirebaseCommunicationUnit
    private var mqqtCU : MQTTCommunicationUnit

    init {

        nearbyCU = NearbyCommunicationUnit(activity)
        nearbyCU.registerObserver(homeCU)
        firebaseCU = FirebaseCommunicationUnit()
        mqqtCU = MQTTCommunicationUnit(context)
        mqqtCU.registerObserver(homeCU)
        ioTivityCU= IoTivityCommunicationUnit(context)
        ioTivityCU.registerObserver(homeCU)
        openThreadCU = OpenThreadCommunicationUnit(context)
        openThreadCU.registerObserver(homeCU)
    }

    fun getIotivityCommunicationUnit() = ioTivityCU
    fun getOpenThreadCommunicationUnit() = openThreadCU
    fun getNearbyCommunicationUnit() = nearbyCU
    fun getFirebaseCommunicationUnit() = firebaseCU
    fun getMQTTCommunicationUnit() = mqqtCU



}