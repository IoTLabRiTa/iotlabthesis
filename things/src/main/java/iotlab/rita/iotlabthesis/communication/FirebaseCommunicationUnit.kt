package iotlab.rita.iotlabthesis.communication

import com.google.firebase.database.*
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.model.rules.Rule

class FirebaseCommunicationUnit  {

    private val home = "home"
    private val homeChildDevices = "devices"
    private val homeChildSmartSpaces = "smartSpaces"
    private val rules = "rules"

    private var database : DatabaseReference = FirebaseDatabase.getInstance().reference

    init{
        //Clean database
        database.child(home).child(homeChildDevices).setValue(null)
        database.child(home).child(homeChildSmartSpaces).setValue(null)
    }

    fun storeRule(rule : Rule) {
        database.child(home).child(rules).child("rule " + rule.getID()).setValue(rule.getSerialization())
    }

    fun deleteRule(id :Int) {
        database.child(home).child(rules).child("rule " + id).removeValue()
    }

    fun getRules(smartHome: SmartHome){
        database.child(home).child(rules).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.getValue() !=null)
                    smartHome.initRules(dataSnapshot.getValue() as HashMap<String, Any>)
            }

            override fun onCancelled(p0: DatabaseError) {
                //Do nothing
            }
        })
    }


    fun storeDevice(map : HashMap<String,String>, id : String){
        database.child(home).child(homeChildDevices).child(id).setValue(map)
    }

    fun deleteDevice(id:String){
        database.child(home).child(homeChildDevices).child(id).removeValue()
    }

    fun storeSmartSpace(smartSpace: SmartSpace){
        /* PRIMA VERSIONE
        var name = smartSpace.getName()
        var mapSmartSpace : HashMap<String,String> = HashMap()
        mapSmartSpace.put("name",name)
        database.child(home).child(homeChildSmartSpaces).child(name).setValue(mapSmartSpace)

        var mapDevices : HashMap<String,String> = HashMap()
        var count = 0;
        smartSpace.getDevices().forEach {
            mapDevices.put("device$count",it.getUniqueID())
            count++
        }
        database.child(home).child(homeChildSmartSpaces).child(name).child(homeChildDevices).setValue(mapDevices)

        smartSpace.getResources().getSerialization().forEach {
            database.child(home).child(homeChildSmartSpaces).child(name).child(homeChildSmartSpacesResources).child(it.get("resourceName")!!).setValue(it)
        }
        */
        //SECONDA VERSIONE
        var name = smartSpace.getName()
        var mapSmartSpace : HashMap<String,Any> = HashMap()
        mapSmartSpace.put("name",name)

        //Store devices
        var mapDevices : HashMap<String,String> = HashMap()
        var count = 0;
        smartSpace.getDevices().forEach {
            mapDevices.put("device$count",it.getUniqueID())
            count++
        }
        mapSmartSpace.put("devices",mapDevices)

        var resourceMap : HashMap<String,Any> = HashMap()
        //Store resources
        smartSpace.getResources().getSerialization().forEach {
            resourceMap.put(it.get("resourceName")!!,it)
        }
        mapSmartSpace.put("resources",resourceMap)
        database.child(home).child(homeChildSmartSpaces).child(name).setValue(mapSmartSpace)

    }

}