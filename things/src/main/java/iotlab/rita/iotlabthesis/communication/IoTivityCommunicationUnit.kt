package iotlab.rita.iotlabthesis.communication

import android.content.Context
import android.util.Log
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.device.communication_protocols.IoTivityProtocol
import iotlab.rita.iotlabthesis.pattern_observer.message.*
import org.iotivity.base.*
import org.iotivity.service.RcsResourceAttributes
import org.iotivity.service.client.RcsAddress
import org.iotivity.service.client.RcsDiscoveryManager
import org.iotivity.service.client.RcsRemoteResourceObject
import org.iotivity.service.ns.common.Message
import org.iotivity.service.ns.common.NSException
import org.iotivity.service.ns.common.SyncInfo
import org.iotivity.service.ns.consumer.ConsumerService
import org.iotivity.service.ns.consumer.Provider
import java.lang.Exception
import kotlin.collections.HashMap


class IoTivityCommunicationUnit(cont:Context) : CommunicationStructure(), ConsumerService.OnProviderDiscoveredListener,
        Provider.OnProviderStateListener,Provider.OnMessageReceivedListener, Provider.OnSyncInfoReceivedListener{



    private val LOG_TAG: String = "IOTIVITY_CONNECTION"
    private val LOG_TAG_ERROR: String = "IOTIVITY_ERROR"
    private val RESOURCE_TYPE = "oic.r.temperature.sensor"
    private lateinit var resourceDiscoveredListener : RcsDiscoveryManager.OnResourceDiscoveredListener
    private var consumerService: ConsumerService
    private lateinit var discoveryTask: RcsDiscoveryManager.DiscoveryTask

    //private lateinit var remoteAttributesReceivedListener: RcsRemoteResourceObject.OnRemoteAttributesReceivedListener

    init {
        configurePlatform(cont)
        consumerService= ConsumerService()
        startConsumer()

    }


    private fun configurePlatform(context: Context) {
        //Platform Config
        try{
            OcPlatform.Configure(PlatformConfig(context,
                    ServiceType.IN_PROC, ModeType.CLIENT_SERVER, //TODO Controllo il tipo
                    QualityOfService.LOW))
            Log.d(LOG_TAG, "Platform Configuration done Successfully")
        } catch (ex: OcException ) {
            Log.d(LOG_TAG_ERROR, "Failed to configure platform ${ex.toString()}");
        }


        resourceDiscoveredListener = RcsDiscoveryManager.OnResourceDiscoveredListener { foundResource ->
            Log.d(LOG_TAG, "Resource Discovered ${foundResource.address}")
            val remoteAttributesReceivedListener = RcsRemoteResourceObject.OnRemoteAttributesReceivedListener { attrs, eCode ->
                val host = foundResource.address
                createDevice(attrs,host,foundResource)
                //Ack di conferma di creazione del device
                var ackAttributes = RcsResourceAttributes()
                ackAttributes.put("ACK_CREATION",true)
                foundResource.setRemoteAttributes(ackAttributes
                ) { attrs,eCode ->
                    Log.d(LOG_TAG,"ACK RECEIVED BY CLIENT ${host}")
                    //Ack received, can now start consumer and subscribe presence
                    rescanProvider()
                }
            }
            foundResource.getRemoteAttributes(remoteAttributesReceivedListener)
            Log.d(LOG_TAG, " GET REQUEST TO ${foundResource.address}")
        }


        discoveryTask = RcsDiscoveryManager.getInstance()
                .discoverResourceByType(RcsAddress.multicast(), RESOURCE_TYPE,
                        resourceDiscoveredListener)

        Log.d(LOG_TAG,"START MULTICAST DISCOVERY")
    }

    fun startConsumer(){
        consumerService.start(this)
    }

    fun stopConsumer(){
        try{
            consumerService.stop()
            Log.d(LOG_TAG,"Stop consumer")
        }catch (e : Exception){
            Log.d(LOG_TAG_ERROR,"Failed to stop consumer service")
        }
    }

    fun rescanProvider(){
        try{
            consumerService.rescanProvider()
        }catch (e : Exception){
            Log.d(LOG_TAG_ERROR,"Failed to rescan providers")
        }
    }

    fun stopDiscovery(){
        if(discoveryTask != null) discoveryTask.cancel()
    }

    override fun onProviderDiscovered(provider: Provider?) {
        if (provider !=null && provider.topicList.topicsList.isNotEmpty()){
            provider.setListener(this,this,this)
            notifyObserver(ProviderMessage(provider))
            Log.d(LOG_TAG,"Provider ${provider.providerId} information received")
        }
        else Log.d(LOG_TAG,"Empty provider information received")
    }

    override fun onProviderStateReceived(pr: Provider.ProviderState?) {
        Log.d("PROVIDER_STATE",pr.toString())
        if(Provider.ProviderState.DENY == pr || Provider.ProviderState.STOPPED == pr) {
            try {
                notifyObserver(OnProviderDisconnectedMessage(pr))
            }catch (ex: NSException){
                Log.d("IOTIVITY_ERROR","Provider unsubscribe error: ${ex.message.toString()}")
            }
            //TODO messaggio di mancato collegamento al device
        }
    }

    override fun onMessageReceived(message: Message?) {
        try {
            Log.d(LOG_TAG,"Message received from ${message!!.providerId} ")
            var map: HashMap<String,String> = HashMap()
            message!!.extraInfo.values.forEach {
                map.put(it.key,it.value.toString())
            }
            notifyObserver(IotivityDeviceStateUpdateMessage(message.providerId,map))
        }catch (e:Exception){
            Log.d("IOTIVITY_ERROR","String: ${message!!.contentText} \t Errore: "+e.message)
            //TODO messaggio di non aggiornamento dello stato del device
        }
    }

    override fun onSyncInfoReceived(p0: SyncInfo?) {
        //TODO("not implemented") To change body of created functions use File | Settings | File Templates.
    }

    override fun onStateReceived() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendCommand() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPresentationReceveid() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reset() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }




    private fun createAttributesMap(attrs: RcsResourceAttributes) : HashMap<String,String>{
        var map: HashMap<String,String> = HashMap()
        attrs.keySet().forEach {
            map.put(it,attrs[it].asString())
        }
        return map
    }

    private fun createDevice(attrs: RcsResourceAttributes, host: String, foundResource: RcsRemoteResourceObject){

            try {
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"].asString()
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState

                notifyObserver(DeviceDiscoveredMessage(state, createAttributesMap(attrs), host, IoTivityProtocol(foundResource)))
            } catch (ex: Exception) {
                Log.d(LOG_TAG_ERROR, "Device Creation: " + ex.message.toString())
            }
    }




}