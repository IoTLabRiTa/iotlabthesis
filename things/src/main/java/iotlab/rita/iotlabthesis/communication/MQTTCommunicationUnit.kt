package iotlab.rita.iotlabthesis.communication

import android.content.Context
import android.util.Log
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.IMqttToken
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.rules.Rule
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.DeviceChangeSpaceMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.PerformReceivedCommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ReceivedRuleMessage
import org.eclipse.paho.client.mqttv3.MqttConnectOptions


class MQTTCommunicationUnit(var context: Context) : MyObservable<MyMessage>() {

    var mqttAndroidClient: MqttAndroidClient

    //val serverUri = "tcp://iot.eclipse.org:1883"
    val serverUri = "tcp://131.175.21.173:1883"



    val clientId = "RiTaAndroidThingsIoTLab1234"
    val subscriptionTopic = "HomeControlUnitAndroidThingsIoTLab"

    /*val username = "xxxxxxx"
    val password = "yyyyyyyyyy"*/

    val TAG = "MQTT_CONNECTION"

    init {
        mqttAndroidClient = MqttAndroidClient(context, serverUri, clientId)
        mqttAndroidClient.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(b: Boolean, s: String) {
                Log.d(TAG,"Connection complete ${s}" )
            }

            override fun connectionLost(throwable: Throwable) {

            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                try{
                    val typeAny : LinkedTreeMap<String,Any> = LinkedTreeMap()
                    var mapAny: LinkedTreeMap<String,Any> = Gson().fromJson(mqttMessage.toString(),typeAny.javaClass)
                    val commandAny = mapAny["command"] as String
                    if(commandAny!!.equals("ActionRule")){
                        notifyObserver(ReceivedRuleMessage(mapAny))
                    }
                    else {
                        val type: HashMap<String, String> = HashMap()
                        var map: HashMap<String, String> = Gson().fromJson(mqttMessage.toString(), type.javaClass)
                        val deviceID = map["uniqueID"]
                        val command = map["command"]
                        map.remove("uniqueID")
                        map.remove("command")
                        Log.d(TAG, "Message received: ${mqttMessage}")
                        if (command.equals("MoveDevice")) {
                            var message = DeviceChangeSpaceMessage(map["to"]!!)
                            message.deviceID = deviceID!!
                            notifyObserver(message)
                        } else {
                            notifyObserver(PerformReceivedCommandMessage(deviceID!!, command!!, map))
                        }
                    }
                }catch (e:Exception){
                    Log.d(TAG, "Error to parse message received: ${mqttMessage}\n error : ${e.message}")
                }

            }

            override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {

            }
        })
        connect()
    }


    private fun connect() {
        val mqttConnectOptions = MqttConnectOptions()
        mqttConnectOptions.isAutomaticReconnect = true
        mqttConnectOptions.isCleanSession = false
        //mqttConnectOptions.connectionTimeout = 60000
        /*mqttConnectOptions.userName = username
        mqttConnectOptions.password = password.toCharArray()*/

        try {

            mqttAndroidClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {

                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.isBufferEnabled = true
                    disconnectedBufferOptions.bufferSize = 100
                    disconnectedBufferOptions.isPersistBuffer = false
                    disconnectedBufferOptions.isDeleteOldestMessages = false
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions)
                    subscribeToTopic()
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d(TAG, "Failed to connect to: $serverUri$exception")
                }
            })


        } catch (ex: MqttException) {
            Log.d(TAG,"mqtt error to connect: ${ex.message}")
        }

    }

    private fun subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    Log.d(TAG, "Subscribed!")
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d(TAG, "Subscribed fail!")
                }
            })

        } catch (ex: MqttException) {
            Log.d(TAG,"mqtt ERROR subscribing: ${ex.message}")
        }

    }

}