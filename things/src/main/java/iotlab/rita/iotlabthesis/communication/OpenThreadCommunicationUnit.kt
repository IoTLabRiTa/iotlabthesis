package iotlab.rita.iotlabthesis.communication

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.util.Log
import com.google.android.things.contrib.driver.lowpan.UartLowpanDriver
import com.google.android.things.lowpan.LowpanInterface
import com.google.android.things.lowpan.LowpanManager
import com.google.android.things.lowpan.LowpanException
import java.io.IOException
import com.google.android.things.lowpan.LowpanCredential
import com.google.android.things.lowpan.LowpanIdentity
import com.google.android.things.lowpan.LowpanProvisioningParams
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.device.communication_protocols.IoTivityProtocol
import iotlab.rita.iotlabthesis.device.communication_protocols.OpenthreadProtocol
import iotlab.rita.iotlabthesis.pattern_observer.message.DeviceDiscoveredMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.IotivityDeviceStateUpdateMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.OpenThreadDeviceStateUpdateMessage
import org.iotivity.service.RcsResourceAttributes
import org.iotivity.service.client.RcsRemoteResourceObject
import java.nio.ByteBuffer
import java.net.*


class OpenThreadCommunicationUnit(var context: Context) : CommunicationStructure() {

    //LOG TAG PARAMETER
    private val LOG_TAG="THREAD_CONNECTION"
    private val LOG_TAG_ERROR="THREAD_ERROR"

    // UART parameters for the LoWPAN module
    private val UART_PORT = "USB1-1.4:1.1"
    private val UART_BAUD = 115200

    private val PANID = 0xB5E9
    private val XPANID = longToBytes(0x0004002099090000)
    private val CHANNEL = 11
    private val LOWPAN_NETWORK: String = "BATMAN_NETWORK"
    private val LOWPAN_KEY = "00112233445566778899aabbccddeeff"

    private var mLowpanManager: LowpanManager
    private var mLowpanDriver: UartLowpanDriver? = null
    private var mLowpanInterface: LowpanInterface? = null

    private val BROADCAST_PORT=1234
    private val UNICAST_IN_PORT = 1235
    private val UNICAST_OUT_PORT = 1236
    private lateinit var lowpanNetwork : Network
    private lateinit var broadcastRecSocket : DatagramSocket
    private lateinit var uniReceiverSocket : DatagramSocket
    private lateinit var uniSenderSocket : DatagramSocket
    private lateinit var broadRunnable: BroadcastRunnable
    private lateinit var receiverRunnable: ReceiverRunnable
    private lateinit var broadThread : Thread
    private lateinit var receiverThread: Thread


    private val mInterfaceCallback = object : LowpanManager.Callback() {
        override fun onInterfaceAdded(lpInterface: LowpanInterface?) {
            try {
                ensureLowpanInterface()
                formNetwork()
            } catch (e: LowpanException) {
                Log.e(LOG_TAG, "Unable to form LoWPAN network")
            }

        }

        override fun onInterfaceRemoved(lpInterface: LowpanInterface) {
            Log.w(LOG_TAG, "Removed: " + lpInterface.name)
        }
    }

    /**
     * Callback to react to state changes in the LoWPAN interface
     */
    private var mStateCallback = object : LowpanInterface.Callback() {

        override fun onStateChanged(state: Int) {

            when(state){
                LowpanInterface.STATE_ATTACHED->{
                    Log.d(LOG_TAG, "${mLowpanInterface!!.name} provisioned on a LoWPAN network as ${mLowpanInterface!!.role}")
                    var connectivityManager= context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    var canContinue = false
                    while(!canContinue) {
                        connectivityManager.allNetworks.forEach {
                            var netInfo= connectivityManager.getNetworkInfo(it)
                            Log.d("THREAD_CONNECTION","network: ${netInfo}")
                            if(netInfo.typeName.contains("LoWPAN",false)){
                                canContinue = netInfo.isConnected
                                if(canContinue){
                                    lowpanNetwork = it
                                }
                            }
                        }
                        Thread.sleep(200)
                    }

                    initSocket()
                }
                LowpanInterface.STATE_OFFLINE       ->  Log.d(LOG_TAG, "LoWPAN interface has been offline")
                LowpanInterface.STATE_DISABLED      ->  Log.d(LOG_TAG, "LoWPAN interface has been disabled")
                LowpanInterface.STATE_FAULT         ->  Log.d(LOG_TAG, "LoWPAN interface has been fault")
                LowpanInterface.STATE_ATTACHING     ->  Log.d(LOG_TAG, "Lowpan Interface is attacching")
                else                                ->  Log.d(LOG_TAG, "LoWPAN interface changes state in ${state}")

            }
        }

        override fun onProvisionException(e: Exception) {
            Log.d(LOG_TAG, "Could not provision network: ${e.printStackTrace()}")
        }
    }

    init {
        mLowpanManager = LowpanManager.getInstance()
        try {
            mLowpanManager.registerCallback(mInterfaceCallback)
        } catch (e: LowpanException) {
            Log.d(LOG_TAG, "Unable to attach LoWPAN callback")
        }

        try {
            ensureLowpanInterface()
            formNetwork()
            initSocket()
        } catch (e: LowpanException) {
            Log.d(LOG_TAG, "Unable to form LoWPAN network")
        }

        // Register a LoWPAN module connected over UART
        try {
            mLowpanDriver = UartLowpanDriver(UART_PORT, UART_BAUD)
            mLowpanDriver!!.register()
        } catch (e: IOException) {
            Log.d(LOG_TAG, "Unable to init LoWPAN driver")
        }

    }


    /**
     * Verify that a LoWPAN interface is attached
     */
    @Throws(LowpanException::class)
    private fun ensureLowpanInterface() {
        mLowpanInterface = mLowpanManager.getInterface()

        if (mLowpanInterface == null) {
            Log.e(LOG_TAG, "No LoWPAN interface found")
            throw LowpanException("No LoWPAN interface found")
        }

        mLowpanInterface!!.registerCallback(mStateCallback)
    }


    /**
     * Create a new LoWPAN network, if necessary
     */
    @Throws(LowpanException::class)
    private fun formNetwork() {
        if (mLowpanInterface == null) return


        // Check if we are already provisioned on the right network
        var params = mLowpanInterface!!.getLowpanProvisioningParams(false)
        /*if (params != null && LOWPAN_NETWORK.equals(params!!.lowpanIdentity.name) && PANID.equals(params!!.lowpanIdentity.panid) && CHANNEL.equals(params!!.lowpanIdentity.channel)) {
            Log.d(LOG_TAG, "Already provisioned on the network")
            return
        }*/

        Log.d(LOG_TAG, "Forming network")
        params = LowpanProvisioningParams.Builder()
                .setLowpanIdentity(LowpanIdentity.Builder()
                        .setName(LOWPAN_NETWORK)
                        .setXpanid(XPANID)
                        .setPanid(PANID)
                        .setChannel(CHANNEL)
                        .build())
                .setLowpanCredential(LowpanCredential.createMasterKey(LOWPAN_KEY))
                .build()

        mLowpanInterface!!.form(params)
    }



    fun closeConnection(){

        //Closing socket of thread
        if(uniReceiverSocket !=null && !uniReceiverSocket.isClosed)
            uniReceiverSocket.close()
        if(uniSenderSocket !=null && !uniSenderSocket.isClosed)
            uniSenderSocket.close()
        if(broadcastRecSocket !=null && !broadcastRecSocket.isClosed)
            broadcastRecSocket.close()

        if(receiverRunnable != null)
            receiverRunnable.running = false

        if(broadRunnable != null)
            broadRunnable.running = false



        // Detach LoWPAN callbacks
        mLowpanManager.unregisterCallback(mInterfaceCallback);
        if (mLowpanInterface != null) {
            mLowpanInterface!!.unregisterCallback(mStateCallback);
            mLowpanInterface = null;
        }

        if (mLowpanDriver != null) {
            try {
                mLowpanDriver!!.close()
            } catch (e: IOException) {
                Log.d(LOG_TAG, "Unable to close LoWPAN driver")
            } finally {
                mLowpanDriver = null
            }
        }

    }

     private fun initSocket(){
         Log.d(LOG_TAG,"Try to open socket")
         try {
             broadcastRecSocket = DatagramSocket(BROADCAST_PORT)
         }catch (e:Exception){
             Log.d(LOG_TAG,"Broadcast socket is already opened")
             /*broadcastRecSocket.close()
             broadcastRecSocket = DatagramSocket(BROADCAST_PORT)*/
         }
         try {
             uniReceiverSocket = DatagramSocket(UNICAST_IN_PORT)
         }catch (e:Exception){
             Log.d(LOG_TAG,"UniReceiver socket is already opened")
             /*uniReceiverSocket.close()
             uniReceiverSocket = DatagramSocket(UNICAST_IN_PORT)*/
         }
         try {
             uniSenderSocket = DatagramSocket(UNICAST_OUT_PORT)
         }catch (e:Exception){
             Log.d(LOG_TAG,"UniSender socket is already opened")
            /* uniSenderSocket.close()
             uniSenderSocket = DatagramSocket(UNICAST_OUT_PORT)*/
         }



         lowpanNetwork.bindSocket(broadcastRecSocket)
         lowpanNetwork.bindSocket(uniReceiverSocket)
         lowpanNetwork.bindSocket(uniSenderSocket)

         broadRunnable = BroadcastRunnable(broadcastRecSocket,this)
         receiverRunnable = ReceiverRunnable(uniReceiverSocket,this)


         //Init and start Threads
         broadThread = Thread(broadRunnable)
         receiverThread = Thread(receiverRunnable)
         broadThread.start()
         receiverThread.start()

     }


    class ReceiverRunnable(var socket: DatagramSocket, var communicationUnit: OpenThreadCommunicationUnit) : Runnable {

        var running: Boolean = false

        override fun run() {
            running = true
            var buf : ByteArray
            var address : String
            Log.d("THREAD_CONNECTION","Receiver unicast socket opened")
            var packet : DatagramPacket

            while (running) {

                buf = ByteArray(250)
                packet = DatagramPacket(buf, buf.size)
                socket.receive(packet)
                var received = String(packet.getData(), 0, packet.data.size).replace(0.toChar().toString(), "")
                address = packet.address.toString()
                address = address.substring(1)
                Log.d("THREAD_CONNECTION","packet in unicast: ${received} received from ${address}")
                var attrs= received.split(";")
                communicationUnit.deviceUpdate(communicationUnit.createAttributesMap(attrs),address)

            }
            socket.close()

        }
    }

    class BroadcastRunnable(var socket: DatagramSocket,var communicationUnit: OpenThreadCommunicationUnit) : Runnable {

        var running: Boolean = false

        override fun run() {
            running = true
            var buf : ByteArray
            //var counter = 0
            var address : String
            //val address = InetAddress.getByName("fd00:400:2099:0:c86c:d6cf:4124:9652")
            //var datagramSocket : DatagramSocket = DatagramSocket(1234)
            //datagramSocket.broadcast = true
            //Thread.sleep(30000)
            Log.d("THREAD_CONNECTION","Receiver broadcast socket opened")
            //var socket : DatagramSocket = DatagramSocket(1234)
            socket.broadcast = true
            //lowpanNetwork.bindSocket(socket) // FOR UNICAST ON WPAN1
            var packet : DatagramPacket
            //socket = socketFactory.createSocket("ff02::1",1236)
            while (running) {
                buf = ByteArray(250)
                packet = DatagramPacket(buf, buf.size)
                socket.receive(packet)
                //counter ++
                //Log.d("THREAD_CONNECTION","${packet.address}")
                var received = String(packet.getData(), 0, packet.data.size).replace(0.toChar().toString(), "")
                //FOR UNICAST
                address = packet.address.toString() //"fd00:400:2099:0:5e01:e31:7927:71d5"//
                address = address.substring(1)
                Log.d("THREAD_CONNECTION","packet in broadcast: ${received} received from ${address}")
                //buf = "ciao topogigio".toByteArray()
                //packet = DatagramPacket(buf, buf.size,Inet6Address.getByName(address),1234)
                //FOR BROADCAST/ANYCAST/MULTICAST
                //buf = "ciao topogigio".toByteArray()
                //packet = DatagramPacket(buf, buf.size,Inet6Address.getByName("ff02::1%wpan1"),1234)
                var attrs= received.split(";")
                if(attrs[0].equals("presentation")){
                    communicationUnit.createDevice(communicationUnit.createAttributesMap(attrs.subList(1,attrs.size)),address)

                }else{
                    //TODO update state of device
                }
                if (received.equals("end")) {
                    running = false
                    Log.d("THREAD_CONNECTION","END TRASMISSION ${packet}")
                }
                //if(counter>50 && counter%2==0){
                //socket.send(packet)
                /*if(counter<20) {
                    socket.send(packet)
                    Log.d("THREAD_CONNECTION", "messaggio inviato")
                }else running = false*/
               // }

            }
            socket.close()

        }

    }

    private fun createAttributesMap(attrs:List<String>) : HashMap<String,String>{
        var map: HashMap<String,String> = HashMap()
        attrs.forEach {
            val attrValue= it.split(":")
            map.put(attrValue[0],attrValue[1])
            Log.d("ATTRIBUTE_MAP_OT","${attrValue[0]} and ${attrValue[1]} ")
        }
        return map
    }

    private fun deviceUpdate(attrs: HashMap<String,String>,host:String){
        notifyObserver(OpenThreadDeviceStateUpdateMessage(host,attrs))
    }

    private fun createDevice(attrs: HashMap<String,String>, host: String){

        try {
            val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
            var class_state = Class.forName(name)
            var construct_state = class_state.getDeclaredConstructor()
            var state = (construct_state.newInstance()) as DeviceState
            notifyObserver(DeviceDiscoveredMessage(state, attrs, host, OpenthreadProtocol(host,uniSenderSocket)))
            ackCreation(host)
        } catch (ex: java.lang.Exception) {
            Log.d(LOG_TAG_ERROR, "Device Creation: " + ex.message.toString())
        }
    }

    private fun ackCreation(host:String){
        var buf = "ACK_DEVICE_CREATED:${UNICAST_IN_PORT}:${UNICAST_OUT_PORT}".toByteArray()
        var packet = DatagramPacket(buf,buf.size,Inet6Address.getByName(host),BROADCAST_PORT)
        broadcastRecSocket.send(packet)
        Log.d(LOG_TAG,"Ack creation sended")
    }


    override fun sendCommand() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStateReceived() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPresentationReceveid() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reset() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun longToBytes(x: Long): ByteArray {
        val buffer = ByteBuffer.allocate(java.lang.Long.BYTES)
        buffer.putLong(x)
        return buffer.array()
    }


}