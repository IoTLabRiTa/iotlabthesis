package iotlab.rita.iotlabthesis.communication

import android.app.Activity
import android.util.Log
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.model.rules.Rule
import iotlab.rita.iotlabthesis.pattern_observer.message.DeviceChangeSpaceMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.PerformReceivedCommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.NearbySetupDeviceMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ReceivedRuleMessage
import org.json.JSONObject
import java.lang.Exception

class NearbyCommunicationUnit(var context: Activity) : CommunicationStructure() {

    val LOG_TAG = "NEARBY_CONNECTION"
    val thingsNickname = "IoTLabAndroidThings"
    lateinit var connectionLifecycleCallback : ConnectionLifecycleCallback
    lateinit var payloadCallback : PayloadCallback
    var incomingPayloads: MutableMap<String, Payload>
    var endpoints : ArrayList<String> = ArrayList()

    var gson = GsonBuilder().create()


    init {
        initCallbacks()
        incomingPayloads = mutableMapOf()
        startAdvertising()
    }

    private fun initCallbacks(){

        payloadCallback  = object : PayloadCallback(){

            override fun onPayloadReceived(endPoint: String, payload: Payload) {
                val receivedMessage = String(payload!!.asBytes()!!, 0, payload!!.asBytes()!!.size)
                try {
                    //add payload to map
                    //incomingPayloads[endPoint] = payload

                    Log.d(LOG_TAG, "Message received: ${receivedMessage} from ${endPoint} ")
                    val typeAny: LinkedTreeMap<String, Any> = LinkedTreeMap()
                    var mapAny: LinkedTreeMap<String, Any> = Gson().fromJson(receivedMessage, typeAny.javaClass)
                    val commandAny = mapAny["command"] as String
                    if (commandAny!!.equals("ActionRule")) {
                        notifyObserver(ReceivedRuleMessage(mapAny))
                    } else {
                        val type: HashMap<String, String> = HashMap()
                        var map: HashMap<String, String> = Gson().fromJson(receivedMessage, type.javaClass)
                        val deviceID = map["uniqueID"]
                        val command = map["command"]
                        map.remove("uniqueID")
                        map.remove("command")
                        if (command.equals("MoveDevice")) {
                            var message = DeviceChangeSpaceMessage(map["to"]!!)
                            message.deviceID = deviceID!!
                            notifyObserver(message)
                        } else {
                            notifyObserver(PerformReceivedCommandMessage(deviceID!!, command!!, map))
                        }
                    }
                }catch (e:Exception){
                    Log.d(LOG_TAG, "Error to parse message received: ${receivedMessage}\n error : ${e.message}")
                }
            }

            override fun onPayloadTransferUpdate(endPoint: String, update: PayloadTransferUpdate) {
                when(update.status){
                    PayloadTransferUpdate.Status.IN_PROGRESS ->{
                        var size = update.totalBytes
                        if (size.equals(-1)){
                            //i'm receiving a stream
                        }
                    }
                    PayloadTransferUpdate.Status.SUCCESS ->{
                        //Transferred 100%
                    }
                    PayloadTransferUpdate.Status.FAILURE -> {
                        //ERROR
                    }
                }
            }
        }

        connectionLifecycleCallback = object : ConnectionLifecycleCallback() {
            override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
                //Accept or reject connection
                //TODO Authentication
                Nearby.getConnectionsClient(context).acceptConnection(endpointId, payloadCallback)
            }

            override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
                when (result.status.statusCode) {
                    ConnectionsStatusCodes.STATUS_OK -> {
                        // We're connected! Can now start sending and receiving data.
                        Log.d(LOG_TAG, "Accept connection with ${endpointId}")
                        endpoints.add(endpointId)
                        notifyObserver(NearbySetupDeviceMessage(endpointId))
                    }
                    ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                        // The connection was rejected by one or both sides.
                        Log.d(LOG_TAG, "Reject connection with ${endpointId}")
                    }
                    ConnectionsStatusCodes.STATUS_ERROR -> {
                        // The connection broke before it was able to be accepted.
                        Log.d(LOG_TAG, "Error to connect to ${endpointId}")
                    }
                }
            }

            override fun onDisconnected(endpointId: String) {
                // We've been disconnected from this endpoint. No more data can be
                // sent or received.
                Log.d(LOG_TAG, "Lost connection with ${endpointId}, device disconnected")
                endpoints.remove(endpointId)
            }
        }
    }

    private fun startAdvertising() {

        Nearby.getConnectionsClient(context).stopAdvertising()

        Nearby.getConnectionsClient(context).startAdvertising(
                thingsNickname,
                thingsNickname,
                connectionLifecycleCallback,
                AdvertisingOptions.Builder().setStrategy(com.google.android.gms.nearby.connection.Strategy.P2P_STAR).build())
                .addOnSuccessListener (){ void ->
                    Log.d(LOG_TAG,"Start to advertise nearby devices...")
                }
                .addOnFailureListener(){
                    Log.d(LOG_TAG,"Problem with advertising nearby devices: ${it.printStackTrace()}")
                }
    }


    fun sendMessage(message: String, endpoint: String){
        try {
            val bytesPayload = Payload.fromBytes(message.toByteArray())
            Nearby.getConnectionsClient(context).sendPayload(endpoint, bytesPayload)
        }catch (ex:Exception){
            Log.d(LOG_TAG,"Impossible to send a message a ${endpoint}")
        }
    }

    fun sendDeviceUpdate(device : Device, alive : Boolean, endpoint: String){
        val type : HashMap<String,String> = HashMap()
        var serialization = device.getSerialization()
        serialization.put("alive",alive.toString())
        val jsonString = gson.toJson(serialization,type.javaClass)
        sendMessage(jsonString,endpoint)
    }

    fun sendSmartSpace(smartSpace: SmartSpace, endpoint: String){
        val type : HashMap<String,Any> = HashMap()

        var name = smartSpace.getName()
        var mapSmartSpace : HashMap<String,Any> = HashMap()
        mapSmartSpace.put("name",name)

        //Store devices
        var mapDevices : HashMap<String,String> = HashMap()
        var count = 0;
        smartSpace.getDevices().forEach {
            mapDevices.put("device$count",it.getUniqueID())
            count++
        }
        mapSmartSpace.put("devices",mapDevices)

        //Store resources
        var resourceMap : HashMap<String,Any> = HashMap()
        smartSpace.getResources().getSerialization().forEach {
            resourceMap.put(it.get("resourceName")!!,it)
        }
        mapSmartSpace.put("resources",resourceMap)

        val jsonString = gson.toJson(mapSmartSpace,type.javaClass)
        sendMessage(jsonString,endpoint)
    }

    fun sendRule(rule: Rule, endpoint: String, actionRule : String){
        var mapMessage = rule.getSerialization()
        mapMessage.put("command","ActionRule")
        mapMessage.put("actionRule", actionRule)
        var jsonMessage = GsonBuilder().create().toJson(mapMessage)

        sendMessage(jsonMessage,endpoint)
    }



    //TODO unimplemented method of CommunicationStrucutre to define/eliminate

    override fun sendCommand() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStateReceived() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPresentationReceveid() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reset() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}