package iotlab.rita.iotlabthesis.communication

import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import java.util.*

abstract class CommunicationStructure: MyObservable<MyMessage>() {


    abstract fun sendCommand()
    abstract fun onStateReceived()
    abstract fun onPresentationReceveid()
    abstract fun reset()


}