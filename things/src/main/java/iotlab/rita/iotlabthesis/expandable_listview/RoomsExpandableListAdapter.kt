package iotlab.rita.iotlabthesis.expandable_listview

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.SolarPanel
import iotlab.rita.iotlabthesis.device.Thermostat
import iotlab.rita.iotlabthesis.fragment.CommandDialog
import iotlab.rita.iotlabthesis.fragment.MoveRoomDialog
import iotlab.rita.iotlabthesis.model.SmartSpace
import java.util.HashMap

class RoomsExpandableListAdapter(var context : Context, var smartSpaces : ArrayList<SmartSpace>, var fragmentManager: FragmentManager) : BaseExpandableListAdapter() {


    override fun getGroup(listPosition: Int): Any {
        return smartSpaces.get(listPosition)
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(listPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var smartSpace = getGroup(listPosition) as SmartSpace
        var myConvertView : View?

        if(convertView == null){
            var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.room_listgroup, null)
        }else{
            myConvertView = convertView
        }

        //Set room icon and name
        var roomIcon = myConvertView!!.findViewById(R.id.roomIcon) as ImageView
        //TODO SET ICON
        var nameTV = myConvertView!!.findViewById(R.id.roomName) as TextView
        nameTV.text = smartSpace.getName()

        var resourcesTV = myConvertView!!.findViewById(R.id.resourcesTV) as TextView
        resourcesTV.text = "${smartSpace.getResources()}"

        return myConvertView!!
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return smartSpaces.get(listPosition).getDevices().size
    }

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return smartSpaces.get(listPosition).getDevices().get(expandedListPosition)
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getChildView(listPosition: Int, expandedListPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var device = getChild(listPosition,expandedListPosition) as Device
        var myConvertView : View?
        var holder : DeviceViewHolder

        if(convertView == null){
            var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.device_childgroup, null)
            holder= DeviceViewHolder(myConvertView,smartSpaces)
            myConvertView.tag = holder
        }else{
            myConvertView = convertView
            holder = convertView!!.tag as DeviceViewHolder
        }

        //Setup Graphic
        holder.deviceTV.text = "${device.getNameProductor()} - ${device.getName()} ID: ${device.getSerialNumber()}"

        if(device.getCommunicationProtocol().isNotReady()){
            holder.blockUI()
        }else{
            holder.drawUI(device, context, fragmentManager)
        }

        return myConvertView!!
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return smartSpaces.size
    }




    class DeviceViewHolder(view: View, var smartSpaces: ArrayList<SmartSpace>) {

        var constraintLayout : ConstraintLayout
        var loadinProgressBar : ProgressBar
        var deviceIcon : ImageView
        var deviceLB : TextView
        var deviceTV : TextView
        var deviceNotAvailableTV : TextView
        var onOffSwitchLB : TextView
        var onOffSwitch: Switch
        var powerConsumptionLB : TextView
        var powerConsumptionTV: TextView
        var commandLB : TextView
        var commandSpinner: Spinner
        var commandBT: Button
        var viewList : ArrayList<View> = ArrayList()

        var addedView : ArrayList<View> = ArrayList()

        var lineView : View
        var moveRoomBT : ImageButton

        init {
            constraintLayout = view.findViewById(R.id.constraintLayout)
            loadinProgressBar = view.findViewById(R.id.loadingProgressBar)
            deviceIcon = view.findViewById(R.id.deviceIcon)
            deviceLB = view.findViewById(R.id.deviceLB)
            deviceTV = view.findViewById(R.id.deviceID)
            deviceNotAvailableTV = view.findViewById(R.id.deviceNotAvailable)
            onOffSwitch = view.findViewById(R.id.onOffSwitch) as Switch
            onOffSwitchLB = view.findViewById(R.id.onOffSwitchLB)
            powerConsumptionLB = view.findViewById(R.id.powerLabel)
            powerConsumptionTV = view.findViewById(R.id.powerConsumptionTV) as TextView
            commandLB = view.findViewById(R.id.commandLabel)
            commandSpinner = view.findViewById(R.id.commandSpinner) as Spinner
            commandBT = view.findViewById(R.id.commandButton) as Button

            moveRoomBT = view.findViewById(R.id.moveRoom) as ImageButton
            lineView = view.findViewById(R.id.lineView)


            //Add views to list
            viewList.add(deviceIcon)
            viewList.add(deviceLB)
            viewList.add(deviceTV)
            viewList.add(onOffSwitch)
            viewList.add(onOffSwitchLB)
            viewList.add(powerConsumptionLB)
            viewList.add(powerConsumptionTV)
            viewList.add(commandLB)
            viewList.add(commandBT)
            viewList.add(commandSpinner)
        }

        fun blockUI(){
            viewList.forEach { it.alpha = 0.4f }
            onOffSwitch.isClickable = false
            commandSpinner.isClickable = false
            commandBT.isClickable = false
            deviceNotAvailableTV.visibility = View.VISIBLE
            loadinProgressBar.visibility = View.GONE

        }

        fun drawUI(device : Device, context: Context, fragmentManager: FragmentManager){
            addedView.forEach {
                constraintLayout.removeView(it) //Remove added view to avoid overlap
                viewList.remove(it)
            }
            deviceNotAvailableTV.visibility = View.GONE
            viewList.forEach { it.alpha = 1f }
            onOffSwitch.isClickable = true
            commandSpinner.isClickable = true
            commandBT.isClickable = true
            onOffSwitch.setOnCheckedChangeListener(null)
            onOffSwitch.isChecked = device.getDeviceState().isOn()
            onOffSwitch.setOnCheckedChangeListener{ compoundButton, checked ->
                var hashmap = HashMap<String,String>()
                hashmap.put("onOffState",checked.toString())
                device.getCommunicationProtocol().sendCommand("onOff", hashmap )
                loadinProgressBar.visibility = View.VISIBLE

            }

            var variablesDevice = device.getSerialization()
            //Clean fields
            variablesDevice.remove("isOn")
            variablesDevice.remove("model")
            variablesDevice.remove("name")
            variablesDevice.remove("nameProductor")
            variablesDevice.remove("powerConsumption")
            variablesDevice.remove("serialNumber")
            device.getDeviceState().getCommandMap().forEach{
                variablesDevice.remove(it.key)
            }


            //Add variables view dinamic!
            var firtVariable = true
            var lastView : View? = null
            variablesDevice.forEach{
                var set = ConstraintSet()
                val variable = TextView(context)
                val value = TextView(context)
                Log.d("ExpandableListView","${it.key} --> ${it.value}")
                variable.id = View.generateViewId()
                variable.text = it.key
                value.id = View.generateViewId()
                value.text = it.value

                constraintLayout.addView(variable,0)
                set.clone(constraintLayout)
                if(firtVariable) {
                    //Add variable
                    set.connect(variable.id, ConstraintSet.LEFT, onOffSwitchLB.id, ConstraintSet.LEFT, 8)
                    set.connect(variable.id, ConstraintSet.TOP, lineView.id, ConstraintSet.BOTTOM, 16)
                    set.applyTo(constraintLayout)

                    lastView = variable
                    firtVariable = false
                }else{
                    //Add variable
                    set.connect(variable.id, ConstraintSet.LEFT, lastView!!.id, ConstraintSet.LEFT, 0)
                    set.connect(variable.id, ConstraintSet.TOP, lastView!!.id, ConstraintSet.BOTTOM, 16)
                    set.applyTo(constraintLayout)

                }

                //Add value
                set = ConstraintSet()
                constraintLayout.addView(value,0)
                set.clone(constraintLayout)
                set.connect(value.id, ConstraintSet.LEFT, powerConsumptionLB.id, ConstraintSet.LEFT, 0)
                set.connect(value.id, ConstraintSet.TOP, variable.id, ConstraintSet.TOP, 0)
                set.connect(value.id, ConstraintSet.BOTTOM, variable.id, ConstraintSet.BOTTOM, 0)
                set.applyTo(constraintLayout)

                lastView = variable

                viewList.add(variable)
                viewList.add(value)
                addedView.add(variable)
                addedView.add(value)
            }

            //Add last constraints
            var set = ConstraintSet()
            set.clone(constraintLayout)
            set.connect(commandLB.id, ConstraintSet.LEFT, lastView!!.id, ConstraintSet.LEFT, 0)
            set.connect(commandLB.id, ConstraintSet.TOP, lastView!!.id, ConstraintSet.BOTTOM, 8)
            set.applyTo(constraintLayout)

            var iconName = device.getName().toLowerCase() + "_icon"
            context.resources.getIdentifier(iconName, "mipmap", context.packageName)
            deviceIcon.setImageResource(context.resources.getIdentifier(iconName, "mipmap", context.packageName))



            powerConsumptionTV.text = "${device.getDeviceState().getPowerConsumption().getValue().toString()} W"
/* OLD          if(device.getName().equals("SolarPanel")){
                temperatureTV.text = (device.getDeviceState() as SolarPanel).getPOwerAccumulated().toString()+ " W"
                temperatureLB.text = "power accumulated: "
                deviceIcon.setImageResource(R.drawable.solar_panel_icon)
                deviceIcon.invalidate()
            }
            else temperatureTV.text = (device.getDeviceState() as Thermostat).getTemperatureReading().toString()+ " °C"
*/
            //Create list of commands from device
            var commandList : ArrayList<String> = ArrayList()
            device.getDeviceState().getCommandMap().forEach {
                commandList.add(it.key+": "+it.value.getDescription())
                Log.d("CommandList",it.key)
            }

            //Populate spinner
            var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,commandList)
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            commandSpinner.adapter=spinnerAdapter

            commandBT.setOnClickListener {
                //Create Dialog
                var commandSelectedString :String = commandSpinner.selectedItem as String// as TextView).text.toString()
                var commandSelected= device.getDeviceState().getCommandMap()[commandSelectedString.split(":")[0]]
                var bundle: Bundle = Bundle()
                //TODO IL comando nel bundle ha i parametri vuoti
                bundle.putSerializable("command",commandSelected)
                var commandDialog: CommandDialog = CommandDialog()
                commandDialog.arguments = bundle
                commandDialog.registerObserver(device)
                commandDialog.show(fragmentManager,"CommandDialog")

            }

            // Show loading circle
            if (device.getCommunicationProtocol().waitingNewState){
                loadinProgressBar.visibility = View.VISIBLE
            }else{
                loadinProgressBar.visibility = View.GONE
            }

            if(!device.getDeviceState().isOn()){
                commandBT.isClickable = false
                commandBT.alpha = 0.4f
                commandSpinner.isClickable = false
                commandSpinner.alpha = 0.4f
            }

            moveRoomBT.setOnClickListener {
                var bundle : Bundle = Bundle()
                var smartSpacesString : ArrayList<String> = ArrayList()
                smartSpaces.forEach {
                    if(!it.getDevices().contains(device))
                        smartSpacesString.add((it.getName()))
                }
                bundle.putStringArrayList("smartspaces",smartSpacesString)
                var moveRoomDialog : MoveRoomDialog = MoveRoomDialog()
                moveRoomDialog.arguments = bundle
                moveRoomDialog.registerObserver(device)
                moveRoomDialog.show(fragmentManager,"MoveRoomDialog")
            }
        }
    }


}