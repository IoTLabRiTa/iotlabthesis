package iotlab.rita.iotlabthesis.expandable_listview

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.rules.*
import java.text.SimpleDateFormat
import java.util.*

class RulesGUI ( var activity: AppCompatActivity, var smartHome: SmartHome) {


    private var addConditionBT : Button
    private var addConditionTimeBT : Button
    private var addConsequenceBT : Button
    private var addRuleBT : Button
    private var conditionsLV : ListView
    private var consequencesLV : ListView
    private var ruleLV : ListView

    private var conditionsAdapter : ConditionListAdapter
    var conditionList : ArrayList<ConditionRule> = ArrayList()

    private var consequencesAdapter : ConsequenceAdapter
    var consequenceList : ArrayList<Consequence> = ArrayList()

    private var ruleAdapter : RuleAdapter

    init {
        ruleLV = activity.findViewById(R.id.rulesLV)
        /*conditionList.add(ConditionResourcePredicate())
        consequenceList.add(Consequence())*/

        addConditionBT = activity.findViewById(R.id.addconditionBT)
        addConditionTimeBT = activity.findViewById(R.id.addtimeconditionBT)
        addConsequenceBT = activity.findViewById(R.id.addconsequenceBT)
        addRuleBT = activity.findViewById(R.id.addRuleBT)

        conditionsLV = activity.findViewById(R.id.conditionLV)
        consequencesLV = activity.findViewById(R.id.consequencesLV)

        conditionsAdapter = ConditionListAdapter(activity, smartHome,conditionList)
        conditionsLV.adapter = conditionsAdapter

        var fragmentManager = activity.supportFragmentManager
        consequencesAdapter = ConsequenceAdapter(activity,fragmentManager,smartHome,consequenceList)
        consequencesLV.adapter = consequencesAdapter

        ruleAdapter = RuleAdapter(activity,smartHome)
        ruleLV.adapter = ruleAdapter

        addConditionBT.setOnClickListener {
            conditionList.add(ConditionResourcePredicate())
            notifyCondition()
        }

        addConditionTimeBT.setOnClickListener {
            var timeCondition = ConditionTimePredicate()
            activity.runOnUiThread{showTimePicker(activity,timeCondition)}
        }


        addConsequenceBT.setOnClickListener {
            consequenceList.add(Consequence())
            notifyConsequece()
        }

        addRuleBT.setOnClickListener {
            var ok = conditionList.fold(true){
                acc, x -> acc and x.isCorrectedSaved()
            }
            ok = consequenceList.fold(ok){
                acc, x -> acc and x.isCorrectedSaved()
            }
            if(ok){
                var rule = Rule()
                conditionList.forEach {
                    rule.addConditionPredicate(it)
                }
                consequenceList.forEach {
                    rule.addConsequence(it)
                }
                rule.setReady(true)
                smartHome.addRule(rule)
                activity.runOnUiThread{
                    Toast.makeText(activity,"Rule ${rule.getID()} Saved", Toast.LENGTH_SHORT).show()
                    Log.d("RULE_CONFIGURATION","Created Rule ${rule.getID()}")
                    conditionList.clear()
                    consequenceList.clear()
                    notifyCondition()
                    notifyConsequece()
                    notifyRule()
                }
            }else{
                activity.runOnUiThread{
                    Log.d("RULE_CONFIGURATION","Rule Not Created")
                    Toast.makeText(activity,"Error to save rule, conditions or consequences are not completed", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun notifyConsequece(){
        activity.runOnUiThread {
            (consequencesLV.adapter as ConsequenceAdapter).notifyDataSetChanged()
        }
    }

    fun notifyCondition(){
        activity.runOnUiThread{
            (conditionsLV.adapter as ConditionListAdapter).notifyDataSetChanged()
        }
    }

    fun notifyRule(){
        activity.runOnUiThread{
            (ruleLV.adapter as RuleAdapter).notifyDataSetChanged()
        }
    }

    fun update(){
        notifyCondition()
        notifyConsequece()
        notifyRule()
    }

    fun showTimePicker(activity: AppCompatActivity,timeCondition: ConditionTimePredicate){
        var yearSelected : Int =1
        var monthSelected : Int = 1
        var daySelected : Int = 1
        var calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val calendarHour = calendar.get(Calendar.HOUR_OF_DAY)
        val calendarMinute = calendar.get(Calendar.MINUTE)
        var calendarSelected = Calendar.getInstance()

        var timepickerdialog = TimePickerDialog(activity,
                object : TimePickerDialog.OnTimeSetListener{
                    override fun onTimeSet(view: TimePicker?, hour: Int, minute: Int) {
                        calendarSelected.set(yearSelected,monthSelected,daySelected,hour,minute)
                        var sdf = SimpleDateFormat("d MMM yyyy HH:mm", Locale.ITALY)
                        var timeAntecedent = TimeAntecedent(calendarSelected,sdf)
                        timeCondition.setParameters("After","","","DateTime",timeAntecedent.getAntecedentID())
                        timeCondition.setPredicate(timeAntecedent)
                        timeCondition.setcorrectedSaved(true)
                        conditionList.add(timeCondition)
                        notifyCondition()
                    }

                }, calendarHour, calendarMinute, true)

        var datePicker = DatePickerDialog(activity, object:DatePickerDialog.OnDateSetListener{

            override fun onDateSet(view: DatePicker?, y: Int, m: Int, d: Int) {
                yearSelected = y
                monthSelected = m
                daySelected = d
                timepickerdialog.show()
            }

        },year,month,day)

        datePicker.show()

    }
}