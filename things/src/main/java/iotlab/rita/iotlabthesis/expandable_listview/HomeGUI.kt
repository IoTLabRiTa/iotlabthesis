package iotlab.rita.iotlabthesis.expandable_listview

import android.app.Activity
import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.widget.ExpandableListView
import android.widget.TextView
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace

class HomeGUI ( context: Context, smartHome: SmartHome) {

    private var logTV: TextView
    private var activity: AppCompatActivity
    private var expandableListView: ExpandableListView
    private var expandableListAdapter: RoomsExpandableListAdapter
    var fragmentManager: FragmentManager

    init {
        //Definizione activity usata per la nearby
        activity = context as AppCompatActivity

        fragmentManager = activity.supportFragmentManager
        logTV = (context as Activity).findViewById(R.id.LOGTV)
        logTV.setMovementMethod(ScrollingMovementMethod())

        expandableListView = (context as Activity).findViewById(R.id.expandableListView)

        var smartSpaces: ArrayList<SmartSpace> = ArrayList()
        smartSpaces.add(smartHome)
        smartHome.getRooms().forEach {
            smartSpaces.add(it)
        }
        expandableListAdapter = RoomsExpandableListAdapter(context, smartSpaces, fragmentManager)
        expandableListView.setAdapter(expandableListAdapter)

    }

    fun log(logString : String){

        if(logTV !=null) {
            activity.runOnUiThread {
                if (logTV.text.contains("EmptyLog"))
                    logTV.text = logString
                else
                    logTV.append("\n ${logString}")
            }
        }
    }

    fun updateLV(){
        if(expandableListAdapter != null) {
            activity.runOnUiThread {
                (expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }
        }
        /*if(rulesGUI != null){
            rulesGUI!!.update()
        }*/
    }

}




// *****************OLD DEVICE LIST VIEW ADAPTER ********************

/* CALL OF DEVICE LISTVIEW

deviceLV = (context as Activity).findViewById(R.id.devicesLV)
deviceArrayAdapter = DeviceAdapter(fragmentManager,activity,smartHome.getAllDevices())
deviceLV.adapter = deviceArrayAdapter
*/


//CLASS DEVICE ADAPTER

/*class DeviceAdapter(var fragmentManager: FragmentManager,context: Context,var deviceList:ArrayList<Device>)
      : ArrayAdapter<Device>(context,0,deviceList) {



      override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
          var myConvertView: View?
          var holder :DeviceViewHolder?
          var device = getItem(position)

          //Inflate view
          Log.d("LISTVIEW_IOT","${device.getSerialNumber()} saved at position ${position}")
          if(convertView==null){
              var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
              myConvertView = layoutInflater.inflate(R.layout.device_childgroup,parent,false)
              holder= DeviceViewHolder(myConvertView)
              myConvertView.tag = holder
          }else{
              myConvertView = convertView
              holder = convertView!!.tag as DeviceViewHolder
          }


          //Setup Graphic
          holder.deviceTV.text = "${device.getNameProductor()} - ${device.getName()} ID: ${device.getSerialNumber()}"

          if(device.getCommunicationProtocol().isNotReady()){
              holder.blockUI()
          }else{
              holder.drawUI(device, context, fragmentManager)
          }


          return myConvertView!!
      }


      class DeviceViewHolder(view: View) {

          var loadinProgressBar : ProgressBar
          var deviceIcon : ImageView
          var deviceLB : TextView
          var deviceTV : TextView
          var deviceNotAvailableTV : TextView
          var onOffSwitchLB : TextView
          var onOffSwitch: Switch
          var powerConsumptionLB : TextView
          var powerConsumptionTV: TextView
          var temperatureLB : TextView
          var temperatureTV: TextView
          var commandLB : TextView
          var commandSpinner: Spinner
          var commandBT: Button
          var viewList : ArrayList<View> = ArrayList()

          init {
              loadinProgressBar = view.findViewById(R.id.loadingProgressBar)
              deviceIcon = view.findViewById(R.id.deviceIcon)
              deviceLB = view.findViewById(R.id.deviceLB)
              deviceTV = view.findViewById(R.id.deviceID)
              deviceNotAvailableTV = view.findViewById(R.id.deviceNotAvailable)
              onOffSwitch = view.findViewById(R.id.onOffSwitch) as Switch
              onOffSwitchLB = view.findViewById(R.id.onOffSwitchLB)
              powerConsumptionLB = view.findViewById(R.id.powerLabel)
              powerConsumptionTV = view.findViewById(R.id.powerConsumptionTV) as TextView
              temperatureLB = view.findViewById(R.id.temperatureLabel)
              temperatureTV = view.findViewById(R.id.temperatureValue) as TextView
              commandLB = view.findViewById(R.id.commandLabel)
              commandSpinner = view.findViewById(R.id.commandSpinner) as Spinner
              commandBT = view.findViewById(R.id.commandButton) as Button

              //Add views to list
              viewList.add(deviceIcon)
              viewList.add(deviceLB)
              viewList.add(deviceTV)
              viewList.add(onOffSwitch)
              viewList.add(onOffSwitchLB)
              viewList.add(powerConsumptionLB)
              viewList.add(powerConsumptionTV)
              viewList.add(temperatureLB)
              viewList.add(temperatureTV)
              viewList.add(commandLB)
              viewList.add(commandBT)
              viewList.add(commandSpinner)
          }

          fun blockUI(){
              viewList.forEach { it.alpha = 0.4f }
              onOffSwitch.isClickable = false
              commandSpinner.isClickable = false
              commandBT.isClickable = false
              deviceNotAvailableTV.visibility = View.VISIBLE
              loadinProgressBar.visibility = View.GONE

          }

          fun drawUI(device : Device, context: Context, fragmentManager: FragmentManager){
              deviceNotAvailableTV.visibility = View.GONE
              viewList.forEach { it.alpha = 1f }
              onOffSwitch.isClickable = true
              commandSpinner.isClickable = true
              commandBT.isClickable = true
              onOffSwitch.setOnCheckedChangeListener(null)
              onOffSwitch.isChecked = device.getDeviceState().isOn()
              onOffSwitch.setOnCheckedChangeListener{ compoundButton, checked ->
                  var hashmap = HashMap<String,String>()
                  hashmap.put("onOffState",checked.toString())
                  device.getCommunicationProtocol().sendCommand("onOff", hashmap )
                  loadinProgressBar.visibility = View.VISIBLE

              }
              powerConsumptionTV.text = device.getDeviceState().getPowerConsumption().toString()
              if(device.getName().equals("SolarPanel")){
                  temperatureTV.text = (device.getDeviceState() as SolarPanel).getPOwerAccumulated().toString()+ " W"
                  temperatureLB.text = "power accumulated: "
                  deviceIcon.setImageResource(R.drawable.solar_panel_icon)
                  deviceIcon.invalidate()
              }
              else temperatureTV.text = (device.getDeviceState() as Thermostat).getTemperatureReading().toString()+ " °C"
              //TODO AGGIUNGERE GRAFICA DINAMICA

              //Create list of commands from device
              var commandList : ArrayList<String> = ArrayList()
              device.getDeviceState().getCommandMap().forEach {
                  commandList.add(it.key+": "+it.value.getDescription())
                  Log.d("CommandList",it.key)
              }

              //Populate spinner
              var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,commandList)
              spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
              commandSpinner.adapter=spinnerAdapter

              commandBT.setOnClickListener {
                  //Create Dialog
                  var commandSelectedString :String = commandSpinner.selectedItem as String// as TextView).text.toString()
                  var commandSelected= device.getDeviceState().getCommandMap()[commandSelectedString.split(":")[0]]
                  var bundle: Bundle = Bundle()
                  //TODO IL comando nel bundle ha i parametri vuoti
                  bundle.putSerializable("command",commandSelected)
                  var commandDialog: CommandDialog = CommandDialog()
                  commandDialog.arguments = bundle
                  commandDialog.registerObserver(device)
                  commandDialog.show(fragmentManager,"CommandDialog")

              }

              // Show loading circle
              if (device.getCommunicationProtocol().waitingNewState){
                  loadinProgressBar.visibility = View.VISIBLE
              }else{
                  loadinProgressBar.visibility = View.GONE
              }

              if(!device.getDeviceState().isOn()){
                  commandBT.isClickable = false
                  commandBT.alpha = 0.4f
                  commandSpinner.isClickable = false
                  commandSpinner.alpha = 0.4f
              }
          }
      }


  }*/