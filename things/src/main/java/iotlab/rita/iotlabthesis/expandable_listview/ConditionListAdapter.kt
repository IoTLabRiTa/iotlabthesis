package iotlab.rita.iotlabthesis.expandable_listview

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.model.rules.Antecedent
import iotlab.rita.iotlabthesis.model.rules.ConditionResourcePredicate
import iotlab.rita.iotlabthesis.model.rules.ConditionRule


class ConditionListAdapter(var context: Context, var smartHome: SmartHome, var conditionList : ArrayList<ConditionRule> ): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var myConvertView: View?
        var holder : ConditionViewHolder?
        var conditionRule = getItem(position) as ConditionRule

        //Inflate view
        if(convertView==null){
            var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.condition_rule,parent,false)
            holder= ConditionViewHolder(myConvertView)
            myConvertView.tag = holder
        }else{
            myConvertView = convertView
            holder = convertView!!.tag as ConditionViewHolder
        }


        //Setup Graphic
        if(conditionRule.isTimeBased()){
            holder.drawTimeUI(conditionRule,this@ConditionListAdapter)
        }
        else holder.drawResourceUI(context,smartHome,conditionRule,this@ConditionListAdapter)


        return myConvertView!!
    }

    class ConditionViewHolder(var view: View){

        var smartspaceTV : TextView
        var resourceTV : TextView
        var attributeTV : TextView
        var operatorTV : TextView
        var valueTV : TextView

        var smartspaceSpinner : Spinner
        var resourceSpinner : Spinner
        var attributeSpinner : Spinner
        var operatorSpinner: Spinner

        var valueET : EditText

        var saveBT : Button
        var deleteBT : Button

        init {
            smartspaceTV = view.findViewById(R.id.smartSpaceTV)
            resourceTV = view.findViewById(R.id.resourceTV)
            attributeTV = view.findViewById(R.id.attributeTV)
            operatorTV = view.findViewById(R.id.operatorTV)
            valueTV = view.findViewById(R.id.valueTV)

            smartspaceSpinner = view.findViewById(R.id.smartspaceSpinnner)
            resourceSpinner = view.findViewById(R.id.resourceSpinner)
            attributeSpinner = view.findViewById(R.id.attributeSpinner)
            operatorSpinner = view.findViewById(R.id.operatorSpinner)
            valueET = view.findViewById(R.id.valueET)

            saveBT = view.findViewById(R.id.saveBT)
            deleteBT = view.findViewById(R.id.deleteBT)
            deleteBT.isClickable = false
            deleteBT.visibility = View.GONE

        }

        fun drawResourceUI(context: Context, smartHome: SmartHome, conditionRule: ConditionRule, adapter: ConditionListAdapter){

            var deviceSelected : Device? = null
            var resourceSelected : Antecedent? = null
            var spaceList = ArrayList<SmartSpace> ()
            spaceList.add(smartHome)
            spaceList.addAll(smartHome.getRooms())
            var smartSpaceList = ArrayList<String>()
            smartSpaceList.add(smartHome.getName())
            smartHome.getRooms().forEach {
                smartSpaceList.add(it.getName())
            }
            //Populate spinner
            var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,smartSpaceList)
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            smartspaceSpinner.adapter=spinnerAdapter

            smartspaceSpinner.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val spaceSelectedString = smartSpaceList.get(position)
                    var spaceSelected = spaceList.filter {
                        it.getName().equals(spaceSelectedString)
                    }.firstOrNull()
                    if(spaceSelected != null){
                        val resourceList = ArrayList<String>()
                        spaceSelected.getResources().getSerialization().forEach {
                            if(it["isActive"]!!.toBoolean())
                                resourceList.add(it["resourceName"]!!)
                        }
                        spaceSelected.getDevices().forEach {
                            resourceList.add(it.getUniqueID())
                        }

                        //Populate spinner
                        var spinnerAdapter2: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,resourceList)
                        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        resourceSpinner.adapter=spinnerAdapter2

                        resourceSpinner.visibility = View.VISIBLE
                        resourceTV.visibility = View.VISIBLE

                        resourceSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                val resourceSelectedString = resourceList.get(position)
                                var attrList : ArrayList<String> = ArrayList()
                                deviceSelected = spaceSelected.getDevices().filter {
                                    it.getUniqueID().equals(resourceSelectedString)
                                }.firstOrNull()
                                if(deviceSelected != null){
                                    var attrsmap = deviceSelected!!.getDeviceState().getSerialization().keys
                                    attrsmap.removeAll(deviceSelected!!.getDeviceState().getCommandMap().keys)
                                    attrList = ArrayList(attrsmap)
                                    resourceSelected = deviceSelected
                                }else {
                                    resourceSelected = spaceSelected.getResources().getResourceList().filter {
                                        it.getAntecedentID().equals(resourceSelectedString)
                                    }.firstOrNull()
                                    if(resourceSelected!=null){
                                        attrList.add("value")
                                    }
                                }

                                //Populate spinner
                                var spinnerAdapter3: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,attrList)
                                spinnerAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                attributeSpinner.adapter=spinnerAdapter3

                                attributeSpinner.visibility = View.VISIBLE
                                attributeTV.visibility = View.VISIBLE

                                attributeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                                    override fun onNothingSelected(parent: AdapterView<*>?) {
                                    }

                                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                        val operatorString = ArrayList<String>()
                                        operatorString.add("=")
                                        operatorString.add(">")
                                        operatorString.add("<")
                                        operatorString.add(">=")
                                        operatorString.add("<=")

                                        var spinnerAdapter4: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,operatorString)
                                        spinnerAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                        operatorSpinner.adapter=spinnerAdapter4

                                        operatorTV.visibility = View.VISIBLE
                                        operatorSpinner.visibility = View.VISIBLE

                                        valueTV.visibility = View.VISIBLE
                                        valueET.visibility = View.VISIBLE

                                    } // end on item selected of attribute spinner
                                } // end item selected listener of attribute spinner
                            }// end on item selected of resource spinner
                        } // end item selected listener of resource spinner
                    } // end if space seletectd is found
                } // end on item selected of space spinner
            } // end item selected listener of space spinner

            saveBT.setOnClickListener {
                val spaceString = smartspaceSpinner.selectedItem as String
                val operator = operatorSpinner.selectedItem as String
                val attribute = attributeSpinner.selectedItem as String
                val editText = valueET.text
                var value :String
                if(editText == null || editText.isEmpty())
                    value = "25"
                else value= editText.toString()
                if(resourceSelected != null){
                    conditionRule.setParameters(operator,attribute,value,spaceString,resourceSelected!!.getAntecedentID())
                    conditionRule.setPredicate(resourceSelected!!)
                    conditionRule.setcorrectedSaved(true)
                    Log.d("RULE_CONFIGURATION","Condition saved: ${conditionRule.getConditionLabel()}")
                    blockUI(conditionRule.getConditionLabel())
                    deleteBT.setOnClickListener {
                        adapter.removeCondition(conditionRule)
                    }
                    deleteBT.visibility = View.VISIBLE
                    deleteBT.isClickable = true

                }else{
                    Log.d("RULE_CONFIGURATION","Condition unsaved: Device not found, rule is not applied")
                }


            }

        } // end drawUI fun


        fun blockUI(condString: String){
            smartspaceTV.text = condString
            smartspaceSpinner.visibility = View.GONE
            smartspaceSpinner.isClickable = false

            resourceTV.visibility = View.GONE
            resourceSpinner.visibility = View.GONE
            resourceSpinner.isClickable = false

            attributeTV.visibility = View.GONE
            attributeSpinner.visibility = View.GONE
            attributeSpinner.isClickable = false

            operatorTV.visibility = View.GONE
            operatorSpinner.visibility = View.GONE
            operatorSpinner.isClickable = false

            valueTV.visibility = View.GONE
            valueET.visibility = View.GONE
            valueET.isClickable = false

            saveBT.visibility = View.GONE
            saveBT.isClickable = false
        }

        fun drawTimeUI(conditionRule: ConditionRule, adapter: ConditionListAdapter){
            blockUI(conditionRule.getConditionLabel())
            deleteBT.setOnClickListener {
                adapter.removeCondition(conditionRule)
            }
            deleteBT.visibility = View.VISIBLE
            deleteBT.isClickable = true
        }
    } // end condition View Holder

    fun removeCondition(cond:ConditionRule){
        conditionList.remove(cond)
        this.notifyDataSetChanged()
    }

    override fun getItem(p0: Int): Any {
        return conditionList.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return conditionList.count()
    }

}