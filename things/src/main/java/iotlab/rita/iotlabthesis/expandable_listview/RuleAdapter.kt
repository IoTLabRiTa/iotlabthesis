package iotlab.rita.iotlabthesis.expandable_listview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.rules.Rule
import iotlab.rita.iotlabthesis.pattern_observer.message.OnRuleActivatedChangeMessage

class RuleAdapter (var context: Context, var smartHome: SmartHome) : BaseAdapter(){

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var myConvertView: View?
        var holder : RuleViewHolder
        var rule = getItem(position) as Rule

        //Inflate view
        if(convertView==null){
            var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.rule,parent,false)
            holder= RuleViewHolder(myConvertView)
            myConvertView.tag = holder
        }else{
            myConvertView = convertView
            holder = convertView!!.tag as RuleViewHolder
        }


        //Setup Graphic
        holder.drawUI(smartHome,rule,this@RuleAdapter)


        return myConvertView!!
    }

    class RuleViewHolder(var view: View){

        var conditionsTV : TextView
        var consequencesTV : TextView
        var activatedSwitch : Switch
        var deleteBT : Button

        init {
            conditionsTV = view.findViewById(R.id.conditionsTV)
            consequencesTV = view.findViewById(R.id.consequencesTV)
            activatedSwitch  = view.findViewById(R.id.activatedSwitch)
            deleteBT  = view.findViewById(R.id.deleteRuleBT)
        }

        fun drawUI(smartHome: SmartHome,rule:Rule,adapter: RuleAdapter){

            conditionsTV.text = ""
            consequencesTV.text = ""

            rule.getConditionList().forEach {
                conditionsTV.append(it.getConditionLabel()+"\n")
            }

            rule.getConsequenceList().forEach {
                consequencesTV.append(it.getLabel()+"\n")
            }

            activatedSwitch.isChecked= rule.isActivated()
            activatedSwitch.setOnCheckedChangeListener{button, value ->
                rule.setActivated(value)
                smartHome.notifyObserver(OnRuleActivatedChangeMessage(rule))
                if(value)
                    rule.applyRule()
            }

            deleteBT.setOnClickListener {
                smartHome.removeRule(rule)
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun getItem(p0: Int): Rule {
        return smartHome.getRules().get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return smartHome.getRules().count()
    }

}
