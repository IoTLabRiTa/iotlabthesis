package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState

interface ProtocolMessage: MyMessage{

    fun perform(device: Device)

}