package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.communication_protocols.OpenthreadProtocol

class OpenThreadDeviceStateUpdateMessage(var host:String, var map: HashMap<String,String>) : MyMessage {

    //TODO Attenzione: il device notifica la smart home o la stanza e non home control unit
    //TODO Se abbiamo tempo, modificare creando una funzione sul Device che torna la connectionID
    override fun perform(homeControlUnit: HomeControlUnit) {
        try {
            var deviceUpdated=homeControlUnit.getSmartHome().getAllDevices().filter {
                var protocol = it.getCommunicationProtocol()
                if(protocol is OpenthreadProtocol){
                    protocol.getHostAddress().equals(host)
                }else false
            }.first()
            homeControlUnit.update(DeviceStateUpdateMessage(deviceUpdated,map))
        }catch (ex:Exception){
            Log.d("OPENTHREAD_ERROR","Error in updating Openthread device: ${host} not found")
        }

    }


}