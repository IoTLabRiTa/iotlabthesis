package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.controller.HomeControlUnit

interface MyMessage {

    fun perform(homeControlUnit: HomeControlUnit)

}