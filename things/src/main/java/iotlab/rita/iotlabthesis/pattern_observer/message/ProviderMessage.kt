package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.communication_protocols.IoTivityProtocol
import org.iotivity.service.ns.consumer.Provider
import java.util.stream.Collectors

class ProviderMessage(pr:Provider) : MyMessage{

    private var provider: Provider = pr

    override fun perform(homeControlUnit: HomeControlUnit) {
        var string = "Provider found, Topics: ${provider.topicList.topicsList.stream().map { it -> it.topicName }.collect(Collectors.joining(","))}"
        homeControlUnit.log(string)
        Log.d("Provider", provider.topicList.topicsList.toString())
        Thread.sleep(5000)
        try {
            var dev : Device? = homeControlUnit.getSmartHome().getAllDevices().filter { device ->
                provider.topicList.topicsList.stream().map{it.topicName}.anyMatch { it.equals(
                        device.getName()+device.getNameProductor()+device.getModel()+device.getSerialNumber()
                ) }
            }.firstOrNull()
            if(dev != null){
                (dev.getCommunicationProtocol() as IoTivityProtocol).setProvider(provider)
                homeControlUnit.updateLV()
            }
            else{
                Log.d("IOTIVITY_ERROR","Device not found during provider assignment")
                homeControlUnit.log("Device not found during provider assignment")
            }
        }catch (e : Exception){
            val messageError = "Provider error: ${e.message} - #devices: ${homeControlUnit.getSmartHome().getAllDevices().size}"
            Log.d("IOTIVITY_ERROR",messageError)
            homeControlUnit.log(messageError)
        }

    }


}