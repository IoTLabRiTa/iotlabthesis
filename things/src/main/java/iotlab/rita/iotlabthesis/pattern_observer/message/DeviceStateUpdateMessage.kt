package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device


class DeviceStateUpdateMessage(var deviceUpdated: Device, map: HashMap<String,String>) : MyMessage {

    var attributeMap : HashMap<String,String> = map
    var gson = GsonBuilder().create()

    override fun perform(homeControlUnit: HomeControlUnit) {
        //Update device value in local model
        deviceUpdated.convert(attributeMap,false)
        //Update device value in remote database model
        var firebase = homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit()
        firebase.storeDevice(deviceUpdated.getSerialization(),deviceUpdated.getUniqueID())
        //Update smartspace value in remote database model
        var smartSpace = homeControlUnit.getSmartHome().getSmartSpaceOfDevice(deviceUpdated)!!
        firebase.storeSmartSpace(smartSpace)

        //Update nearby devices
        homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().endpoints.forEach {
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendDeviceUpdate(deviceUpdated,true,it)
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendSmartSpace(smartSpace,it)
        }
        //Update GUI
        deviceUpdated.getCommunicationProtocol().waitingNewState = false
        Log.d("DEVICE_UPDATED","Device ${deviceUpdated} has been updated")
        homeControlUnit.log("Device updated, new state: ${deviceUpdated}")
        homeControlUnit.updateLV()

        homeControlUnit.getSmartHome().applyRules()


    }

}