package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.SmartSpace

class DeviceChangeSpaceMessage( var smartSpaceToID: String): ProtocolMessage{


    lateinit var deviceID : String

    override fun perform(device: Device) {
        deviceID = device.getUniqueID()
    }

    override fun perform(homeControlUnit: HomeControlUnit) {
        var smartSpaceTo :SmartSpace?
        if(homeControlUnit.getSmartHome().getName().equals(smartSpaceToID))
            smartSpaceTo = homeControlUnit.getSmartHome()
        else {
            smartSpaceTo = homeControlUnit.getSmartHome().getRooms().filter {
                it.getName().equals(smartSpaceToID)
            }.firstOrNull()
        }
        var device = homeControlUnit.getSmartHome().getDeviceAnyWhere(deviceID)
        var smartSpaceFrom = homeControlUnit.getSmartHome().removeFromSmartSpace(device)
        var attrsmap : HashMap<String,String> = HashMap()
        smartSpaceFrom!!.getResources().resetResources()
        smartSpaceFrom!!.getDevices().forEach {
            attrsmap = it.getSerialization()
            it.convert(attrsmap,true)
        }
        attrsmap = device.getSerialization()
        device.getDeviceState().setInterface(smartSpaceTo!!.getResources())
        device.convert(attrsmap,false)
        smartSpaceTo.addDevice(device)

        //TODO IMPORTANTE INVIARE PRIMA IL TO E DOPO IL FROM, ALTRIMENTI IL DEVICE VIENE ELIMINATO
        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().storeSmartSpace(smartSpaceTo)
        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().storeSmartSpace(smartSpaceFrom)

        var nearbyCommunicationUnit = homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit()
        nearbyCommunicationUnit.endpoints.forEach {
            nearbyCommunicationUnit.sendSmartSpace(smartSpaceTo,it)
            nearbyCommunicationUnit.sendSmartSpace(smartSpaceFrom,it)
        }
        homeControlUnit.updateLV()

    }
}