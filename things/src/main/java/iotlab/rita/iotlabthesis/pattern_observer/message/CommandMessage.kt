package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device

class CommandMessage(var commandString:String,var map:HashMap<String,String>) : ProtocolMessage {


    override fun perform(device: Device) {
        device.getCommunicationProtocol().sendCommand(commandString,map)
    }

    override fun perform(homeControlUnit: HomeControlUnit) {
        homeControlUnit.log("Executed command %${commandString}")
        homeControlUnit.updateLV()
    }
}