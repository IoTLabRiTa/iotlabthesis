package iotlab.rita.iotlabthesis.pattern_observer.message

import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.model.rules.Rule

class ReceivedRuleMessage(var mapAny: LinkedTreeMap<String, Any>) : MyMessage {

    override fun perform(homeControlUnit: HomeControlUnit) {
        val typeAction = mapAny["actionRule"] as String
        when(typeAction){
            "Delete" -> {
                var ruleToDelete = homeControlUnit.getSmartHome().getRules().filter {
                    it.getID() == (mapAny["id"] as String).toInt()
                }.firstOrNull()
                if(ruleToDelete!=null)
                    homeControlUnit.getSmartHome().removeRule(ruleToDelete!!)
            }
            "Activate" ->{
                var ruleToUpdate = homeControlUnit.getSmartHome().getRules().filter {
                    it.getID() == (mapAny["id"] as String).toInt()
                }.firstOrNull()
                if(ruleToUpdate!=null){
                    ruleToUpdate.setActivated(!ruleToUpdate.isActivated())
                    homeControlUnit.getSmartHome().notifyObserver(OnRuleActivatedChangeMessage(ruleToUpdate))
                }
            }
            "Add" ->{
                var newRule = Rule()
                newRule.deserialize(mapAny,homeControlUnit.getSmartHome())
                homeControlUnit.getSmartHome().addRule(newRule)
            }
        }

        //update graphic
        homeControlUnit.updateLV()
    }

}
