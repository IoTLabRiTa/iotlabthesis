package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.device.communication_protocols.CommunicationProtocol

class DeviceDiscoveredMessage(var state: DeviceState, var attrs: HashMap<String,String>, var host : String, var protocol: CommunicationProtocol ) : MyMessage {

    var gson = GsonBuilder().create()

    override fun perform(homeControlUnit: HomeControlUnit) {

        state.setInterface(homeControlUnit.getSmartHome().getResources())

        var device = Device(state, attrs, host, protocol)

        device.registerObserver(homeControlUnit) //TODO registrare la smarthome e non la cu
        //TODO check se esiste un device con UID uguale, lo rimuovo e lo riaggiungo
        if(homeControlUnit.getSmartHome().removeDevice(device))
                Log.d("REMOVE_DEVICE","Removed already present device: ${device.getUniqueID()}")
        homeControlUnit.getSmartHome().addDevice(device)
        Log.d("DEVICE_DISCOVERED","Device ${device.getUniqueID()} is added to smarthome")

        //Update rules
        homeControlUnit.getSmartHome().updateRules()
        homeControlUnit.getSmartHome().applyRules()

        //Adding device to remote database
        var firebase = homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit()
        firebase.storeDevice(device.getSerialization(),device.getUniqueID())
        firebase.storeSmartSpace(homeControlUnit.getSmartHome())

        //Update nearby devices
        homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().endpoints.forEach {
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendDeviceUpdate(device,true,it)
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendSmartSpace(homeControlUnit.getSmartHome(),it)
        }
        //update GUI
        homeControlUnit.log("Device discovered: ${device.getHostAddress()}")
        homeControlUnit.updateLV()
    }
}