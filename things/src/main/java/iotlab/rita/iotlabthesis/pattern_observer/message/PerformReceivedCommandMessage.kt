package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit

class PerformReceivedCommandMessage(var deviceID:String, var command:String, var parameterMap : HashMap<String,String> ) : MyMessage {
    override fun perform(homeControlUnit: HomeControlUnit) {
        var device = homeControlUnit.getSmartHome().getAllDevices().filter{
            it.getUniqueID().equals(deviceID)
        }.firstOrNull()
        if(device != null){
            device.getCommunicationProtocol().sendCommand(command,parameterMap)
            Log.d("DEVICE_CONNECTION","Send command ${command} to ${deviceID}")
        }else{
            Log.d("DEVICE_CONNECTION", "${deviceID} not found : command is not sent back")
        }
    }
}