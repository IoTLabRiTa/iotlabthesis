package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.home_resources.HomeResource

class ResourceMessage<T>(var value : T) {

    fun perform (homeResource: HomeResource<T>){
        Log.d("RESOURCE", "SONO PASSATO 2")
        homeResource.setValue(value)
    }
}