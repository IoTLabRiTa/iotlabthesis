package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.communication_protocols.IoTivityProtocol
import org.iotivity.service.ns.consumer.Provider
import java.lang.Exception

class OnProviderDisconnectedMessage(var providerState:Provider.ProviderState) : MyMessage {


    override fun perform(homeControlUnit: HomeControlUnit) {
        try {
            homeControlUnit.getSmartHome().getAllDevices().forEach {
                var provider = (it.getCommunicationProtocol() as IoTivityProtocol).getProvider()
                if (provider!!.providerState == providerState)
                    provider.unsubscribe()
            }
        }catch (ex:Exception){
            Log.d("IOTIVITY_ERROR","Provider error: ${ex.message}")
        }
    }
}