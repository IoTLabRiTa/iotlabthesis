package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.Device

class OnDeviceDisconnectedMessage() : ProtocolMessage {

    private lateinit var device: Device

    override fun perform(dev: Device) {
        device=dev
    }

    override fun perform(homeControlUnit: HomeControlUnit) {
        Log.d("DELETE_DEVICE","Device ${device.getName()} has been removed")
        homeControlUnit.log("Device ${device.getName()} has been removed")
        //TODO attenzione: rimuoverlo dalla stanza e non dalla home in base a dove è
        //TODO Attenzione: il device notifica la smart home o la stanza e non home control unit
        //TODO notifica recycler view che un elemento è stato eliminato
        //Remove device in local model
        var smartSpaceFrom = homeControlUnit.getSmartHome().removeFromSmartSpace(device)
        var attrsmap : HashMap<String,String> = HashMap()
        smartSpaceFrom!!.getResources().resetResources()
        smartSpaceFrom!!.getDevices().forEach {
            attrsmap = it.getSerialization()
            it.convert(attrsmap,true)
        }

        //Update and apply rules
        homeControlUnit.getSmartHome().updateRules()
        homeControlUnit.getSmartHome().applyRules()

        //Remove device in remote database model
        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().deleteDevice(device.getUniqueID())
        homeControlUnit.getSmartHome().getAllDevices().forEach {
            homeControlUnit.log("Remaining device ${it.getName()} ${it.getSerialNumber()}")
        }

        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().storeSmartSpace(smartSpaceFrom!!)
        homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().endpoints.forEach {
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendDeviceUpdate(device,false,it)
            homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit().sendSmartSpace(smartSpaceFrom!!,it)
        }


        homeControlUnit.updateLV()


    }
}