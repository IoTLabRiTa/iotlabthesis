package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.device.communication_protocols.IoTivityProtocol

class IotivityDeviceStateUpdateMessage(var providerId:String, var map: HashMap<String,String>) : MyMessage{
    override fun perform(homeControlUnit: HomeControlUnit) {
        //TODO Attenzione: il device notifica la smart home o la stanza e non home control unit
        //TODO Se abbiamo tempo, modificare creando una funzione sul Device che torna la connectionID
        try {
            var deviceUpdated=homeControlUnit.getSmartHome().getAllDevices().filter {
                var protocol = it.getCommunicationProtocol()
                if(protocol is IoTivityProtocol){
                     protocol.getProvider()!!.providerId.equals(providerId)
                }else false

            }.first()
            homeControlUnit.update(DeviceStateUpdateMessage(deviceUpdated,map))
        }catch (ex:Exception){
            Log.d("IOTIVITY_ERROR","Error in updating Iotivity device: ${providerId} not found")
        }
    }

}