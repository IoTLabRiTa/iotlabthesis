package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.model.rules.Rule

class OnRuleDeletedMessage(var oldRule: Rule) : MyMessage {
    override fun perform(homeControlUnit: HomeControlUnit) {
        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().deleteRule(oldRule.getID())

        var nearbyCU = homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit()
        nearbyCU.endpoints.forEach {
            nearbyCU.sendRule(oldRule,it,"Delete")
        }
    }

}
