package iotlab.rita.iotlabthesis.pattern_observer.message

import android.util.Log
import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.controller.HomeControlUnit

class NearbySetupDeviceMessage (var endpoint: String) : MyMessage {

    var gson = GsonBuilder().create()

    override fun perform(homeControlUnit: HomeControlUnit) {
        var nearbyCommunicationUnit = homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit()

        //Send devices
        homeControlUnit.getSmartHome().getAllDevices().forEach {
            nearbyCommunicationUnit.sendDeviceUpdate(it,true,endpoint)
        }

        //Send smartspaces
        nearbyCommunicationUnit.sendSmartSpace(homeControlUnit.getSmartHome(),endpoint)

        homeControlUnit.getSmartHome().getRooms().forEach {
            nearbyCommunicationUnit.sendSmartSpace(it,endpoint)
        }

        //Send Rules
        homeControlUnit.getSmartHome().getRules().forEach {
            nearbyCommunicationUnit.sendRule(it,endpoint,"Add")
        }

        Log.d("NEARBY_CONNECTION","Sending ${homeControlUnit.getSmartHome().getAllDevices().size} devices state to nearby devices")
    }

}