package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.controller.HomeControlUnit
import iotlab.rita.iotlabthesis.model.rules.Rule

class OnRuleActivatedChangeMessage(var rule: Rule) : MyMessage {

    override fun perform(homeControlUnit: HomeControlUnit) {
        homeControlUnit.getCommunicationUnit().getFirebaseCommunicationUnit().storeRule(rule)

        var nearbyCU = homeControlUnit.getCommunicationUnit().getNearbyCommunicationUnit()
        nearbyCU.endpoints.forEach {
            nearbyCU.sendRule(rule,it,"Activate")
        }
    }
}