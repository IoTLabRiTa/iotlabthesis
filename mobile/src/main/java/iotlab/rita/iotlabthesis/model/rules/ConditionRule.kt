package iotlab.rita.iotlabthesis.model.rules

import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome
import java.util.function.Predicate

abstract class ConditionRule {

    protected lateinit var antName: String
    protected lateinit var compareAttribute : String
    protected lateinit var comparedValue : String
    protected lateinit var operator : String
    protected lateinit var smartSpace: String
    protected var correctedSaved : Boolean = false
    protected lateinit var label : String


    constructor()

    constructor(map:HashMap<String,String>,smartHome: SmartHome){
        deserialize(map,smartHome)
    }

    constructor(map: LinkedTreeMap<String, String>, smartHome: SmartHome){
        deserialize(map,smartHome)
    }

    abstract fun isTimeBased() : Boolean

    fun getConditionLabel() = label

    fun setcorrectedSaved(value: Boolean){
        correctedSaved = value
    }

    fun isCorrectedSaved() : Boolean = correctedSaved

    open fun setParameters(op:String,attribute:String,value:String,space: String, antname : String){
        antName = antname
        compareAttribute = attribute
        comparedValue = value
        operator = op
        smartSpace = space
    }

    fun deserialize(map: HashMap<String, String>,smartHome: SmartHome){
        val spaceName = map["smartSpace"] as String
        val resourceName = map["antecedent"] as String
        val oper = map["operator"] as String
        val attr = map["attribute"] as String
        val compVal = map["value"] as String
        setParameters(oper,attr,compVal,spaceName,resourceName)
    }

    fun deserialize(map: LinkedTreeMap<String, String>, smartHome: SmartHome){
        val spaceName = map["smartSpace"] as String
        val resourceName = map["antecedent"] as String
        val oper = map["operator"] as String
        val attr = map["attribute"] as String
        val compVal = map["value"] as String
        setParameters(oper,attr,compVal,spaceName,resourceName)
    }


    fun getSerialization() : HashMap<String,String>{
        var map : HashMap<String,String> = HashMap()
        map.put("attribute",compareAttribute)
        map.put("operator",operator)
        map.put("value",comparedValue)
        map.put("smartSpace",smartSpace)
        map.put("antecedent",antName)
        return  map
    }



}