package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import iotlab.rita.iotlabthesis.model.rules.Antecedent
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver

import java.lang.Exception
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit


abstract class HomeResource<T> : Antecedent{

    private lateinit var name : String
    private var value : T? = null
    //private lateinit var lastTimestamp  : LocalDateTime //Oppure mi salvo la liste di tutti i cambiamenti?
    private var isActive : Boolean = false //Indica se c'è almeno un dispositivo che la fornisce

    fun getName() = name
    fun getValue() = value
    //fun getLastTimestamp() = lastTimestamp
    fun isActive() = isActive

    open fun setValue(newValue: T){
        value = newValue
    }

    /*fun setLastTimestamp(currentTimestamp : LocalDateTime){
        lastTimestamp = currentTimestamp
    }
*/

    fun setName(name : String){
        this.name = name
    }

    override fun getSerialization(): HashMap<String,String>{
        var map : HashMap<String, String> = HashMap()
        map.put("resourceName", name)
        map.put("isActive",isActive.toString())
        map.put("value",value.toString())
        return map
    }

    fun deserialize(map : HashMap<String,String>){
        isActive = map.get("isActive")!!.toBoolean()
        deserializeSpecific(map)
    }

    abstract fun deserializeSpecific(map: HashMap<String,String>)

    override fun getAntecedentID(): String = getName()

}
