package iotlab.rita.iotlabthesis.model.home_resources


class PowerConsumptionResource : HomeResource<Int>() {

    init {
        super.setValue(0)
        super.setName("PowerConsumptionResource")
    }

    override fun deserializeSpecific(map: HashMap<String, String>) {
        super.setValue(map.get("value")!!.toInt())
    }

}