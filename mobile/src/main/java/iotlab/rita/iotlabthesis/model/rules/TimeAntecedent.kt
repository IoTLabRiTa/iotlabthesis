package iotlab.rita.iotlabthesis.model.rules

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class TimeAntecedent(private var calendar: Calendar,var dateFormat : SimpleDateFormat) : Antecedent {

    fun getCalendar() = calendar

    override fun getSerialization(): HashMap<String, String> {
        var map : HashMap<String,String> = HashMap()
        map.put("date",getAntecedentID())
        return map
    }

    override fun getAntecedentID(): String {
        return dateFormat.format(calendar.time)
    }
}