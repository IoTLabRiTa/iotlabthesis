package iotlab.rita.iotlabthesis.model

import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.model.rules.Rule


class SmartHome(nameSpace: String) : SmartSpace(nameSpace) {

    private var rooms: ArrayList<SmartRoom> = ArrayList()
    private var rules : ArrayList<Rule> = ArrayList()
    private var RULES_ID = 0

    init {
        rooms.add(SmartRoom("Bathroom"))
        rooms.add(SmartRoom("Bedroom"))
        rooms.add(SmartRoom("Kitchen"))
    }

    fun getRooms() = rooms
    fun getRules() = rules


    fun addRule(newRule: Rule){
        newRule.setID(++RULES_ID)
        rules.add(newRule)
    }

    fun removeRule(oldRuleID : Int){
        var oldRule = rules.filter { it.getID() == oldRuleID }.firstOrNull()
        if(oldRule != null)
            rules.remove(oldRule)
    }

    fun initRule(newrule: HashMap<String, Any>) {
        var newRule = Rule()
        newRule.deserialize(newrule,this)
        if(newRule.getID()>RULES_ID)
            RULES_ID = newRule.getID()
        rules.add(newRule)
    }

    fun initRule(newrule: LinkedTreeMap<String, Any>) {
        var newRule = Rule()
        newRule.deserialize(newrule,this)
        if(newRule.getID()>RULES_ID)
            RULES_ID = newRule.getID()
        rules.add(newRule)
    }

    fun changeRule(changeRuleID : Int,activated:Boolean){
        var ruleToChange = rules.filter {
            it.getID() == changeRuleID
        }.firstOrNull()

        if(ruleToChange != null)
            ruleToChange.setActivated(activated)
    }

    /*
        Retrieve all devices in home and rooms
     */
    fun getAllDevices() : ArrayList<Device>{
        var allDevices: ArrayList<Device> = ArrayList()
        rooms.forEach {
            allDevices.addAll(it.getDevices())
        }
        allDevices.addAll(getDevices())
        return allDevices
    }

    fun removeDevice(device: Device) : Boolean{
        return removeFromSmartSpace(device) != null
    }

    fun removeFromSmartSpace(device: Device) : SmartSpace?{
        if(!this.getDevices().remove(device)){
            this.rooms.forEach {
                if(it.getDevices().remove(device))
                    return it
            }
        }else{
            return this
        }
        return null
    }


    fun deserialize(smartSpaceName: String, devicesIDSmartSpace: ArrayList<String>, resourcesSmartSpace: ArrayList<HashMap<String, String>>, savedDevices : ArrayList<Device>){
        var smartSpace : SmartSpace? = null
        if(this.getName().equals(smartSpaceName)){ //home
            smartSpace = this
        }else{ //Find the room
            rooms.forEach{
                if(it.getName().equals(smartSpaceName)){
                    smartSpace = it
                }
            }
        }

        smartSpace!!.getResources().deserialize(resourcesSmartSpace)

        //Remove unused devices
        var deviceToBeRemoved : ArrayList<Device> = ArrayList()
        smartSpace!!.getDevices().forEach {
            if(!devicesIDSmartSpace.contains(it.getUniqueID()))
                deviceToBeRemoved.add(it)
        }
        smartSpace!!.getDevices().removeAll(deviceToBeRemoved)

        //Add new devices
        smartSpace!!.getDevices().addAll(savedDevices.filter { devicesIDSmartSpace.contains(it.getUniqueID()) && !smartSpace!!.getDevices().contains(it) })

    }

}