package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import java.time.LocalDateTime

class TemperatureResource : HomeResource<Float>() {

    init {
        super.setValue(0F)
        super.setName("TemperatureResource")
    }

    override fun toString(): String {
        return "Temperature: ${super.getValue()} °C"
    }

    override fun deserializeSpecific(map: HashMap<String, String>) {
       super.setValue(map.get("value")!!.toFloat())
    }
}