package iotlab.rita.iotlabthesis.model.rules

interface Antecedent {

    fun getSerialization() : HashMap<String,String>
    fun getAntecedentID():String

}
