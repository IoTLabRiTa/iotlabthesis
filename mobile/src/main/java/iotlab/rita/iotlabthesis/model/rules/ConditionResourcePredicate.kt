package iotlab.rita.iotlabthesis.model.rules

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import java.util.function.Predicate

class ConditionResourcePredicate : ConditionRule {

    constructor():super()

    constructor(map:HashMap<String,String>,smartHome: SmartHome):super(map,smartHome)
    constructor(map: LinkedTreeMap<String, String>, smartHome: SmartHome):super(map,smartHome)

    override fun isTimeBased() = false

    override fun setParameters(op:String,attribute:String,value:String,space: String, antname : String){
        super.setParameters(op, attribute, value, space, antname)
        label = "${smartSpace} ${antName}: ${compareAttribute} ${operator} ${comparedValue}"
    }

}