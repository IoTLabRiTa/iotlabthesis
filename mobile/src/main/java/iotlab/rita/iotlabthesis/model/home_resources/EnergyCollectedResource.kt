package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log

import java.time.LocalDateTime

class EnergyCollectedResource : HomeResource<Float>() {


    init {
        super.setValue(0F)
        super.setName("EnergyCollectedResource")
    }
    override fun toString(): String {
        return "Energy accumulated: ${super.getValue()} °W"
    }

    override fun deserializeSpecific(map: HashMap<String, String>) {
        super.setValue(map.get("value")!!.toFloat())
    }


}