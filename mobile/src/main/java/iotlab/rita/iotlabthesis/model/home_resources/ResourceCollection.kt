package iotlab.rita.iotlabthesis.model.home_resources

import android.util.Log
import iotlab.rita.iotlabthesis.model.rules.Antecedent

class ResourceCollection   {

    private val temperature = TemperatureResource()
    private val energyCollected = EnergyCollectedResource()
    private val powerConsumption = PowerConsumptionResource()
    private var resourceList : ArrayList<Antecedent>

    init {
        resourceList = ArrayList()
        resourceList.add(temperature)
        resourceList.add(energyCollected)
        resourceList.add(powerConsumption)
    }

    fun getTemperature() = temperature
    fun getEnergyCollected() = energyCollected
    fun getPowerConsumption() = powerConsumption

    override fun toString(): String {

        return "${if(temperature.isActive()) "Temperatura: ${temperature.getValue()} °C \n" else ""} " +
                "${if(energyCollected.isActive()) "Energia acc.: ${energyCollected.getValue()} W \n" else ""}" +
                "${if(powerConsumption.isActive()) "Power Consumption: ${powerConsumption.getValue()} W" else ""}"

    }

    fun deserialize(resourcesSmartSpace: ArrayList<HashMap<String, String>>){
        resourcesSmartSpace.forEach {
            var resourceName = it.get("resourceName")
            when(resourceName){
                "TemperatureResource"->temperature.deserialize(it)
                "EnergyCollectedResource"->energyCollected.deserialize(it)
                "PowerConsumptionResource" -> powerConsumption.deserialize(it)
                else -> Log.d("ERROR","RESOURCE NOT FOUND")
            }
        }
    }

    fun getSerialization(): ArrayList<HashMap<String,String>>{
        var result : ArrayList<HashMap<String,String>> = ArrayList()
        result.add(temperature.getSerialization())
        result.add(energyCollected.getSerialization())
        result.add(powerConsumption.getSerialization())
        return result
    }

    fun getResourceList() = resourceList
}