package iotlab.rita.iotlabthesis.model.rules

import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.model.SmartHome
import java.util.*

class ConditionTimePredicate : ConditionRule {

    constructor():super()

    constructor(map: HashMap<String, String>, smartHome: SmartHome):super(map,smartHome)
    constructor(map: LinkedTreeMap<String, String>, smartHome: SmartHome):super(map,smartHome)

    override fun isTimeBased() = true

    override fun setParameters(op: String, attribute: String, value: String, space: String, antname: String) {
        super.setParameters(op, attribute, value, space, antname)
        label = "${op} ${antname}"
    }
}