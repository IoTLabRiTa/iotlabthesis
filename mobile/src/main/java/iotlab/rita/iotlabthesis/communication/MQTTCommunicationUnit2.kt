package iotlab.rita.iotlabthesis.communication

import android.util.Log
import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.DrawerActivity
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

class MQTTCommunicationUnit2(var activity: DrawerActivity) : MyObserver<CommandMessage> {

    var mqttAndroidClient: MqttAndroidClient

    //val serverUri = "tcp://iot.eclipse.org:1883"
    val serverUri = "tcp://131.175.21.173:1883"


    val clientId = "RiTaMobileIoTLab"
    val topic = "HomeControlUnitAndroidThingsIoTLab"

    /*val username = "xxxxxxx"
    val password = "yyyyyyyyyy"*/

    val TAG = "MQTT_CONNECTION"

    init {
        mqttAndroidClient = MqttAndroidClient(activity, serverUri, clientId)
        mqttAndroidClient.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(b: Boolean, s: String) {
                Log.d(TAG,"Connection complete ${s}" )
            }

            override fun connectionLost(throwable: Throwable) {
            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                //DO NOTHING
            }

            override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {
                Log.d(TAG,"Message sent correctly")
            }
        })
        connect()
    }


    private fun connect() {
        val mqttConnectOptions = MqttConnectOptions()
        mqttConnectOptions.isAutomaticReconnect = true
        mqttConnectOptions.isCleanSession = false
        //mqttConnectOptions.connectionTimeout = 60000
        /*mqttConnectOptions.userName = username
        mqttConnectOptions.password = password.toCharArray()*/

        try {

            mqttAndroidClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {

                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.isBufferEnabled = true
                    disconnectedBufferOptions.bufferSize = 100
                    disconnectedBufferOptions.isPersistBuffer = false
                    disconnectedBufferOptions.isDeleteOldestMessages = false
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions)
                    //subscribeToTopic()
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d(TAG, "Failed to connect to: $serverUri$exception")
                }
            })


        } catch (ex: MqttException) {
            Log.d(TAG,"mqtt error to connect: ${ex.message}")
        }

    }


    override fun update(message: CommandMessage) {
        var mapMessage : HashMap<String,String> = message.parameterMap
        mapMessage.put("uniqueID",message.deviceID)
        mapMessage.put("command",message.commandName)
        var jsonMessage = GsonBuilder().create().toJson(mapMessage)
        sendMessage(jsonMessage)

    }

    fun sendMessage(jsonMessage : String){
        var mqttMessage = MqttMessage(jsonMessage.toByteArray())
        mqttMessage.qos = 2
        mqttMessage.isRetained = false
        mqttAndroidClient.publish(topic,mqttMessage)
        Log.d("MQTT_CONNECTION","Sending message ${jsonMessage}")
    }

}