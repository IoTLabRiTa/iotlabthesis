package iotlab.rita.iotlabthesis.communication

import android.util.Log
import com.google.firebase.database.*
import iotlab.rita.iotlabthesis.DrawerActivity
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.gui.RoomsExpandableListAdapter
import iotlab.rita.iotlabthesis.model.SmartHome

class FirebaseCommunicationUnit2(var activity: DrawerActivity, var mqttCU : MQTTCommunicationUnit2) {
    //Firebase var
    private val home = "home"
    private val homeChildDevices = "devices"
    private val homeChildSmartSpaces = "smartSpaces"
    private val rules = "rules"

    var database : DatabaseReference

    init {
        database = FirebaseDatabase.getInstance().reference

        var childListener = object  : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                //Do Nothing
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = activity.smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) {
                    deviceFound.get().convert(attrs)
                    deviceFound.get().waitingNewState = false
                }
                else {
                    device.registerObserver(mqttCU)
                    activity.smartHome.addDevice(device)
                }
                Log.d("MobileUnique",device.getUniqueID())
                activity.homeDevicesFragment.updateLV()

            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = activity.smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) {
                    deviceFound.get().convert(attrs)
                    deviceFound.get().waitingNewState = false
                }
                else {
                    device.registerObserver(mqttCU)
                    activity.smartHome.addDevice(device)
                }
                activity.homeDevicesFragment.updateLV()

            }

            override fun onCancelled(p0: DatabaseError) {
                //Do Nothing
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = activity.smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) activity.smartHome.removeDevice(deviceFound.get())
                /*
                smartHome.removeDevice(dataSnapshot.key!!)
                Log.d("LISTENER", dataSnapshot.key!!)*/
                activity.homeDevicesFragment.updateLV()
            }
        }

        database.child(home).child(homeChildDevices).addChildEventListener(childListener)









        var smartSpacesListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d("MobileUnique","?) Che numero sono? :)")
                var savedDevices = activity.smartHome.getAllDevices()

                dataSnapshot.children.forEach {
                    val snapshotValue = it.getValue() as HashMap<String,Any>
                    var smartSpaceName = snapshotValue.get("name") as String
                    var devicesIDSmartSpace : ArrayList<String> = ArrayList()
                    if(snapshotValue.get("devices") != null)
                        devicesIDSmartSpace = ArrayList((snapshotValue.get("devices") as HashMap<String,String>).values.toMutableList())

                    var resourcesSmartSpace = (snapshotValue.get("resources") as HashMap<String,Any>).values.toMutableList() as ArrayList<HashMap<String,String>>

                    activity.smartHome.deserialize(smartSpaceName,devicesIDSmartSpace,resourcesSmartSpace, savedDevices)

                }
                activity.homeDevicesFragment.updateLV()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("FIREBASE_ERROR", "Error to read : ${databaseError.message}")
            }
        }
        database.child(home).child(homeChildSmartSpaces).addValueEventListener(smartSpacesListener)




        //Rules
            database.child(home).child(rules).addChildEventListener(object : ChildEventListener{
                override fun onChildMoved(dataSnapshot: DataSnapshot, p1: String?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                    if (dataSnapshot != null) {
                        var changeRule = dataSnapshot.getValue() as HashMap<String, Any>
                        val id = (changeRule["id"] as String).toInt()
                        val activated = (changeRule["isActivated"] as String).toBoolean()
                        activity.smartHome.changeRule(id,activated)
                        activity.homeRulesFragment.notifyRule(activity)
                    }
                }

                override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                    if(dataSnapshot.getValue() !=null) {
                        activity.smartHome.initRule(dataSnapshot.getValue() as HashMap<String, Any>)
                        activity.homeRulesFragment.notifyRule(activity)
                    }
                }
                override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                    if(dataSnapshot.getValue() !=null) {
                        activity.smartHome.removeRule(((dataSnapshot.getValue() as HashMap<String, Any>)["id"] as String).toInt())
                        activity.homeRulesFragment.notifyRule(activity)
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                    //Do nothing
                }
            })

    }



}