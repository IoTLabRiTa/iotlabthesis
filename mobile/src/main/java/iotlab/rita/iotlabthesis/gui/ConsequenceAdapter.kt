package iotlab.rita.iotlabthesis.expandable_listview

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.device.Command
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.fragment.CommandDialog
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.model.rules.Consequence
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver

class ConsequenceAdapter (var context: Context, var fragmentManager: FragmentManager, var smartHome: SmartHome, var consequenceList : ArrayList<Consequence> ): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var myConvertView: View?
        var holder : ConsequenceViewHolder
        var consequence = getItem(position) as Consequence

        //Inflate view
        if(convertView==null){
            var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.consequence_rule,parent,false)
            holder= ConsequenceViewHolder(myConvertView)
            myConvertView.tag = holder
        }else{
            myConvertView = convertView
            holder = convertView!!.tag as ConsequenceViewHolder
        }


        //Setup Graphic
        holder.drawUI(context,smartHome,consequence,fragmentManager,this@ConsequenceAdapter)


        return myConvertView!!
    }

    fun removeConsequence(consequence: Consequence){
        consequenceList.remove(consequence)
        this.notifyDataSetChanged()
    }

    class ConsequenceViewHolder(var view: View) : MyObserver<CommandMessage>{


        var spaceTV :TextView
        var deviceTV : TextView
        var commandTV :TextView

        var spaceSpinner : Spinner
        var deviceSpinner : Spinner
        var commandSpinner : Spinner

        var saveBT : Button
        var deleteBT : Button

        var consequence : Consequence? = null
        var device : Device? = null


        init {
            spaceTV = view.findViewById(R.id.spaceTV)
            deviceTV = view.findViewById(R.id.deviceChoosenTV)
            commandTV = view.findViewById(R.id.commandTV)

            spaceSpinner = view.findViewById(R.id.spaceSpinner)
            deviceSpinner = view.findViewById(R.id.deviceChoosenSpinner)
            commandSpinner = view.findViewById(R.id.commandChoosenSpinner)

            saveBT = view.findViewById(R.id.saveCommandBT)
            deleteBT = view.findViewById(R.id.deleteCommandBT)
            deleteBT.isClickable = false
            deleteBT.visibility = View.GONE
        }

        override fun update(msg: CommandMessage) {
            var message = msg
            if(consequence!=null && device!=null){
                consequence!!.setParameters(device!!.getUniqueID(), message.commandName,message.parameterMap)
                consequence!!.setCorrectedSaved(true)
            }
            blockUI(consequence!!.getLabel())
        }

        fun drawUI(context: Context, smartHome: SmartHome, cons: Consequence, fragmentManager: FragmentManager, adapter: ConsequenceAdapter){
            var commandMap : HashMap<String,Command> = HashMap()
            var deviceSelected : Device? = null
            var spaceList = ArrayList<SmartSpace> ()
            spaceList.add(smartHome)
            spaceList.addAll(smartHome.getRooms())
            var smartSpaceList = ArrayList<String>()
            smartSpaceList.add(smartHome.getName())
            smartHome.getRooms().forEach {
                smartSpaceList.add(it.getName())
            }
            //Populate spinner
            var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,smartSpaceList)
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spaceSpinner.adapter=spinnerAdapter

            spaceSpinner.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val spaceSelectedString = smartSpaceList.get(position)
                    var spaceSelected = spaceList.filter {
                        it.getName().equals(spaceSelectedString)
                    }.firstOrNull()
                    if (spaceSelected != null) {
                        val deviceList = ArrayList<String>()
                        spaceSelected.getDevices().forEach {
                            deviceList.add(it.getUniqueID())
                        }

                        //Populate spinner
                        var spinnerAdapter2: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item,deviceList)
                        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        deviceSpinner.adapter=spinnerAdapter2

                        deviceSpinner.visibility = View.VISIBLE
                        deviceTV.visibility = View.VISIBLE

                        deviceSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                val deviceSelectedString = deviceList.get(position)
                                deviceSelected = spaceSelected.getDevices().filter {
                                    it.getUniqueID().equals(deviceSelectedString)
                                }.firstOrNull()

                                if(deviceSelected != null) {
                                    commandMap = deviceSelected!!.getDeviceState().getCommandMap()
                                    commandMap.put("onOff",
                                            Command("onOff","On/Off del device-onOffState,Boolean, , ,Switch"))

                                    //Populate spinner
                                    var spinnerAdapter3: ArrayAdapter<String> = ArrayAdapter(context, R.layout.spinner_item, ArrayList(commandMap.keys))
                                    spinnerAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                    commandSpinner.adapter = spinnerAdapter3

                                    commandSpinner.visibility = View.VISIBLE
                                    commandSpinner.visibility = View.VISIBLE

                                }// end if device is found

                            } // end of on item selected of device spinner
                        }// end of item selected listener of device spinner

                    } // end if space selected is found
                } // end of on item selected of space spinner
            } // end of item selected listener of space spinner

            saveBT.setOnClickListener {

                val commandSelectedString = commandSpinner.selectedItem as String
                var commandSelected = commandMap[commandSelectedString]
                if (deviceSelected != null && commandSelected != null) {
                    device = deviceSelected
                    consequence = cons
                    deleteBT.setOnClickListener {
                        adapter.removeConsequence(cons)
                    }
                    var bundle: Bundle = Bundle()
                    //TODO IL comando nel bundle ha i parametri vuoti
                    bundle.putSerializable("command",commandSelected)
                    var commandDialog: CommandDialog = CommandDialog()
                    commandDialog.arguments = bundle
                    commandDialog.registerObserver(this@ConsequenceViewHolder)
                    commandDialog.show(fragmentManager,"CommandDialog")
                    Log.d("RULE_CONFIGURATION", "Consequence saved for ${deviceSelected!!.getUniqueID()}")
                } else {
                    Log.d("RULE_CONFIGURATION", "Consequence unsaved: Device or Command not found, rule is not applied")
                }
            }// end of savebutton

        } // end of drawUI of ViewHolder


        fun blockUI(commandString:String){
            spaceTV.text = commandString
            //spaceSpinner.alpha = 0.4f
            spaceSpinner.visibility = View.GONE
            spaceSpinner.isClickable = false

            deviceTV.visibility = View.GONE
            deviceSpinner.visibility = View.GONE
            deviceSpinner.isClickable = false

            commandTV.visibility = View.GONE
            commandSpinner.visibility = View.GONE
            commandSpinner.isClickable = false

            saveBT.visibility = View.GONE
            saveBT.isClickable = false


            /*deviceTV.alpha = 0.4f
            deviceSpinner.alpha = 0.4f
            deviceSpinner.isClickable = false

            commandTV.alpha = 0.4f
            commandSpinner.alpha = 0.4f
            commandSpinner.isClickable = false

            saveBT.alpha = 0.4f
            saveBT.isClickable = false*/

            deleteBT.visibility = View.VISIBLE
            deleteBT.isClickable = true

        }
    } // end of ConsequenceViewHolder

    override fun getItem(p0: Int): Any {
        return consequenceList.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return consequenceList.count()
    }

}
