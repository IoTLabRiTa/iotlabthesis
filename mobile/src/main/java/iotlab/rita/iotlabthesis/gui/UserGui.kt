package iotlab.rita.iotlabthesis.gui

import android.support.v4.app.FragmentManager
import android.widget.*
import iotlab.rita.iotlabthesis.trash_bin.MainActivity
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.model.SmartSpace

class UserGui(activity: MainActivity, smartSpaces: ArrayList<SmartSpace>) {

    //Grafica
    var expandableListView: ExpandableListView
    private var expandableListAdapter: RoomsExpandableListAdapter
    var fragmentManager : FragmentManager

    init {

        fragmentManager = activity.supportFragmentManager
        expandableListView = activity.findViewById(R.id.expandableListView)
        expandableListAdapter = RoomsExpandableListAdapter(activity,smartSpaces,fragmentManager)
        expandableListView.setAdapter(expandableListAdapter)

    }
}