package iotlab.rita.iotlabthesis

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import iotlab.rita.iotlabthesis.communication.FirebaseCommunicationUnit2
import iotlab.rita.iotlabthesis.communication.MQTTCommunicationUnit2
import iotlab.rita.iotlabthesis.communication.NearbyCommunicationUnit2
import iotlab.rita.iotlabthesis.fragment.HomeDevicesFragment
import iotlab.rita.iotlabthesis.fragment.HomeRulesFragment
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import kotlinx.android.synthetic.main.activity_drawer.*
import kotlinx.android.synthetic.main.app_bar_drawer.*

class DrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,MyObserver<MyMessage> {


    lateinit var homeDevicesFragment : HomeDevicesFragment
    lateinit var homeRulesFragment : HomeRulesFragment
    //lateinit var roomManagementFragment: RoomManagementFragment

    //mqtt
    private lateinit var mqttCU : MQTTCommunicationUnit2
    //Nearby
    private lateinit var nearbyCU : NearbyCommunicationUnit2
    //Firebase
    private lateinit var firebaseCU : FirebaseCommunicationUnit2
    //Model
    var smartHome : SmartHome = SmartHome("Home")

    var localConnection : Boolean = true





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        var  navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)


        initFragments()

        //TODO RIPRISTINARE
        nearbyCU = NearbyCommunicationUnit2(this)

        var ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_frame, this.homeDevicesFragment);
        ft.commit();

    }

    private fun initFragments() {
        homeDevicesFragment = HomeDevicesFragment()
        homeDevicesFragment.registerObserver(this)
        homeRulesFragment = HomeRulesFragment()
        homeRulesFragment.registerObserver(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var ft = getSupportFragmentManager().beginTransaction();
        when (item.itemId) {
            R.id.nav_devices -> {
                ft.replace(R.id.main_frame, this.homeDevicesFragment);
                ft.commit()
            }
            R.id.nav_rules -> {
                ft.replace(R.id.main_frame, this.homeRulesFragment);
                ft.commit()
            }
            R.id.nav_about -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    fun startRemoteConnection(){
        localConnection = false
        mqttCU = MQTTCommunicationUnit2(this)
        firebaseCU = FirebaseCommunicationUnit2(this, mqttCU)
    }

    override fun update(message: MyMessage) {
        message.perform(this)
    }

    fun getMQTTCU() = mqttCU
    fun getNearbyCU() = nearbyCU
}
