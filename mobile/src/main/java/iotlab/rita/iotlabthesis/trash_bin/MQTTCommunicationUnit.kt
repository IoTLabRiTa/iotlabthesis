package iotlab.rita.iotlabthesis.trash_bin

import android.util.Log
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.IMqttToken
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver
import org.eclipse.paho.client.mqttv3.MqttConnectOptions


class MQTTCommunicationUnit(var context: MainActivity) : MyObserver<CommandMessage> {


    var mqttAndroidClient: MqttAndroidClient

    val serverUri = "tcp://iot.eclipse.org:1883"

    val clientId = "RiTaMobileIoTLab"
    val topic = "HomeControlUnitAndroidThingsIoTLab"

    /*val username = "xxxxxxx"
    val password = "yyyyyyyyyy"*/

    val TAG = "MQTT_CONNECTION"

    init {
        mqttAndroidClient = MqttAndroidClient(context, serverUri, clientId)
        mqttAndroidClient.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(b: Boolean, s: String) {
                Log.d(TAG,"Connection complete ${s}" )
            }

            override fun connectionLost(throwable: Throwable) {

            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                /*try{
                    val type : HashMap<String,String> = HashMap()
                    var map : HashMap<String,String> = Gson().fromJson(mqttMessage.toString(),type.javaClass)
                    val deviceID = map["uniqueID"]
                    val command = map["command"]
                    map.remove("name")
                    map.remove("command")
                    Log.d(TAG, "Message received: ${mqttMessage}")
                }catch (e:Exception){
                    Log.d(TAG, "Error to parse message received: ${mqttMessage}")
                }*/

            }

            override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {
                Log.d(TAG,"Message sent correctly")
            }
        })
        connect()
    }


    private fun connect() {
        val mqttConnectOptions = MqttConnectOptions()
        mqttConnectOptions.isAutomaticReconnect = true
        mqttConnectOptions.isCleanSession = false
        //mqttConnectOptions.connectionTimeout = 60000
        /*mqttConnectOptions.userName = username
        mqttConnectOptions.password = password.toCharArray()*/

        try {

            mqttAndroidClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {

                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.isBufferEnabled = true
                    disconnectedBufferOptions.bufferSize = 100
                    disconnectedBufferOptions.isPersistBuffer = false
                    disconnectedBufferOptions.isDeleteOldestMessages = false
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions)
                    //subscribeToTopic()
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d(TAG, "Failed to connect to: $serverUri$exception")
                }
            })


        } catch (ex: MqttException) {
            Log.d(TAG,"mqtt error to connect: ${ex.message}")
        }

    }

    /*private fun subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    Log.d(TAG, "Subscribed!")
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.d(TAG, "Subscribed fail!")
                }
            })

        } catch (ex: MqttException) {
            Log.d(TAG,"mqtt ERROR subscribing: ${ex.message}")
        }

    }*/

    override fun update(message: CommandMessage) {
        var mapMessage : HashMap<String,String> = message.parameterMap
        mapMessage.put("uniqueID",message.deviceID)
        mapMessage.put("command",message.commandName)
        var jsonMessage = GsonBuilder().create().toJson(mapMessage)
        var mqttMessage = MqttMessage(jsonMessage.toByteArray())
        mqttMessage.qos = 2
        mqttMessage.isRetained = false
        mqttAndroidClient.publish(topic,mqttMessage)
        Log.d("MQTT_CONNECTION","Sending message ${jsonMessage}")


    }

}