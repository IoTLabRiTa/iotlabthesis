package iotlab.rita.iotlabthesis.trash_bin

import android.util.Log
import android.widget.Toast
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import com.google.gson.GsonBuilder
import com.google.gson.internal.LinkedTreeMap
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.gui.RoomsExpandableListAdapter
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObserver

class NearbyCommunicationUnit(var activity: MainActivity) : MyObserver<CommandMessage>{

    val LOG_TAG = "NEARBY_CONNECTION"
    val thingsNickname = "IoTLabAndroidThings"
    var isLocalConnected = false


    //Nearby var
    private lateinit var endpointCU : String
    private lateinit  var payloadCallback : PayloadCallback
    private lateinit var endpointDiscoveryCallback : EndpointDiscoveryCallback
    private lateinit var connectionLifecycleCallback : ConnectionLifecycleCallback

    //Gson
    private var gson = GsonBuilder().create()

    init {
        Nearby.getConnectionsClient(activity).stopDiscovery()
        startNearbyAPI()
    }

    private fun startNearbyAPI() {

        payloadCallback = object : PayloadCallback(){

            private var receivedMessage : String = ""

            override fun onPayloadReceived(endpoint: String, payload: Payload) {
                receivedMessage = String(payload!!.asBytes()!!, 0,payload!!.asBytes()!!.size )
                //Previous CODE
                /*
                Log.d(LOG_TAG,"Message received: ${receivedMessage} from ${endpoint} ")
                val type : HashMap<String,String> = HashMap()
                val attrs = gson.fromJson(receivedMessage,type.javaClass)
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = activity.smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) deviceFound.get().convert(attrs)
                else {
                    device.registerObserver(this@NearbyCommunicationUnit)
                    activity.smartHome.addDevice(device)
                }*/

                Log.d(LOG_TAG,"Message received: ${receivedMessage} from ${endpoint} ")
                val type : HashMap<String,Any> = HashMap()
                val map = gson.fromJson(receivedMessage,type.javaClass)

                //Check if device state is received
                if(map.get("serialNumber") != null){
                    val typeDeviceMessage : HashMap<String,String> = HashMap()
                    val attrs = gson.fromJson(receivedMessage,typeDeviceMessage.javaClass)
                    val alive = attrs.remove("alive")!!.toBoolean()
                    val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                    var class_state = Class.forName(name)
                    var construct_state = class_state.getDeclaredConstructor()
                    var state = (construct_state.newInstance()) as DeviceState
                    val device = Device(state, attrs)
                    Log.d("READ_DEVICE", "Device: ${device}")
                    var deviceFound = activity.smartHome.getAllDevices().stream().filter{
                        it.equals(device)
                    }.findFirst()
                    if(deviceFound.isPresent){
                        if(alive)
                            deviceFound.get().convert(attrs)
                        else
                            activity.smartHome.removeDevice(deviceFound.get())
                    }
                    else {
                        device.registerObserver(this@NearbyCommunicationUnit)
                        activity.smartHome.addDevice(device)
                    }
                }else{  //Received a smartSpaceUpdate
                    var smartSpaceName = map.get("name") as String
                    var devicesIDSmartSpace : ArrayList<String> = ArrayList()
                    if((map.get("devices") as LinkedTreeMap<String,String>).isNotEmpty())
                        devicesIDSmartSpace = ArrayList((map.get("devices") as LinkedTreeMap<String,String>).values.toMutableList())
                    var resourcesSmartSpace = (map.get("resources") as LinkedTreeMap<String,String>).values.toMutableList() as ArrayList<LinkedTreeMap<String,String>>

                    var resourceList : ArrayList<HashMap<String,String>> = ArrayList()

                    resourcesSmartSpace.forEach {
                        var linkedTree = it
                        var temp : HashMap<String,String> = HashMap()
                        linkedTree.keys.forEach{
                            temp.put(it,linkedTree[it]!!)
                        }
                        resourceList.add(temp)

                    }



                    activity.smartHome.deserialize(smartSpaceName,devicesIDSmartSpace,resourceList, activity.smartHome.getAllDevices()) //TODO CHECK GETALLDEVICES SIA CORRETTA IN QUESTO CASO
                }

                (activity.gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }

            override fun onPayloadTransferUpdate(endpoint: String, update: PayloadTransferUpdate) {
                when(update.status){
                    PayloadTransferUpdate.Status.IN_PROGRESS ->{
                        var size = update.totalBytes
                        if (size.equals(-1)){
                            //i'm receiving a stream
                        }
                    }
                    PayloadTransferUpdate.Status.SUCCESS ->{
                        //Transferred 100%
                    }
                    PayloadTransferUpdate.Status.FAILURE -> {
                        //ERROR
                    }
                }
            }
        }

        connectionLifecycleCallback = object : ConnectionLifecycleCallback() {

            override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
                Nearby.getConnectionsClient(activity).acceptConnection(endpointId, payloadCallback)
                isLocalConnected = true
            }

            override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
                when (result.status.statusCode) {
                    ConnectionsStatusCodes.STATUS_OK -> {
                        Log.d(LOG_TAG, "Accept connection with ${endpointId}")
                        endpointCU = endpointId
                    }
                    ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                        Log.d(LOG_TAG, "Reject connection with ${endpointId}")
                    }
                    ConnectionsStatusCodes.STATUS_ERROR -> {
                        Log.d(LOG_TAG, "Error to connect to ${endpointId}")
                    }
                }
            }

            override fun onDisconnected(endpointId: String) {
                Log.d(LOG_TAG, "Lost connection with ${endpointId}, device disconnected")
                activity.runOnUiThread{
                    Toast.makeText(activity,"Lost connection to Home Control Unit", Toast.LENGTH_SHORT).show()
                }
            }
        }

        endpointDiscoveryCallback = object : EndpointDiscoveryCallback() {
            override fun onEndpointFound(endpointId: String, discoveredEndpointInfo: DiscoveredEndpointInfo) {
                Log.d(LOG_TAG,"Found endpoint: ${endpointId}")
                Nearby.getConnectionsClient(activity).requestConnection(
                        "thingsNickname",
                        endpointId,
                        connectionLifecycleCallback)
                        .addOnSuccessListener {
                            Log.d(LOG_TAG,"Successfully requested connection")
                        }.addOnFailureListener(){
                            Log.d(LOG_TAG,"Failed to request a connection to ${it.message}")
                        }
            }

            override fun onEndpointLost(endpointId: String) {
                Log.d(LOG_TAG,"Lost the discovered ${endpointId} endpoint")
                activity.runOnUiThread{
                    Toast.makeText(activity,"Lost connection to Home Control Unit", Toast.LENGTH_SHORT).show()
                }
            }
        }

        Nearby.getConnectionsClient(activity).startDiscovery(
                thingsNickname,
                endpointDiscoveryCallback,
                DiscoveryOptions.Builder().setStrategy(Strategy.P2P_STAR).build())
                .addOnSuccessListener(){ void ->
                    Log.d(LOG_TAG,"Start to discovery nearby devices...")
                    activity.runOnUiThread{
                        Toast.makeText(activity,"Searching for nearby Home Control Unit", Toast.LENGTH_SHORT).show()
                    }
                    Thread{
                        Thread.sleep(15000)
                        if(!isLocalConnected){
                            Log.d("REMOTE_CONNECTION","Home Control Unit not found, starting remote connection")
                            activity.runOnUiThread{
                                Toast.makeText(activity,"Home Control Unit not found, starting remote connection", Toast.LENGTH_SHORT).show()
                                Nearby.getConnectionsClient(activity).stopDiscovery()
                                activity.startRemoteConnection()
                            }
                        }
                    }.start()
                }
                .addOnFailureListener(){void ->
                    Log.d(LOG_TAG,"Problem finding nearby Home Control Unit, starting remote connection")
                    activity.runOnUiThread{
                        Toast.makeText(activity,"Problem finding nearby Home Control Unit, starting remote connection", Toast.LENGTH_SHORT).show()
                        Nearby.getConnectionsClient(activity).stopDiscovery()
                        activity.startRemoteConnection()
                    }
                }
    }


    override fun update(message: CommandMessage) {
        var mapMessage : HashMap<String,String> = message.parameterMap
        mapMessage.put("uniqueID",message.deviceID)
        mapMessage.put("command",message.commandName)
        var jsonMessage = gson.toJson(mapMessage)
        val bytesPayload = Payload.fromBytes(jsonMessage.toByteArray())
        Nearby.getConnectionsClient(activity).sendPayload(endpointCU, bytesPayload)
        Log.d(LOG_TAG,"Sending commad message: ${message.commandName} to ${endpointCU} \n")

    }

}
