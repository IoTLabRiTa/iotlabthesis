package iotlab.rita.iotlabthesis.trash_bin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.database.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.device.Device
import iotlab.rita.iotlabthesis.device.DeviceState
import iotlab.rita.iotlabthesis.gui.RoomsExpandableListAdapter
import iotlab.rita.iotlabthesis.gui.UserGui
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace


class MainActivity : AppCompatActivity() {



    //Firebase var
    private val home = "home"
    private val homeChildDevices = "devices"
    private val homeChildSmartSpaces = "smartSpaces"
    private lateinit var database : DatabaseReference

    //Model
    //var deviceList : ArrayList <Device> = ArrayList()
    var smartHome : SmartHome = SmartHome("Home")

    //Grafica
    lateinit var gui: UserGui

    //mqtt
    private lateinit var mqttCU : MQTTCommunicationUnit

    //Nearby
    private lateinit var nearbyCU : NearbyCommunicationUnit



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var smartspaces : ArrayList<SmartSpace> = ArrayList()
        smartspaces.add(smartHome)
        smartspaces.addAll(smartHome.getRooms())
        gui = UserGui(this, smartspaces)
        nearbyCU = NearbyCommunicationUnit(this)
        //startRemoteConnection()
    }






    fun startRemoteConnection(){
        mqttCU = MQTTCommunicationUnit(this)
        database = FirebaseDatabase.getInstance().reference

        //ADD TEST prima versione
        /*
        var name = "Home"
        var mapSmartSpace : HashMap<String,String> = HashMap()
        mapSmartSpace.put("name",name)
        database.child(home).child(homeChildSmartSpaces).child(name).setValue(mapSmartSpace)
        Log.d("MobileUnique","1) aggiunto nome")

        var mapDevices : HashMap<String,String> = HashMap()
        mapDevices.put("device0","SolarPanelRiTaSP30003594")
        mapDevices.put("device1","TemperatureSensorRitaPremium10")
        database.child(home).child(homeChildSmartSpaces).child(name).child(homeChildDevices).setValue(mapDevices)
        Log.d("MobileUnique","2) aggiunta devices")

        var map : HashMap<String, String> = HashMap()
        map.put("resourceName", "TemperatureResource")
        map.put("lastTimestamp","ieri")
        map.put("isActive","true")
        map.put("value","100")
        database.child(home).child(homeChildSmartSpaces).child(name).child("resources").child(map["resourceName"]!!).setValue(map)
        Log.d("MobileUnique","3) aggiunta prima risorsa")

        map = HashMap()
        map.put("resourceName", "EnergyCollectedResource")
        map.put("lastTimestamp","ieri1")
        map.put("isActive","true")
        map.put("value","1000")
        database.child(home).child(homeChildSmartSpaces).child(name).child("resources").child(map["resourceName"]!!).setValue(map)
        Log.d("MobileUnique","4) aggiunta seconda risorsa")
        */
        //FINE ADD

        //Add test seconda versione con singolo update
        /*
        var name = "Home"
        var mapSmartSpace : HashMap<String,Any> = HashMap()
        mapSmartSpace.put("name",name)

        //Store devices
        var mapDevices : HashMap<String,String> = HashMap()
        mapDevices.put("device0","SolarPanelRiTaSP30003594")
        mapDevices.put("device1","TemperatureSensorRitaPremium10")
        mapSmartSpace.put("devices",mapDevices)

        var resourceMap : HashMap<String,Any> = HashMap()
        var temperatureMap : HashMap<String,String> = HashMap()
        temperatureMap.put("resourceName", "TemperatureResource")
        temperatureMap.put("lastTimestamp","ieri")
        temperatureMap.put("isActive","true")
        temperatureMap.put("value","100")
        resourceMap.put("TemperatureResource",temperatureMap)

        var energyMap : HashMap<String,String> = HashMap()
        energyMap.put("resourceName", "EnergyCollectedResource")
        energyMap.put("lastTimestamp","ieri1")
        energyMap.put("isActive","true")
        energyMap.put("value","1000")
        resourceMap.put("EnergyCollectedResource",energyMap)
        mapSmartSpace.put("resources",resourceMap)


        val type : HashMap<String,Any> = HashMap()
        var gson = GsonBuilder().create()

        val jsonString1 = gson.toJson(mapSmartSpace,type.javaClass)
        val jsonString2 = JSONObject(mapSmartSpace)
        Log.d("MobileUnique","1 $jsonString1")
        Log.d("MobileUnique","2 = $jsonString2")


        database.child(home).child(homeChildSmartSpaces).child(name).setValue(mapSmartSpace)
*/

        /*var deviceListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSnapshot.children.forEach {
                    val attrs = it.getValue() as HashMap<String, String>
                    val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                    var class_state = Class.forName(name)
                    var construct_state = class_state.getDeclaredConstructor()
                    var state = (construct_state.newInstance()) as DeviceState
                    val device = Device(state, attrs)
                    Log.d("READ_DEVICE", "Device: ${device}")
                    var deviceFound = smartHome.getAllDevices().stream().filter{
                        it.equals(device)
                    }.findFirst()
                    if(deviceFound.isPresent) deviceFound.get().convert(attrs)
                    else {
                        device.registerObserver(mqttCU)
                        smartHome.addDevice(device)
                    }

                    var childListener = object  : ChildEventListener{
                        override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                            //Do Nothing
                        }

                        override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                            //Do Nothing
                        }

                        override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                            //Do Nothing
                        }

                        override fun onCancelled(p0: DatabaseError) {
                            //Do Nothing
                        }

                        override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                            smartHome.removeDevice(dataSnapshot.key!!)
                            Log.d("LISTENER", dataSnapshot.key!!)
                            (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
                        }
                    }
                    database.child(home).child(homeChildSmartSpaces)/*.child(device.getUniqueID())*/.addChildEventListener(childListener)



                }

                (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("FIREBASE_ERROR", "Error to read : ${databaseError.message}")
            }
        }
        database.child(home).child(homeChildDevices).addValueEventListener(deviceListener)
*/

        var childListener = object  : ChildEventListener{
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                //Do Nothing
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) {
                    deviceFound.get().convert(attrs)
                    deviceFound.get().waitingNewState = false
                }
                else {
                    device.registerObserver(mqttCU)
                    smartHome.addDevice(device)
                }
                Log.d("MobileUnique",device.getUniqueID())
                (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()

            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) deviceFound.get().convert(attrs)
                else {
                    device.registerObserver(mqttCU)
                    smartHome.addDevice(device)
                }
                (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()

            }

            override fun onCancelled(p0: DatabaseError) {
                //Do Nothing
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                val attrs = dataSnapshot.getValue() as HashMap<String, String>
                val name = "iotlab.rita.iotlabthesis.device." + attrs["name"]
                var class_state = Class.forName(name)
                var construct_state = class_state.getDeclaredConstructor()
                var state = (construct_state.newInstance()) as DeviceState
                val device = Device(state, attrs)
                Log.d("READ_DEVICE", "Device: ${device}")
                var deviceFound = smartHome.getAllDevices().stream().filter{
                    it.equals(device)
                }.findFirst()
                if(deviceFound.isPresent) smartHome.removeDevice(deviceFound.get())
                /*
                smartHome.removeDevice(dataSnapshot.key!!)
                Log.d("LISTENER", dataSnapshot.key!!)*/
                (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }
        }

        database.child(home).child(homeChildDevices).addChildEventListener(childListener)









        var smartSpacesListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.d("MobileUnique","?) Che numero sono? :)")
                var savedDevices = smartHome.getAllDevices()

                dataSnapshot.children.forEach {
                    val snapshotValue = it.getValue() as HashMap<String,Any>
                    var smartSpaceName = snapshotValue.get("name") as String
                    var devicesIDSmartSpace : ArrayList<String> = ArrayList()
                    if(snapshotValue.get("devices") != null)
                        devicesIDSmartSpace = ArrayList((snapshotValue.get("devices") as HashMap<String,String>).values.toMutableList())

                    var resourcesSmartSpace = (snapshotValue.get("resources") as HashMap<String,Any>).values.toMutableList() as ArrayList<HashMap<String,String>>

                    smartHome.deserialize(smartSpaceName,devicesIDSmartSpace,resourcesSmartSpace, savedDevices)

                }
                (gui.expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("FIREBASE_ERROR", "Error to read : ${databaseError.message}")
            }
        }
        database.child(home).child(homeChildSmartSpaces).addValueEventListener(smartSpacesListener)

    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}
