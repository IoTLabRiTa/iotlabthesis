package iotlab.rita.iotlabthesis.pattern_observer.message

import com.google.gson.GsonBuilder
import iotlab.rita.iotlabthesis.DrawerActivity
import iotlab.rita.iotlabthesis.model.rules.Rule

class OnRuleMessage(var rule: Rule, var actionRule : String) : MyMessage {
    override fun perform(activity: DrawerActivity) {
        var mapMessage = rule.getSerialization()
        mapMessage.put("command","ActionRule")
        mapMessage.put("actionRule", actionRule)
        var jsonMessage = GsonBuilder().create().toJson(mapMessage)

        if(activity.localConnection)
            activity.getNearbyCU().sendMessage(jsonMessage)
        else
            activity.getMQTTCU().sendMessage(jsonMessage)
    }
}