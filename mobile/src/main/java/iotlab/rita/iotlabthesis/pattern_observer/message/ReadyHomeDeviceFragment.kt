package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.DrawerActivity

class ReadyHomeDeviceFragment : MyMessage {
    override fun perform(activity: DrawerActivity) {
        activity.homeDevicesFragment.setup(activity.smartHome)
    }

}