package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.DrawerActivity


interface MyMessage {

    fun perform(activity: DrawerActivity)

}