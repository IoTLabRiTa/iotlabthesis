package iotlab.rita.iotlabthesis.pattern_observer.message

import android.support.v4.view.GravityCompat
import android.view.Gravity
import iotlab.rita.iotlabthesis.DrawerActivity
import kotlinx.android.synthetic.main.activity_drawer.*

class OpenDrawerMessage : MyMessage {

    override fun perform(activity: DrawerActivity) {
        if(!activity.drawer_layout.isDrawerOpen(GravityCompat.START)) activity.drawer_layout.openDrawer(Gravity.START);
        else activity.drawer_layout.closeDrawer(Gravity.END);
    }
}