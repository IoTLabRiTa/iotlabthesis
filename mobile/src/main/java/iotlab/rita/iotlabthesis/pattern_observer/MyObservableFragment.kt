package iotlab.rita.iotlabthesis.pattern_observer

import android.support.v4.app.Fragment
import java.util.ArrayList

abstract class MyObservableFragment<T> : Fragment() {

    private var observers: ArrayList<MyObserver<T>>? = ArrayList()



    fun registerObserver(myObserver: MyObserver<T>) {
        this.observers!!.add(myObserver)
    }

    fun removeObserver(myObserver: MyObserver<*>) {
        this.observers!!.remove(myObserver)
    }

    fun notifyObserver(message: T) {
        for (myObserver in observers!!)
            myObserver.update(message)
    }

    fun setObservers(observers: ArrayList<MyObserver<T>>) {
        this.observers = observers
    }
}