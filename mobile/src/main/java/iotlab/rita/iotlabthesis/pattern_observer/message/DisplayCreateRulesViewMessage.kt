package iotlab.rita.iotlabthesis.pattern_observer.message

import android.widget.Toast
import iotlab.rita.iotlabthesis.DrawerActivity

class DisplayCreateRulesViewMessage : MyMessage {

    override fun perform(activity: DrawerActivity) {
        if(activity.smartHome.getAllDevices().size > 0)
            activity.homeRulesFragment.displayCreateRules()
        else
            Toast.makeText(activity,"No device in the house, add some devices to be able to create a rule",Toast.LENGTH_LONG).show()
    }
}