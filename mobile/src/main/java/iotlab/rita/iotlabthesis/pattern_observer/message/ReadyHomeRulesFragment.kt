package iotlab.rita.iotlabthesis.pattern_observer.message

import iotlab.rita.iotlabthesis.DrawerActivity

class ReadyHomeRulesFragment : MyMessage {

    override fun perform(activity: DrawerActivity) {
        activity.homeRulesFragment.setup(activity.smartHome, activity)
    }
}