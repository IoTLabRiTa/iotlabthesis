package iotlab.rita.iotlabthesis.device

import iotlab.rita.iotlabthesis.device.device_resources.TemperatureDeviceResource

class Thermostat: DeviceState(){

    private var temperatureReading =  TemperatureDeviceResource()
    private var targetTemperature: Float = 0F

    fun getTemperatureReading() = temperatureReading.getValue()
    fun getTargetTemperature() = targetTemperature

    /**
     * Assign the value of the message to the class
     */
    override fun convertAddedAttribute(attr: String, value: String) : Boolean{
        when(attr){
            "temperatureReading"->temperatureReading.setValue(value.toFloat())
            "targetTemperature"->targetTemperature = value.toFloat()
            else -> return false
        }
        return true
    }


    override fun getAddedSerialization(map: HashMap<String, String>) {
        //TODO da fare con reflection
        map.put("temperatureReading",temperatureReading.getValue().toString())
        map.put("targetTemperature",targetTemperature.toString())

    }

    override fun toString(): String {
        return "$(temperatureReading = ${temperatureReading}, targetTemperature = ${targetTemperature}, on = ${isOn()})"
    }
}