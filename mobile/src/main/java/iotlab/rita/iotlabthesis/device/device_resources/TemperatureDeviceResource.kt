package iotlab.rita.iotlabthesis.device.device_resources


class TemperatureDeviceResource : DeviceResource<Float>(){

    private var value : Float = 0F

    fun getValue() = value

    fun setValue(newValue : Float){
        this.value = newValue
    }
}