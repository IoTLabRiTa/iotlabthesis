package iotlab.rita.iotlabthesis.device

import iotlab.rita.iotlabthesis.device.device_resources.EnergyCollectedDeviceResource


class SolarPanel : DeviceState() {


    private var powerAccumulated = EnergyCollectedDeviceResource()

    fun getPOwerAccumulated() = powerAccumulated
    /**
     * Assign the value of the message to the class
     */
    override fun convertAddedAttribute(attr: String, value: String) : Boolean{
        when(attr){
            "powerAccumulated"->powerAccumulated.setValue(value.toFloat())
            else -> return false
        }
        return true
    }

    override fun toString(): String {
        return "(powerAccumulated=${powerAccumulated.getValue()}, on=${isOn()})"
    }

    override fun getAddedSerialization(map: HashMap<String, String>) {
        map.put("powerAccumulated",powerAccumulated.getValue().toString())
    }

}