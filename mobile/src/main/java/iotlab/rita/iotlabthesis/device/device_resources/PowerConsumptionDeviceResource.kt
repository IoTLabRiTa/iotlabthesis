package iotlab.rita.iotlabthesis.device.device_resources

import android.util.Log

class PowerConsumptionDeviceResource : DeviceResource<Int>() {

    private var power : Int = 0

    fun getValue() = power

    fun setValue(newValue : Int){
        Log.d("RESOURCE","SONO PASSATO")
        power = newValue
    }

}