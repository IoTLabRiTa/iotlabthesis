package iotlab.rita.iotlabthesis.device.device_resources

import android.util.Log
import iotlab.rita.iotlabthesis.pattern_observer.MyObservable

class EnergyCollectedDeviceResource : DeviceResource<Float>(){

    private var energyCollected : Float = 0F

    fun getValue() = energyCollected

    fun setValue(newValue : Float){
        Log.d("RESOURCE","SONO PASSATO")
        this.energyCollected = newValue

    }




}