package iotlab.rita.iotlabthesis.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.pattern_observer.message.CommandMessage

import java.util.HashMap

class MoveRoomDialog() : DialogFragmentObservable<CommandMessage>(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val inflater = activity!!.layoutInflater
        var view = inflater.inflate(R.layout.move_room_dialog, null)

        builder.setView(view)

        var moveRoomSpinner = view.findViewById(R.id.moveRoom) as Spinner

        var spinnerAdapter: ArrayAdapter<String> = ArrayAdapter(context,R.layout.spinner_item, arguments!!.getStringArrayList("smartspaces"))
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        moveRoomSpinner.adapter=spinnerAdapter

        var okListener= DialogInterface.OnClickListener { dialog, id ->
            var moveMap : HashMap<String, String> = HashMap()
            moveMap.put("to",moveRoomSpinner.selectedItem as String)
            var commandMessage = CommandMessage("MoveDevice", moveMap)
            notifyObserver(commandMessage)
        }

        val alertDialog = builder.setTitle("Device management")
                .setPositiveButton("yes",okListener)
                .setNegativeButton("cancel", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
                // Create the AlertDialog object and return it
                .create()
        return alertDialog
    }


}