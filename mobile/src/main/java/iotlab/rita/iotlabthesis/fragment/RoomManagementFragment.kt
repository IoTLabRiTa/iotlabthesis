package iotlab.rita.iotlabthesis.fragment

import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.MyObservableFragment


//TODO DA ELIMINARE
class RoomManagementFragment : MyObservableFragment<MyMessage>(){
/*

    lateinit var graphView : GraphView

    lateinit var myView : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        myView =  inflater.inflate(R.layout.fragment_room_management, container, false)

        graphView = myView.findViewById(R.id.graph) as GraphView
        notifyObserver(ReadyRoomManagementFragment())

        return myView
    }

    fun setup(smartHome: SmartHome){
        if(myView!=null){
            graphView = myView.findViewById(R.id.graph) as GraphView

            val graph = Graph()

            var hashMap : HashMap<String,String> = HashMap()
            //Draw mainNode
            hashMap.put("type","mainNode")
            hashMap.put("name","Home")
            var mainNode : Node = Node(hashMap)

            smartHome.getDevices().forEach {
                var hashDevice : HashMap<String,String> = HashMap()
                hashDevice.put("type","device")
                hashDevice.put("name",it.getName())
                hashDevice.put("id",it.getUniqueID())
                var nodeDevice = Node(hashDevice)
                graph.addEdge(mainNode,nodeDevice)
            }

            //Add smartspaces to list
            var smartSpaces : ArrayList<SmartSpace> = ArrayList()
            smartSpaces.add(smartHome)
            smartHome.getRooms().forEach {
                smartSpaces.add(it)
            }

            //Add smartspaces and devices inside them
            smartHome.getRooms().forEach {
                var hashNode : HashMap<String,String> = HashMap()
                hashNode.put("type","smartSpace")
                hashNode.put("name",it.getName())
                var nodeSmartSpace = Node(hashNode)
                graph.addEdge(mainNode,nodeSmartSpace)

                it.getDevices().forEach{
                    var hashDevice : HashMap<String,String> = HashMap()
                    hashDevice.put("type","device")
                    hashDevice.put("name",it.getName())
                    hashDevice.put("id",it.getUniqueID())
                    var nodeDevice = Node(hashDevice)
                    graph.addEdge(nodeSmartSpace,nodeDevice)
                }
            }



            val adapter = object : BaseGraphAdapter<ViewHolder>(graph) {
                @NonNull
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                    val view = LayoutInflater.from(parent.context).inflate(R.layout.node_layout, parent, false)
                    return GraphViewHolder(view)
                }

                override fun onBindViewHolder(viewHolder: ViewHolder, data: Any, position: Int) {
                    var dataMap = data as HashMap<String,String>
                    var holder = viewHolder as GraphViewHolder

                    //Set Icon
                    var iconName = data["name"]!!.toLowerCase() + "_icon"
                    context!!.resources.getIdentifier(iconName, "mipmap", context!!.packageName)
                    holder.icon.setImageResource(context!!.resources.getIdentifier(iconName, "mipmap", context!!.packageName))

                    holder.name.text = data["name"]!!

                    if(dataMap["type"].equals("device")){
                        holder.view.setOnClickListener{
                            var moveMap : java.util.HashMap<String, String> = java.util.HashMap()
                            moveMap.put("to","Bathroom")
                            var commandMessage = CommandMessage("MoveDevice",moveMap)
                            commandMessage.deviceID = dataMap["id"]!!
                            smartHome.getAllDevices().filter { it.getUniqueID().equals(dataMap["id"]!!) }.first().notifyObserver(commandMessage)
                        }
                    }





                }
            }
            graphView.adapter = adapter

            // set the algorithm here
            val configuration = BuchheimWalkerConfiguration.Builder()
                    .setSiblingSeparation(100)
                    .setLevelSeparation(300)
                    .setSubtreeSeparation(300)
                    .setOrientation(BuchheimWalkerConfiguration.ORIENTATION_LEFT_RIGHT)
                    .build()
            adapter.algorithm = BuchheimWalkerAlgorithm(configuration)
        }

    }

    class GraphViewHolder(var view: View) : ViewHolder(view) {

        var icon : ImageView
        var name : TextView

        init{
            icon = view.findViewById(R.id.icon)
            name = view.findViewById(R.id.name)
        }
    }

    */
}
