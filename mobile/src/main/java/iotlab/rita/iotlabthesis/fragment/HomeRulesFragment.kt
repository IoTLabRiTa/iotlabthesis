package iotlab.rita.iotlabthesis.fragment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.DrawerActivity
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.expandable_listview.ConditionListAdapter
import iotlab.rita.iotlabthesis.expandable_listview.ConsequenceAdapter
import iotlab.rita.iotlabthesis.expandable_listview.RuleAdapter
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.rules.*
import iotlab.rita.iotlabthesis.pattern_observer.*
import iotlab.rita.iotlabthesis.model.rules.TimeAntecedent
import iotlab.rita.iotlabthesis.pattern_observer.message.*
import java.text.SimpleDateFormat
import java.util.*

class HomeRulesFragment : MyObservableFragment<MyMessage>()  {

    //Grafica
    private var v: View? = null
    lateinit private var buttonMenu : ImageButton
    lateinit var createRuleButton : FloatingActionButton

    lateinit var showRulesCL : View
    lateinit var addRulesCL : View

    lateinit var addConditionBT : FloatingActionButton
    lateinit var addConditionTimeBT : FloatingActionButton
    lateinit var addConsequenceBT : FloatingActionButton
    lateinit var addRuleBT : FloatingActionButton
    lateinit var abortBT: FloatingActionButton
    lateinit var conditionsLV : ListView
    lateinit var consequencesLV : ListView
    lateinit var ruleLV : ListView


    lateinit private var conditionsAdapter : ConditionListAdapter
    var conditionList : ArrayList<ConditionRule> = ArrayList()

    lateinit private var consequencesAdapter : ConsequenceAdapter
    var consequenceList : ArrayList<Consequence> = ArrayList()

    lateinit private var ruleAdapter : RuleAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(v==null){
            v = inflater.inflate(R.layout.homerules, container, false);
        }

        showRulesCL = v!!.findViewById(R.id.showRulesCL)
        addRulesCL = v!!.findViewById(R.id.addRuleCL)
        createRuleButton = v!!.findViewById(R.id.addRuleFAB)
        createRuleButton.setOnClickListener{
            conditionList.clear()
            consequenceList.clear()
            (conditionsLV.adapter as ConditionListAdapter).notifyDataSetChanged()
            (consequencesLV.adapter as ConsequenceAdapter).notifyDataSetChanged()
            notifyObserver(DisplayCreateRulesViewMessage())
        }

        ruleLV = v!!.findViewById(R.id.rulesLV)
        buttonMenu = v!!.findViewById(R.id.menuButton) as ImageButton
        buttonMenu.setOnClickListener{
            notifyObserver(OpenDrawerMessage())
        }

        addConditionBT = v!!.findViewById(R.id.addconditionBT)
        addConditionTimeBT = v!!.findViewById(R.id.addtimeconditionBT)
        addConsequenceBT = v!!.findViewById(R.id.addconsequenceBT)
        addRuleBT = v!!.findViewById(R.id.addRuleBT)

        abortBT = v!!.findViewById(R.id.abortBT)
        abortBT.setOnClickListener{
            displayShowRules()
        }

        conditionsLV = v!!.findViewById(R.id.conditionLV)
        consequencesLV = v!!.findViewById(R.id.consequencesLV)

        notifyObserver(ReadyHomeRulesFragment())
        displayShowRules()
        return v
    }

    fun displayShowRules(){
        showRulesCL.visibility = View.VISIBLE
        addRulesCL.visibility = View.GONE
    }

    fun displayCreateRules(){
        showRulesCL.visibility = View.GONE
        addRulesCL.visibility = View.VISIBLE
    }

    fun setup(smartHome: SmartHome, activity : DrawerActivity){
        conditionsAdapter = ConditionListAdapter(context!!, smartHome,conditionList)
        conditionsLV.adapter = conditionsAdapter

        var fragmentManager = this.fragmentManager!!
        consequencesAdapter = ConsequenceAdapter(context!!,fragmentManager,smartHome,consequenceList)
        consequencesLV.adapter = consequencesAdapter

        ruleAdapter = RuleAdapter(context!!,smartHome, this)
        ruleLV.adapter = ruleAdapter

        addConditionBT.setOnClickListener {
            conditionList.add(ConditionResourcePredicate())
            notifyCondition(activity)
        }

        addConditionTimeBT.setOnClickListener {
            var timeCondition = ConditionTimePredicate()
            activity.runOnUiThread{showTimePicker(activity,timeCondition)}
        }


        addConsequenceBT.setOnClickListener {
            consequenceList.add(Consequence())
            notifyConsequece(activity)
        }

        addRuleBT.setOnClickListener {
            var ok = conditionList.fold(true){
                acc, x -> acc and x.isCorrectedSaved()
            }
            ok = consequenceList.fold(ok){
                acc, x -> acc and x.isCorrectedSaved()
            }
            if(ok){
                var rule = Rule()
                conditionList.forEach {
                    rule.addConditionPredicate(it)
                }
                consequenceList.forEach {
                    rule.addConsequence(it)
                }
                notifyObserver(OnRuleMessage(rule, "Add"))
                activity.runOnUiThread{
                    Toast.makeText(activity,"Rule ${rule.getID()} Saved", Toast.LENGTH_SHORT).show()
                    Log.d("RULE_CONFIGURATION","Created Rule ${rule.getID()}")
                    conditionList.clear()
                    consequenceList.clear()
                    notifyCondition(activity)
                    notifyConsequece(activity)
                    notifyRule(activity)
                    displayShowRules()
                }
            }else{
                activity.runOnUiThread{
                    Log.d("RULE_CONFIGURATION","Rule Not Created")
                    Toast.makeText(activity,"Error to save rule, conditions or consequences are not completed", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun notifyConsequece(activity: DrawerActivity){
        if(v != null){
            activity.runOnUiThread {
                (consequencesLV.adapter as ConsequenceAdapter).notifyDataSetChanged()
            }
        }

    }

    fun notifyCondition(activity: DrawerActivity){
        if(v != null) {
            activity.runOnUiThread {
                (conditionsLV.adapter as ConditionListAdapter).notifyDataSetChanged()
            }
        }
    }

    fun notifyRule(activity: DrawerActivity){
        if(v != null) {
            activity.runOnUiThread {
                (ruleLV.adapter as RuleAdapter).notifyDataSetChanged()
            }
        }
    }

    fun update(activity: DrawerActivity){
        notifyCondition(activity)
        notifyConsequece(activity)
        notifyRule(activity)
    }

    fun showTimePicker(activity: DrawerActivity, timeCondition: ConditionTimePredicate){
        var yearSelected : Int =1
        var monthSelected : Int = 1
        var daySelected : Int = 1
        var calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val calendarHour = calendar.get(Calendar.HOUR_OF_DAY)
        val calendarMinute = calendar.get(Calendar.MINUTE)
        var calendarSelected = Calendar.getInstance()

        var timepickerdialog = TimePickerDialog(activity,
                object : TimePickerDialog.OnTimeSetListener{
                    override fun onTimeSet(view: TimePicker?, hour: Int, minute: Int) {
                        calendarSelected.set(yearSelected,monthSelected,daySelected,hour,minute)
                        var sdf = SimpleDateFormat("d MMM yyyy HH:mm", Locale.ITALY)
                        var timeAntecedent = TimeAntecedent(calendarSelected,sdf)
                        timeCondition.setParameters("After","","","DateTime",timeAntecedent.getAntecedentID())
                        timeCondition.setcorrectedSaved(true)
                        conditionList.add(timeCondition)
                        notifyCondition(activity)
                    }

                }, calendarHour, calendarMinute, true)

        var datePicker = DatePickerDialog(activity, object: DatePickerDialog.OnDateSetListener{

            override fun onDateSet(view: DatePicker?, y: Int, m: Int, d: Int) {
                yearSelected = y
                monthSelected = m
                daySelected = d
                timepickerdialog.show()
            }

        },year,month,day)

        datePicker.show()

    }

}