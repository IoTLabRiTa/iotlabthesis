package iotlab.rita.iotlabthesis.fragment

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import iotlab.rita.iotlabthesis.R
import iotlab.rita.iotlabthesis.gui.RoomsExpandableListAdapter
import iotlab.rita.iotlabthesis.model.SmartHome
import iotlab.rita.iotlabthesis.model.SmartSpace
import iotlab.rita.iotlabthesis.pattern_observer.*
import iotlab.rita.iotlabthesis.pattern_observer.message.MyMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.OpenDrawerMessage
import iotlab.rita.iotlabthesis.pattern_observer.message.ReadyHomeDeviceFragment

class HomeDevicesFragment() : MyObservableFragment<MyMessage>() {

    //Grafica
    private var v: View? = null

    lateinit var expandableListView: ExpandableListView
    private lateinit var expandableListAdapter: RoomsExpandableListAdapter
    lateinit var mFragmentManager : FragmentManager

    lateinit var connectionTV : TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(v==null){
            v = inflater.inflate(R.layout.activity_main, container, false);
        }

        var button = v!!.findViewById(R.id.menuButton) as ImageButton
        button.setOnClickListener{
            notifyObserver(OpenDrawerMessage())
        }

        //connectionTV = v!!.findViewById(R.id.connectionTV)

        notifyObserver(ReadyHomeDeviceFragment())
        return v
    }



    fun setup(smartHome: SmartHome){
        //Create list of smartspaces for the listview
        var smartspaces : ArrayList<SmartSpace> = ArrayList()
        smartspaces.add(smartHome)
        smartspaces.addAll(smartHome.getRooms())

        mFragmentManager = this.fragmentManager!!
        expandableListView = v!!.findViewById(R.id.expandableListView)
        expandableListAdapter = RoomsExpandableListAdapter(context!!,smartspaces,mFragmentManager)
        expandableListView.setAdapter(expandableListAdapter)
    }


    fun updateLV(){
        activity!!.runOnUiThread{
            (expandableListView.expandableListAdapter as RoomsExpandableListAdapter).notifyDataSetChanged()
        }

    }


}